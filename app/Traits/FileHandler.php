<?php
namespace App\Traits;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
trait FileHandler
{
    /**
     * @param UploadedFile|null $file
     * @param string $dir
     * @param string|null $fileName
     * @return null|string Stored file name, null if uploaded file is null or unable to upload
     */
    private function storePhoto(?UploadedFile $file, ?string $dir = '', ?string $fileName = ''): ?string {
        if (!$file) {
            return null;
        }
        $fileName = $fileName ?: md5(time()) . '.'. $file->getClientOriginalExtension();
        try {
            /** @var Storage $path */
            Storage::putFileAs(
                $dir, $file, $fileName
            );
        } catch (\Exception $exception) {
            return null;
        }
        return $fileName;
    }
}
