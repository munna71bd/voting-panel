<?php


namespace App\Listeners;


use App\Events\LocVillageSaved;
use App\Repositories\LocVillageRepository;

class LocVillageSavedListener
{
    private $locVillageRepository = null;

    public function __construct(LocVillageRepository $locVillageRepository)
    {
        $this->locVillageRepository = $locVillageRepository;
    }

    /**
     * @param LocVillageSaved $event
     */
    public function handle(LocVillageSaved $event)
    {
        $this->locVillageRepository->updateToDistrictDbs($event->village);
        return false;
    }
}
