<?php

namespace App\Providers;

use App\Models\Bank;
use App\Models\CommitteeType;
use App\Models\User;
use App\Policies\BankPolicy;
use App\Policies\CommitteeTypePolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;


class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Bank::class => BankPolicy::class,
        CommitteeType::class => CommitteeTypePolicy::class
    ];

    protected $gates = [

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        if (count($this->gates) > 0) {
            // Gates
            foreach ($this->gates as $gate) {
                Gate::define($gate, function (User $user) use ($gate) {
                    return !$user->isSuperUser() && !$user->isSuperAdmin() && $user->hasPermission($gate);
                });
            }
        }

    }
}
