<?php

namespace App\Providers;

use App\Events\LocVillageSaved;
use App\Events\Voyager\BreadUpdated;
use App\Listeners\AddAdditionalPermission;
use App\Listeners\LocVillageSavedListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        BreadUpdated::class => [
            AddAdditionalPermission::class,
        ],
        LocVillageSaved::class => [
            LocVillageSavedListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
