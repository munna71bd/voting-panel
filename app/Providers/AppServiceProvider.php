<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(VoyagerServiceProvider::class);
    }

    /**
     * @param Router $router
     * @param Dispatcher $event
     */
    public function boot(Router $router, Dispatcher $event)
    {
        Schema::defaultStringLength(191);
    }
}
