<?php


namespace App\Http\Middleware;


use App\Models\User;
use Closure;
use Illuminate\Support\Facades\Route;

class VoyagerAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        /** @var User $user */
        if (!app('VoyagerAuth')->guest()) {
            $user = app('VoyagerAuth')->user();
            return $user->hasPermission('browse_admin') ? $next($request) : redirect('/');
        }

        $urlLogin = route('voyager.login');

        return redirect()->guest($urlLogin);
    }
}
