<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*
        $method_name = Route::getCurrentRoute()->getActionMethod();
        $current_route = Route::getCurrentRoute();

        $allowed_methods = config('userlog.allowed_methods');
        $allowed_method_actions = config('userlog.allowed_method_actions');
        $module_names = config('userlog.module_names');

        //if (is_array($allowed_methods) && in_array($method_name, $allowed_methods)) {

            $module_name = substr($current_route->action['namespace'], strpos($current_route->action['namespace'], '\\') + 1);
            $module_name = substr($module_name, 0, strpos($module_name, '\\'));
            $module_name = isset($module_names[$module_name]) ? $module_names[$module_name] : $module_name;

            $logData = [
                'module_name' => $module_name,
                //'details' => json_encode($current_route),
                'action_url' => url("/") . "/" . $current_route->uri,
                'action' => isset($allowed_method_actions[$method_name]) ? $allowed_method_actions[$method_name] : $method_name,
                'user_id' => auth()->user() ? auth()->user()->id : 0,
                'created_at' => date('Y-m-d h:i:s'),
            ];

            DB::table('user_logs')->insert($logData);
        //}
        */

        App::setLocale(session('locale', config('app.locale')));

        return $next($request);
    }
}
