<?php

namespace App\Http\Controllers\Voyager;

use App\Models\Role;
use App\Models\User;
use App\Models\UsersPermission;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;
use App\Facades\Voyager;
use Yajra\DataTables\Facades\DataTables;


class VoyagerUserController extends VoyagerBaseController
{
    const VIEW_PATH = 'vendor.voyager.';

    private $userRepository = null;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function notifications()
    {
        return Auth::user()->unreadNotifications()->limit(5)->get()->toArray();
    }

    public function profile(Request $request)
    {
        return Voyager::view(self::VIEW_PATH . 'profile');
    }

    public function index(Request $request)
    {
        $this->authorize('browse', app(User::class));
        $usesSoftDeletes = true;
        $isServerSide = false;

        $localPrefix = 'bread.users.';
        $view = "voyager::users.browse";
        return Voyager::view($view, compact(
            'isServerSide',
            'usesSoftDeletes',
            'localPrefix'
        ));
    }

    public function create(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        /** Check permission */
        $this->authorize('add', app($dataType->model_name));

        /** @var User $dataTypeContent */
        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $dataType->addRows[$key]['col_width'] = $row->details->width ?? 100;
        }

        /** If a column has a relationship associated with it, we do not want to show that field */
        $this->removeRelationshipField($dataType, 'add');


        /** Check if BREAD is Translatable */
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $view = self::VIEW_PATH . 'bread.edit-add';

        if (view()->exists(self::VIEW_PATH . $slug.".edit-add")) {
            $view = self::VIEW_PATH . "$slug.edit-add";
        }

        return Voyager::view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable'));
    }

    public function store(Request $request)
    {

        $user = $this->userRepository->newUser();
        $this->authorize('add', $user);

        $this->userRepository->storeValidation($request, $user)->validate();

        DB::beginTransaction();

        try {
            $this->userRepository->saveUser($request, $user);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
            ], 403);
        }
        return response()->json([
            'user_id' => $user->id,
            'message' => __('bread.users.model_name') . ' ' . __('voyager::generic.successfully_added_new')
        ], 200);
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $dataTypeContent = $this->userRepository->getUserById($id, $dataType->scope);

        foreach ($dataType->editRows as $key => $row) {
            $dataType->editRows[$key]['col_width'] = isset($row->details->width) ? $row->details->width : 100;
        }

        $allPermissionTableNames = Voyager::model('Permission')->all()->groupBy('table_name');

        /** If a column has a relationship associated with it, we do not want to show that field  */
        $this->removeRelationshipField($dataType, 'edit');

        $userPermissions = $dataTypeContent->allPermissionKey()->all();
        $customPermissions = $dataTypeContent->getCustomPermissions()->all();

        /** Check permission */
        $this->authorize('edit', $dataTypeContent);

        $roles = Role::pluck('display_name', 'id');

        $view = 'voyager::bread.edit-add';

        if (view()->exists("voyager::$slug.edit-add")) {
            $view = "voyager::$slug.edit-add";
        }

        $userExtraRoles = $dataTypeContent->roles()->get()->keyBy('id');

        return Voyager::view($view, compact(
            'dataType', 'dataTypeContent', 'userPermissions', 'customPermissions',
            'allPermissionTableNames', 'roles', 'userExtraRoles'
        ));
    }

    public function update(Request $request, $id)
    {
        /** @var User $user */
        $user = $this->userRepository->getUserById($id);
        $this->authorize('edit', $user);

        $this->userRepository->storeValidation($request, $user)->validate();

        DB::beginTransaction();

        try {
            $this->userRepository->saveUser($request, $user);
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            Log::debug($e->getMessage());
            Log::debug($e->getTraceAsString());
            return response()->json([
                'message' => __('voyager::generic.something_wrong')
            ], 403);
        }
        return response()->json([
            'message' => __('bread.users.model_name') . ' ' . __('voyager::generic.successfully_updated')
        ], 200);
    }

    public function changePermission(Request $request)
    {
        $this->userRepository->userPermissionValidator($request);
        $user = $this->userRepository->getUserById($request->user_id);
        $this->authorize('editUserPermission', $user);
        $userPermission = $this->userRepository->changeUserPermission($request);
        if (!empty($userPermission) && $userPermission->id) {
            return $userPermission;
        } else {
            abort(400);
        }
    }

    public function changePermissionForTable(Request $request)
    {
        $this->userRepository->userPermissionsValidator($request);
        $user = $this->userRepository->getUserById($request->user_id);
        $this->authorize('editUserPermission', $user);

        if (!$this->userRepository->changeUserPermissions($request)) {
            abort(400);
        } else {
            return ['success' => 1];
        }
    }

    public function getDatatable(Request $request)
    {
        try {
            return $this->userRepository->getDataTableData($request);
        } catch (\Exception $ex) {
            return response()->json([], 404);
        }
    }
}
