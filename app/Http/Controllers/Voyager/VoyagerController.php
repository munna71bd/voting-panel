<?php

namespace App\Http\Controllers\Voyager;

use App\Facades\Voyager;
use App\Http\Middleware\Locale;
use App\Models\DashboardData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use Module\Bank\Models\BankAccount;
use Module\Cdc\Models\Cdc;
use Module\Cluster\Models\Notification;
use Module\Scg\Models\Scg;

class VoyagerController extends Controller
{

    public function index()
    {
        $is_vote_taken = DB::table('users')
            ->where('id', \auth()->user()->id)
            ->where('is_vote_taken', 1)
            ->count();

        if ($is_vote_taken > 0) {

            $voter_message = 'আপনি ইতিমধ্যে ভোট দিয়ে ফেলেছেন। ফলাফলের জন্য অপেক্ষা করুন।';
            return Voyager::view('voyager::voter-message', compact('voter_message'));
        } else {
            $c = config('softbd.candidates');
            $vote_date_start = date('d M Y h:i:s A', strtotime(config('softbd.vote_start_date_time')));
            $vote_date_end = date('d M Y h:i:s A', strtotime(config('softbd.vote_end_date_time')));

            return Voyager::view('voyager::voter-panel', compact('c', 'vote_date_start', 'vote_date_end'));
        }

    }

    public function logout()
    {
        app('VoyagerAuth')->logout();

        return redirect()->route('voyager.login');
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        /** Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc... */
        while (Storage::disk(config('voyager.storage.disk'))->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            /** move uploaded file from temp to uploads directory */
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string)$image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        /** echo out script that TinyMCE can handle and update the image in the editor */
        return "<script> parent.helpers.setImageValue('" . Voyager::image($fullFilename) . "'); </script>";
    }

    public function assets(Request $request)
    {
        $path = Str::start(str_replace(['../', './'], '', urldecode($request->path)), '/');

        $path = base_path('public/backend_resources' . $path);

        if (File::exists($path)) {
            $mime = '';
            if (Str::endsWith($path, '.js')) {
                $mime = 'text/javascript';
            } elseif (Str::endsWith($path, '.css')) {
                $mime = 'text/css';
            } else {
                $mime = File::mimeType($path);
            }
            $response = response(File::get($path), 200, ['Content-Type' => $mime]);
            $response->setSharedMaxAge(31536000);
            $response->setMaxAge(31536000);
            $response->setExpires(new \DateTime('+1 year'));

            return $response;
        }

        return response('', 404);
    }

    public function getDashboardDataMaster()
    {

        $data = [];


        return $data;
    }

    public function launch(Request $request)
    {

        return Voyager::view('voyager::launch');
    }


    public function getDashboardData()
    {

        return $this->getDashboardDataMaster();
    }
}
