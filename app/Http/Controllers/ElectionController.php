<?php


namespace App\Http\Controllers;


use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Voyager\Database\Schema;
use App\Facades\Voyager;
use Yajra\DataTables\Facades\DataTables;

class ElectionController extends Controller
{

    protected $user;


    public function index()
    {
        if (auth()->user()) {
            return \redirect('admin');
        }
        return view('voyager::login');
    }


    public function loginVoter(Request $request)
    {
        try {


            $data = $request->except(['_token', 'api_token']);

            $rules = [
                'username' => 'required',


            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {

                $msg = $validator->messages()->first();

                $msg = str_replace($msg, 'username আবশ্যক', __('Mobile No Or Email ID') . ' আবশ্যক');

                return \redirect()->back()->withErrors([
                    'message' => $msg,
                ]);
            }


            $voter = User::where('username', $request->username)
                ->orWhere('email', $request->username)
                ->orWhere('cell_phone', $request->username)
                ->groupBy('username')
                ->first();

            if ($voter) {
                $voterUserName = $voter->username;
                $voter->otp_code = rand(0, 999999999);
                $voter->otp_sending_time = date('Y-m-d h:i:s');
                $voter->save();

                if (!empty($voter->cell_phone)) {
                    sendOtp('ওটিপি কোডটি হলো ' . $voter->otp_code . ' ।' . __('brand_title'), $voter->cell_phone);
                }

                if (!empty($voter->email)) {
                    sendEmail('আপনার ওটিপি কোডটি হলো ' . $voter->otp_code, $voter->email);
                }


                return \redirect()->back()->with([
                    'voterUserName' => $voterUserName,
                ]);

            }

            return \redirect()->back()->withErrors([
                'message' => 'দুঃখিত! এই ' . __('Mobile No Or Email ID') . ' দিয়ে  কোনো ভোটারের তথ্য খুঁজে পাওয়া যায়নি। ',
            ]);

        } catch (\Exception $ex) {

            return \redirect()->back()->withErrors([
                'message' => $ex->getMessage()
            ]);
        }

        return \redirect()->back()->withErrors([
            'message' => 'যান্ত্রিক ত্রুটি ঘটেছে',
        ]);


    }


    public function verifyOtp(Request $request)
    {
        try {

            Session::flash('voterUserName', $request->username);

            $data = $request->except(['_token', 'api_token']);

            $rules = [
                'otp_code' => 'required',
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {

                $msg = $validator->messages()->first();

                $msg = str_replace($msg, 'otp code আবশ্যক', 'OTP কোড আবশ্যক');

                return \redirect()->back()->withErrors([
                    'message' => $msg,
                ]);
            }


            $voter = DB::table('users')
                ->where('username', $request->username)
                ->first();


            if ($voter && $voter->otp_code == $request->otp_code) {

                $imported_user = $voter;

                if ($voter->is_vote_taken == 1) {

                    return \redirect()->back()->withErrors([
                        'message' => 'আপনি ইতিমধ্যে ভোট দিয়ে ফেলেছেন। ফলাফলের জন্য অপেক্ষা করুন।',

                    ]);
                } else if ($request->otp_code == $imported_user->otp_code) {

                    $otp_limit_time = (int)config('services.otp.limit_time');
                    $otp_generated_time = date('Y-m-d h:i:s', strtotime($imported_user->otp_sending_time . ' ' . $otp_limit_time . ' seconds'));


                    if ($otp_generated_time >= date('Y-m-d h:i:s')) {

                        Auth::loginUsingId($voter->id);
                        return \redirect()->back();

                    } else {

                        return \redirect()->back()->withErrors([
                            'message' => 'ওটিপিটির মেয়াদ শেষ! অনুগ্রহ করে আবার চেষ্টা করুন।',
                        ]);
                    }
                } else {
                    return \redirect()->back()->withErrors([
                        'message' => 'দুঃখিত! OTP  কোডটি ভুল হয়েছে।আবার চেষ্টা করুন।  ',
                    ]);
                }
            }


            return \redirect()->back()->withErrors([
                'message' => 'দুঃখিত! OTP  কোডটি ভুল হয়েছে।আবার চেষ্টা করুন।  ',
            ]);

        } catch (\Exception $ex) {

            return \redirect()->back()->withErrors([
                'message' => $ex->getMessage()
            ]);
        }

        return \redirect()->back()->withErrors([
            'message' => 'যান্ত্রিক ত্রুটি ঘটেছে',
        ]);
    }


    public function makeVote(Request $request)
    {

        try {


            $data = $request->except(['_token', 'api_token']);

            $rules = [
                'candidate' => 'required|numeric|min:1',


            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) {

                $msg = $validator->messages()->first();

                $msg = str_replace($msg, 'candidate আবশ্যক', 'প্রার্থী নির্বাচন করুন');

                return \redirect()->back()->withErrors([
                    'message' => $msg,
                ]);
            }

            $vote_data = [
                'candidate_id' => $data['candidate'],
                'vote_taken_at' => date('Y-m-d h:i:s'),
            ];


            $is_vote_taken = DB::table('users')
                ->where('id', \auth()->user()->id)
                ->where('is_vote_taken', 1)
                ->count();

            if ($is_vote_taken > 0) {
                return \redirect()->back()->withErrors([
                    'success' => 'আপনি ইতিমধ্যে ভোট দিয়ে ফেলেছেন। ফলাফলের জন্য অপেক্ষা করুন।',
                ]);
            } else {
                $limit_time = (int)config('softbd.limit_time');
                $vote_start_date = config('softbd.vote_start_date_time');
                $vote_end_date = config('softbd.vote_end_date_time');

                $vote_start_date = date('Y-m-d H:i:s', strtotime($vote_start_date));
                $vote_end_date = date('Y-m-d H:i:s', strtotime($vote_end_date));


                $today = date('Y-m-d H:i:s');

                if ($today >= $vote_start_date && $today <= $vote_end_date) {

                    $cids = (array)(config('softbd.candidates'));

                    if (array_key_exists($request->candidate, $cids)) {

                        DB::table('users')->where('id', \auth()->user()->id)->update(['is_vote_taken' => 1]);
                        DB::table('votes')->insert($vote_data);


                        $msg = 'ভোট গ্রহণ সফল ভাবে সম্পন্ন হয়েছে। ফলাফলের জন্য অপেক্ষা করুন।';
                        Session::flash('special_message', $msg);

                        return \redirect()->back()->withErrors([
                            'message' => $msg,
                        ]);
                    } else {
                        return \redirect()->back()->withErrors([
                            'message' => 'দয়া করে হ্যাকিং এর চেষ্টা করবেন না !',
                        ]);
                    }

                } else {
                    if ($today < $vote_start_date) {
                        return \redirect()->back()->withErrors([
                            'message' => 'ভোট গ্রহণ এখনো শুরু হয়নি।',
                        ]);
                    }
                    return \redirect()->back()->withErrors([
                        'message' => 'ভোট গ্রহণের নির্দিষ্ট সময়সীমা পার হয়ে গেছে।',
                    ]);
                }
            }


        } catch (\Exception $ex) {

            return \redirect()->back()->withErrors([
                'message' => $ex->getMessage()
            ]);
        }

        return \redirect()->back()->withErrors([
            'message' => 'যান্ত্রিক ত্রুটি ঘটেছে',
        ]);

    }

    public function getResult(Request $request)
    {

        $vote_end_date = config('softbd.vote_end_date_time');
        $vote_end_date = date('Y-m-d H:i:s', strtotime($vote_end_date . " +5 minutes"));


        $today = date('Y-m-d H:i:s');

        if ($today < $vote_end_date) {
            return '<body style="background: #292829;"><center><h1 style="color: #ffc107;margin:20%">ফলাফল এখনো প্রকাশিত হয়নি। ফলাফলের জন্য দয়া করে অপেক্ষা করুন।</h1></center></body>';
        } else {
            $results = [];
            $c1 = [];
            $c2 = [];
            $total_vote_taken = 0;

            if ($request->key == config('softbd.result_key')) {

                $total_vote_taken = DB::table('votes')
                    ->select(DB::raw('candidate_id,count(candidate_id) as total_vote'))->count();

                $c1 = DB::table('votes')
                    ->select(DB::raw('candidate_id,count(candidate_id) as total_vote'))
                    ->where('candidate_id', 1)
                    ->groupBy('candidate_id')->first();

                $c2 = DB::table('votes')
                    ->select(DB::raw('candidate_id,count(candidate_id) as total_vote'))
                    ->where('candidate_id', 2)
                    ->groupBy('candidate_id')->first();
            }

            return view('voyager::vote-result', compact('c1', 'c2', 'total_vote_taken'));
        }

    }


}
