<?php

namespace App\Console\Commands;

use App\Models\DataType;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class CrudGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crud:generate {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Crud including controller, model, views & migrations.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = trim($this->argument('name'));

        $dataType = DataType::where('model_name', "App\\Models\\{$name}")->first();

        Log::debug($dataType->rows->toArray());
    }
}
