<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DataBaseSetup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'setup:database {--withoutmaster=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create databases';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $withoutmaster = intval($this->option('withoutmaster'));

        $dbConnections = config('database.mysql_connections');
        $defaultConnection = config('database.default');
        $defaultConfig = [];
        if (!empty($dbConnections[$defaultConnection])) {
            $defaultConfig = $dbConnections[$defaultConnection];
            unset($dbConnections[$defaultConnection]);
        }

        if (!$withoutmaster && !empty($defaultConfig) && !empty($defaultConfig['database'])) {
            DB::beginTransaction();
            try {
                DB::statement("DROP DATABASE IF EXISTS " . $defaultConfig['database']);
                DB::statement("CREATE DATABASE " . $defaultConfig['database'] . " CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
                DB::commit();
            } catch (\Exception $ex) {
                DB::rollBack();
            }
        }
        foreach ($dbConnections as $conName => $conConfig) {

            if (empty($conConfig['database'])) {
                continue;
            }

            DB::beginTransaction();
            try {
                DB::statement("DROP DATABASE IF EXISTS " . $conConfig['database']);
                DB::statement("CREATE DATABASE " . $conConfig['database'] . " CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci");
                DB::commit();
            } catch (\Exception $ex) {
                DB::rollBack();
            }
        }

    }
}
