<?php


namespace App\Events;

use App\Models\LocVillage;
use Illuminate\Queue\SerializesModels;

class LocVillageSaved
{
    use SerializesModels;

    public $village;

    public function __construct(LocVillage $village)
    {
        $this->village = $village;
    }
}
