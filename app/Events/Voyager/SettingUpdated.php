<?php

namespace App\Events\Voyager;

use App\Models\Setting;
use Illuminate\Queue\SerializesModels;

class SettingUpdated
{
    use SerializesModels;

    public $setting;

    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }
}
