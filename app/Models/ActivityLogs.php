<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityLogs extends Model
{
    protected  $table = 'activity_logs';
    public $timestamps = false;
}
