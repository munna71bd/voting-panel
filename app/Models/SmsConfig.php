<?php

namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class SmsConfig extends BaseModel
{
    const API_TYPE_ONE_TO_MANY = 'one_to_many';
    const API_TYPE_MANY_TO_MANY = 'many_to_many';

    use SoftDeletes;
    protected $table = 'sms_configs';
    public $timestamps = true;
    protected $dates = ['deleted_at'];
    protected $guarded = [];
    protected $casts = [
        'parameters' => 'json'
    ];

    public function isCompanyIdHidden()
    {
        /** @var User $user */
        $user = Auth::user();
        if ($user->isSuperAdmin() || $user->isSuperUser()) {
            return false;
        }

        if ($this->exists) {
            return $this->company_id;
        }
        return $user->company_id;
    }

    public function scopeAcl($query)
    {

        /** @var User $user */
        $user = Auth::user();
        if ($user->isSuperUser() || $user->isSuperAdmin()) {
            return $query;
        }

        return $query->where('company_id', $user->company_id);
    }
}
