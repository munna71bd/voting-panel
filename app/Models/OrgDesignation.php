<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class OrgDesignation extends Model
{
    protected $connection = 'mysql_master';
    protected $table = 'org_designations';

    protected $fillable = [
        'title_en', 'title_bn', 'parent_designation_id', 'ordering', 'row_status'
    ];

    public $timestamps = true;

    public function parentDesignation()
    {
        return $this->belongsTo(OrgDesignation::class, 'parent_designation_id')
            ->select(['id', 'title_en', 'title_bn', 'ordering', 'row_status', 'created_by', 'updated_by']);
    }

    public function childDesignations()
    {
        return $this->hasMany(OrgDesignation::class, 'parent_designation_id')
            ->select(['id', 'title_en', 'title_bn', 'ordering', 'row_status']);
    }

    /**
     * Scope a query to only include the data access associate with the auth user.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAcl($query)
    {
        return $query;
    }
}
