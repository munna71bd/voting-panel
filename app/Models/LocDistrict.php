<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LocDistrict extends Model
{
    protected $connection = 'mysql_master';
    protected $table = 'loc_districts';
    protected $guarded = [];

    public function locDivision()
    {
        return $this->belongsTo(LocDivision::class, 'loc_division_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    /**
     * Scope a query to only include the data access associate with the auth user.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAcl($query)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isSystemUser() || $user->isDataAccessAreaAllUser()) {
            return $query;
        }
        return $query->where('id', $user->loc_district_id);
    }
}
