<?php


namespace App\Models;

class EnumModel
{
    const PROGRAMMES = [
        'পল্লী সমাজ সেবা কার্যক্রম',
        'পল্লী মাতৃ কেন্দ্র',
        'শহর সমাজ সেবা',
        'আশ্রয়ন',
        'দগ্ধ প্রতিবন্ধী পুনর্বাসন ও আশ্রয়ন'
    ];
}
