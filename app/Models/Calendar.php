<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Calendar extends BaseModel
{

    const GENERAL_EVENT = 'general_event';
    const HOLIDAY = 'holiday';
    const OFFICE_DAY = 'officeday';
    const OTHER_EVENT = 'others';

    use SoftDeletes;

    protected $dates = ['start_date', 'end_date', 'deleted_at'];
    protected $fillable = [
        'title', 'details', 'start_date', 'end_date', 'bg_color', 'text_color', 'event_type'
    ];

    public static function getEventTypes()
    {
        return [
            self::HOLIDAY, self::GENERAL_EVENT, self::OFFICE_DAY, self::OTHER_EVENT
        ];
    }

    public static function isHoliday(array $branchIds = [], Carbon $date = null)
    {
        if (is_null($date)) {
            $date = Carbon::now('Asia/Dhaka');
        }

        $exists = static::where(function (Builder $query) use ($date) {
            $query->whereDate('start_date', '>=', $date)
                ->wheredate('end_date', '<=', $date);
        })
            ->where('event_type', static::HOLIDAY)
            ->exists();

        return $exists;
    }

    public function scopeAcl($query)
    {
        /** @var \App\Models\User $user */
        /** @var Builder $query */
        $user = Auth::user();

        if ($user->isSuperAdmin() || $user->isSuperUser()) {
            return $query;
        }

        return $query;
    }
}
