<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LocMunicipality extends Model
{
    protected $connection = 'mysql_master';
    const MUNICIPALITY_TYPE_PAURASAVA = 'paurasava';
    const MUNICIPALITY_TYPE_CITY_CORP = 'city_corp';

    public static function getMunicipalityTypeOptions()
    {
        return [
            self::MUNICIPALITY_TYPE_PAURASAVA => __('generic.paurasava'),
            self::MUNICIPALITY_TYPE_CITY_CORP => __('generic.city_corp')
        ];
    }

    protected $table = 'loc_municipalities';
    protected $guarded = [];

    public function locDivision()
    {
        return $this->belongsTo(LocDivision::class, 'loc_division_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    public function locDistrict()
    {
        return $this->belongsTo(LocDistrict::class, 'loc_district_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    public function locUpazila()
    {
        return $this->belongsTo(LocUpazila::class, 'loc_upazila_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    /**
     * Scope a query to only include the data access associate with the auth user.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAcl($query)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isSystemUser() || $user->isDataAccessAreaAllUser()) {
            return $query;
        }
        if ($user->isDistrictLevelUser()) {
            return $query->where('loc_district_id', $user->loc_district_id);
        }
        if ($user->isUpazillaLevelUser()) {
            return $query->where('loc_upazila_id', $user->loc_upazila_id);
        }
        return $query->where('id', $user->loc_municipality_id);
    }

    public static function staticAcl($query, string $alias)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isSystemUser() || $user->isDataAccessAreaAllUser()) {
            return $query;
        }
        if ($user->isDistrictLevelUser()) {
            return $query->where($alias . 'loc_district_id', $user->loc_district_id);
        }
        if ($user->isUpazillaLevelUser()) {
            return $query->where($alias . 'loc_upazila_id', $user->loc_upazila_id);
        }
        return $query->where($alias . 'id', $user->loc_municipality_id);
    }
}
