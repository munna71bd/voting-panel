<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class BaseModel extends Model
{
    const ROW_STATUS_ACTIVE = '1';
    const ROW_STATUS_INACTIVE = '0';
    const ROW_STATUS_DELETED = '2';

    public static function getRowStatusOptions()
    {
        return [
            self::ROW_STATUS_ACTIVE => __('generic.active'),
            self::ROW_STATUS_INACTIVE => __('generic.inactive'),
            self::ROW_STATUS_DELETED => __('generic.delete'),
        ];
    }

    public static function getStatusOptions()
    {
        return self::getRowStatusOptions();
    }

    public function save(array $options = [])
    {

        if ($this->getAttribute('id')) {
            if (Schema::hasColumn($this->getTable(), 'updated_by')) {
                $this->updated_by = auth()->user()->id;
            }
        } else {
            if (Schema::hasColumn($this->getTable(), 'created_by')) {
                $this->created_by = auth()->user()->id;
            }
        }

        return parent::save($options);
    }

    public function update(array $attributes = [], array $options = [])
    {
        if (Schema::hasColumn($this->getTable(), 'updated_by')) {
            $this->updated_by = auth()->user()->id;
        }

        return parent::update($attributes, $options);
    }
}
