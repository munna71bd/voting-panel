<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Module\Cdc\Models\Cdc;

class LocDivision extends Model
{
    protected $connection = 'mysql_master';
    protected $table = 'loc_divisions';
    protected $guarded = [];

    public function cdcs()
    {
        return $this->hasMany(Cdc::class, 'division_id');
    }

    /**
     * Scope a query to only include the data access associate with the auth user.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAcl($query)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isSystemUser() || $user->isDataAccessAreaAllUser()) {
            return $query;
        }
        return $query->where('id', $user->loc_division_id);
    }
}
