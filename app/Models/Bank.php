<?php

namespace App\Models;


class Bank extends BaseModel
{
    protected $table = 'banks';

    protected $fillable = [
        'bank_name',
        'bank_type',
        'head_office',
        'phone',
        'mobile',
        'email',
        'swift_code',
        'row_status',
        'created_by',
        'updated_by',
    ];

}
