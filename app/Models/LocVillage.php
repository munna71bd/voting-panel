<?php

namespace App\Models;

use App\Events\LocVillageSaved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LocVillage extends Model
{
    protected $connection = 'mysql_master';

    public static function boot()
    {
        parent::boot();
        self::saving(function ($model) {
            $model->load(['locDivision', 'locDistrict', 'locUpazila']);
            $model->division_bbs_code = $model->locDivision->bbs_code;
            $model->district_bbs_code = $model->locDistrict->bbs_code;
            $model->upazila_bbs_code = $model->locUpazila->bbs_code;
        });
    }
    /**
     * The event map for the model.
     * @var array
     */
/*    protected $dispatchesEvents = [
        'saved' => LocVillageSaved::class,
    ];*/

    protected $table = 'loc_villages';
    protected $guarded = [];

    public function locDivision()
    {
        return $this->belongsTo(LocDivision::class, 'loc_division_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    public function locDistrict()
    {
        return $this->belongsTo(LocDistrict::class, 'loc_district_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    public function locUpazila()
    {
        return $this->belongsTo(LocUpazila::class, 'loc_upazila_id')
            ->select(['id', 'title_en', 'title']);
    }

    public function locUnion()
    {
        return $this->belongsTo(LocUnion::class, 'loc_union_id')
            ->select(['id', 'title_en', 'title']);
    }

    /**
     * Scope a query to only include the data access associate with the auth user.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAcl($query)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isSystemUser() || $user->isDataAccessAreaAllUser()) {
            return $query;
        }
        if ($user->isDistrictLevelUser()) {
            return $query->where('loc_district_id', $user->loc_district_id);
        }
        if ($user->isUpazillaLevelUser()) {
            return $query->where('loc_upazila_id', $user->loc_upazila_id);
        }
        if ($user->isUnionLevelUser()) {
            return $query->where('loc_union_id', $user->loc_union_id);
        }
        return $query->where('id', $user->loc_village_id);
    }

    public static function staticAcl($query, string $alias)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isSystemUser() || $user->isDataAccessAreaAllUser()) {
            return $query;
        }
        if ($user->isDistrictLevelUser()) {
            return $query->where($alias . 'loc_district_id', $user->loc_district_id);
        }
        if ($user->isUpazillaLevelUser()) {
            return $query->where($alias . 'loc_upazila_id', $user->loc_upazila_id);
        }
        if ($user->isUnionLevelUser()) {
            return $query->where($alias . 'loc_union_id', $user->loc_union_id);
        }
        return $query->where($alias . 'id', $user->loc_village_id);
    }
}
