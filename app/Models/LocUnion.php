<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class LocUnion extends Model
{
    protected $connection = 'mysql_master';
    protected $table = 'loc_unions';
    protected $guarded = [];

    public static function boot()
    {
        parent::boot();
        self::saving(function ($model) {
            $model->load(['locDivision', 'locDistrict', 'locUpazila']);
            $model->division_bbs_code = $model->locDivision->bbs_code;
            $model->district_bbs_code = $model->locDistrict->bbs_code;
            $model->upazila_bbs_code = $model->locUpazila->bbs_code;
        });
    }

    public function locDivision()
    {
        return $this->belongsTo(LocDivision::class, 'loc_division_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    public function locDistrict()
    {
        return $this->belongsTo(LocDistrict::class, 'loc_district_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    public function locUpazila()
    {
        return $this->belongsTo(LocUpazila::class, 'loc_upazila_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    /**
     * Scope a query to only include the data access associate with the auth user.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAcl($query)
    {
        /** @var User $user */
        $user = Auth::user();

        return $query;
    }

    public static function staticAcl($query, string $alias)
    {
        /** @var User $user */
        $user = Auth::user();

        return $query;
    }
}
