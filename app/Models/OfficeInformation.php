<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;

class OfficeInformation extends BaseModel
{
    const OFFICE_TYPE_MINISTRY = 1;
    const OFFICE_TYPE_HQ = 2;
    const OFFICE_TYPE_DIVISION = 3;
    const OFFICE_TYPE_REGIONAL = 4;
    const OFFICE_TYPE_ZILA = 5;
    const OFFICE_TYPE_UPAZILA = 6; // Upazila/City/Thana,
    const OFFICE_TYPE_UNION = 7; // Union/Porashava/Ward

    public static function getOfficeTypeOptions()
    {
        return [
            '' => __('generic.select_office_type'),
            self::OFFICE_TYPE_MINISTRY => __("generic.ministry"),
            self::OFFICE_TYPE_HQ => __("generic.hq"),
            self::OFFICE_TYPE_DIVISION => __("generic.division"),
            self::OFFICE_TYPE_REGIONAL => __("generic.regional"),
            self::OFFICE_TYPE_ZILA => __("generic.district"),
            self::OFFICE_TYPE_UPAZILA => __("generic.upazila"),
            self::OFFICE_TYPE_UNION => __("generic.union")
        ];
    }

    protected $table = 'office_informations';
    protected $fillable = [
        'title_en', 'title_bn', 'loc_division_id', 'loc_district_id', 'loc_upazila_id',
        'loc_union_id', 'parent_office_id', 'address', 'contact_phone', 'contact_email',
        'contact_fax', 'contact_mobile', 'office_type', 'row_status', 'created_by', 'updated_by'
    ];

    public function locDivision()
    {
        return $this->belongsTo(LocDivision::class, 'loc_division_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    public function locDistrict()
    {
        return $this->belongsTo(LocDistrict::class, 'loc_district_id')
            ->select(['id', 'title_en', 'title', 'bbs_code']);
    }

    public function locUpazila()
    {
        return $this->belongsTo(LocUpazila::class, 'loc_upazila_id')
            ->select(['id', 'title_en', 'title']);
    }

    public function locUnion()
    {
        return $this->belongsTo(LocUnion::class, 'loc_union_id')
            ->select(['id', 'title_en', 'title']);
    }

    /**
     * Scope a query to only include the data access associate with the auth user.
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAcl($query)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isSystemUser() || $user->isDataAccessAreaAllUser()) {
            return $query;
        }
        if ($user->isDistrictLevelUser()) {
            return $query->where('loc_district_id', $user->loc_district_id);
        }
        if ($user->isUpazillaLevelUser()) {
            return $query->where('loc_upazila_id', $user->loc_upazila_id);
        }
        return $query->where('loc_union_id', $user->loc_union_id);
    }

    public static function staticAcl($query, string $alias)
    {
        /** @var User $user */
        $user = Auth::user();

        if ($user->isSystemUser() || $user->isDataAccessAreaAllUser()) {
            return $query;
        }
        if ($user->isDistrictLevelUser()) {
            return $query->where($alias . 'loc_district_id', $user->loc_district_id);
        }
        if ($user->isUpazillaLevelUser()) {
            return $query->where($alias . 'loc_upazila_id', $user->loc_upazila_id);
        }
        return $query->where($alias . 'loc_union_id', $user->loc_union_id);
    }
}
