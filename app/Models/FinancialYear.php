<?php

namespace App\Models;


class FinancialYear extends BaseModel
{

    protected $table = 'financial_years';

    protected $fillable = [
        'title',
        'row_status',
        'second_year',
        'first_year',
        'created_by',
        'updated_by'
    ];
}
