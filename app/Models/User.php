<?php

namespace App\Models;

use App\Voyager\Contracts\User as UserContract;
use App\Voyager\Traits\VoyagerUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Facades\Voyager;
use Module\Cluster\Models\Notification;
use Module\Cdc\Models\Cdc;
use Module\Notification\Models\Department;
use Module\Notification\Models\Group;
use Module\Notification\Models\UserGroup;
use Module\Scg\Models\Scg;
use Module\Scm\Models\Scm;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable implements UserContract
{
    protected $connection = 'mysql_master';
    use Notifiable, VoyagerUser;


    protected $guarded = [];
    public $additional_attributes = ['locale'];

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $dbConnection = null;


    public function hasType(...$types)
    {
        return in_array($this->data_access_area, $types, true);
    }

    public function getAvatarAttribute($value)
    {
        return $value ?? config('voyager.user.default_avatar', 'users/default.png');
    }

    public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = Carbon::parse($value)->format('Y-m-d H:i:s');
    }

    public function setSettingsAttribute($value)
    {
        $this->attributes['settings'] = $value->toJson();
    }

    public function getSettingsAttribute($value)
    {
        return collect(json_decode($value));
    }

    public function setLocaleAttribute($value)
    {
        $this->settings = $this->settings->merge(['locale' => $value]);
    }

    public function getLocaleAttribute()
    {
        return $this->settings->get('locale');
    }

    /**
     * @param $query
     * @return Builder
     */
    public function scopeAcl($query)
    {
        /**  @var Builder $query */
        /** @var \App\Models\User $authUser */

        $authUser = Auth::user();
        return $query;
    }

    public static function staticAclScope($query, $alias = "")
    {
        /**  @var Builder $query */
        /** @var \App\Models\User $authUser */

        $authUser = Auth::user();
        return $query;
    }


    public function getDbConnection($cid = 0)
    {
        $this->dbConnection = "mysql_master";
        return $this->dbConnection;
    }

    /**
     * users custom permission start
     */

    public function activePermissions()
    {
        return $this->hasMany(UsersPermission::class)->where('status', true)->with('permission');
    }

    public function inActivePermissions()
    {
        return $this->hasMany(UsersPermission::class)->where('status', false)->with('permission');
    }

    public function role()
    {
        return $this->belongsTo(Voyager::modelClass('Role'));
    }

    public function loadRolesRelations()
    {
        if (!$this->relationLoaded('role')) {
            $this->load('role');
        }

        if (!$this->relationLoaded('roles')) {
            $this->load('roles');
        }

        if (!$this->relationLoaded('activePermissions')) {
            $this->load('activePermissions');
        }

        if (!$this->relationLoaded('inActivePermissions')) {
            $this->load('inActivePermissions');
        }
    }

    public function loadPermissionsRelations()
    {
        $this->loadRolesRelations();

        if ($this->role && !$this->role->relationLoaded('permissions')) {
            $this->role->load('permissions');
            $this->load('roles.permissions');
        }
    }

    public function hasPermission($name)
    {
        $this->loadPermissionsRelations();

        return $this->roles_all()->contains($name);
    }

    /**
     * @return Collection
     */
    public function allPermissionKey()
    {
        $this->loadRolesRelations();
        $inactiveKeys = $this->getInactivePermissionKeys();

        $activeKeys = $this->getActivePermissionKeys();

        $defaultRolekeys = $this->role->permissions()->pluck('key')->concat($activeKeys);

        return $this->roles()->get()
            ->map(function ($additionalRole) {
                return $additionalRole->permissions()->pluck('key')->all();
            })
            ->flatten()
            ->concat($defaultRolekeys)
            ->diff($inactiveKeys)
            ->unique();
    }

    public function getCustomPermissions()
    {
        return collect([$this->getActivePermissionKeys()])->merge($this->getInactivePermissionKeys())->flatten();
    }

    public function getActivePermissionKeys()
    {
        return $this->activePermissions()->get()->map(function ($userPermission) {
            return $userPermission->permission->key;
        });
    }

    public function getInactivePermissionKeys()
    {
        return $this->inActivePermissions()->get()->map(function ($userPermission) {
            return $userPermission->permission->key;
        });
    }

    public function roles_all()
    {
        return Cache::rememberForever('userwise_permissions_' . auth()->id(), function () {
            return $this->allPermissionKey();
        });
    }


    public function save(array $options = [])
    {
        if(auth()->user()) {
            if ($this->getAttribute('id')) {
                $this->updated_by = auth()->user()->id;
            } else {
                $this->created_by = auth()->user()->id;
            }

        }

        return parent::save($options);
    }

    public function update(array $attributes = [], array $options = [])
    {
        $this->updated_by = auth()->user()->id;
        return parent::update($attributes, $options);
    }

    public function userGroup()
    {
        return $this->hasOne(UserGroup::class);
    }

}
