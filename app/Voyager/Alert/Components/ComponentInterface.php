<?php

namespace App\Voyager\Alert\Components;

interface ComponentInterface
{
    public function render();
}
