<?php


namespace App\Migrations;

use Illuminate\Database\Migrations\Migration as BaseMigration;

class Migration extends BaseMigration
{

    public function getFederatedConnectionString($defaultConnName = null)
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            return $this->fedLinkForWindows($defaultConnName);
        } else {
            return $this->fedLinkForUbuntu();
        }
    }

    protected function fedLinkForUbuntu()
    {
        return "fedlink";
    }

    protected function fedLinkForWindows($defaultConnName = null)
    {
        $connections = config('database.mysql_connections');
        $defaultConnName = is_null($defaultConnName) ? config('database.default') : $defaultConnName;
        $federatedConnection = null;
        if (!empty($connections[$defaultConnName])) {
            $masterDbConfig = $connections[$defaultConnName];
            $federatedConnection = "mysql://" . urlencode($masterDbConfig['username']);
            if ($masterDbConfig['password'] !== '') {
                $federatedConnection .= ":" . urlencode($masterDbConfig['password']);
            }
            $federatedConnection .= "@" . $masterDbConfig['host'] . ":" . $masterDbConfig['port'] . "/" . $masterDbConfig['database'];
            return $federatedConnection;
        }
        return false;
    }
}
