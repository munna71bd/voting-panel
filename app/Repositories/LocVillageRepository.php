<?php


namespace App\Repositories;


use App\Models\LocDistrict;
use App\Models\LocVillage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LocVillageRepository
{

    public function updateToDistrictDbs(LocVillage $village)
    {
        if ($village->relationLoaded('locDistrict')) {
            $village->load('locDistrict');
        }
        if (!$village->locDistrict) {
            return;
        }
        $districtBbsCode = $village->locDistrict->bbs_code;

        $connections = config('database.mysql_connections');
        $villageData = $village->toArray();
        if (!empty($villageData['loc_district'])) {
            unset($villageData['loc_district']);
        }
        foreach ($connections as $connection => $config) {
            if ($connection === 'mysql_master') {
                continue;
            }
            if (empty($config['district_bbs'])) {
                continue;
            }
            if ($districtBbsCode == $config['district_bbs']) {
                $dbConnection = DB::connection($connection)->table($village->getTable());
                $dbConnection->updateOrInsert(
                    [
                        'id' => $village->id
                    ],
                    $villageData
                );
            }

        }
    }
}
