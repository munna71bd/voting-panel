<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Bank;

class BankRepository
{

    /**
     * @param array $data
     * @param null $id
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data, $id = null): \Illuminate\Contracts\Validation\Validator
    {
        $rules = [
            'bank_name'   => 'required|string|unique:banks,bank_name,'. $id,
            'bank_type'   => 'required|numeric',
            'head_office' => 'required|string',
            'mobile'      => 'required|digits:11',
            'phone'       => 'required|digits:11',
            'email'       => 'required|email',
            'swift_code'  => 'nullable|string',
        ];

        return Validator::make($data, $rules);
    }

    /**
     * @return Bank[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {
        return DB::table('banks')
            ->select('id', 'bank_name', 'bank_type', 'mobile', 'email', 'swift_code', 'created_by', 'created_at')
            ->get();
    }
    /**
     * @param array $data
     * @return Bank
     */
    public function create(array $data): Bank
    {
        return Bank::create($data);
    }

    /**
     * @param array $data
     * @param Bank $bank
     * @return bool
     */
    public function update(array $data, $bank): bool
    {
        return $bank->update($data);
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return app(Bank::class);
    }
}
