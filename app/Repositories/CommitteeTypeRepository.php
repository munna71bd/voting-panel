<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\CommitteeType;

class CommitteeTypeRepository
{

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data, $id = null): \Illuminate\Contracts\Validation\Validator
    {
        $rules = [
            'code'   => 'required|string|unique:committee_types,code,'. $id,
            'title'   => 'required|string'
        ];

        return Validator::make($data, $rules);
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function index()
    {
        return DB::table('committee_types')
                ->select('id', 'title', 'code', 'row_status', 'created_by', 'created_at')
                ->get();
    }

    /**
     * @param array $data
     * @return CommitteeType
     */
    public function create(array $data): CommitteeType
    {
        return CommitteeType::create($data);
    }

    /**
     * @param array $data
     * @param $committeeType
     * @return bool
     */
    public function update(array $data, CommitteeType $committeeType): bool
    {
        return $committeeType->update($data);
    }


    public function destroy(CommitteeType $committeeType)
    {
        return $committeeType->update(['row_status' => 2]);
    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return app(CommitteeType::class);
    }
}
