<?php


namespace App\Repositories;


use App\Facades\Voyager;
use App\Models\User;
use App\Models\UsersPermission;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use Module\Cdc\Models\Cdc;
use Module\Notification\Models\UserGroup;
use Module\Scg\Models\Scg;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Input;

class UserRepository
{
    public function storeValidation(Request $request, User $user): \Illuminate\Contracts\Validation\Validator
    {
        $data = $request->input();
        $usernameMinLen = (int)setting('admin.username_min_len');
        $passwordMinLen = (int)setting('admin.password_min_len');

        /** @var User $authUser */
        $authUser = Auth::user();
        $rules = [
            'name' => 'required',

            'username' => [
                'required',
                'string',
                'min:' . $usernameMinLen,
                $user->exists ? Rule::unique($user->getTable())->ignore($user->id) : Rule::unique($user->getTable()),

            ],

            'password' => $user->exists ? [
                'nullable',
                'confirmed',
                'min:' . $passwordMinLen,
                function ($attribute, $value, $fail) {
                    if (empty($value)) {
                        return true;
                    }
                    $isValid = true;
                    if (!preg_match('/[\d]/', $value, $output_array)) {
                        $isValid = false;
                    } else if (!preg_match('/[a-z]/', $value, $output_array)) {
                        $isValid = false;
                    } else if (!preg_match('/[A-Z]/', $value, $output_array)) {
                        $isValid = false;
                    }
                    if (!$isValid) {
                        $fail(__("front_vallidations.user.password.password_validate"));
                    }
                }
            ] : [
                'required',
                'confirmed',
                'min:' . $passwordMinLen,
                function ($attribute, $value, $fail) {
                    $isValid = true;
                    if (!preg_match('/[\d]/', $value, $output_array)) {
                        $isValid = false;
                    } else if (!preg_match('/[a-z]/', $value, $output_array)) {
                        $isValid = false;
                    } else if (!preg_match('/[A-Z]/', $value, $output_array)) {
                        $isValid = false;
                    }
                    if (!$isValid) {
                        $fail(__("front_vallidations.user.password.password_validate"));
                    }
                }
            ],

        ];

        $customAttributes = [];

        $v = Validator::make($data, $rules, [], $customAttributes);


        return $v;
    }

    public function attachScope(Builder $query, $scope)
    {
        if (method_exists($this->newUser(), 'scope' . ucfirst($scope))) {
            $query = $query->{$scope}();
        }
        return $query;
    }

    public function newUser(): User
    {
        return app(User::class);
    }

    /**
     * @param $id
     * @param null $scope
     * @return User
     */
    public function getUserById($id, $scope = null): User
    {
        $query = User::where('id', $id)->with([]);
        if (!is_null($scope)) {
            $query = $this->attachScope($query, $scope);
        }
        return $query->firstOrFail();
    }

    public function uploadAvatar(Request $request, User $user)
    {
        if ($request->hasFile('avatar')) {
            $image = $request->file('avatar');
            $name = 'users/' . time() . '.' . $image->getClientOriginalExtension();
            $destinationPath = storage_path('app/public/users');
            $image->move($destinationPath, $name);
            $user->avatar = $name;
        }
    }

    public function saveUser(Request $request, User $user)
    {
        /** @var User $authUser */
        $authUser = Auth::user();

        $inputs = $request->input();
        $attributes = [
            'name', 'username', 'email',
        ];
        $sensitiveAttribute = [
        ];
        if ($authUser->id != $user->id) {
            $attributes = array_merge($attributes, $sensitiveAttribute);
        }

        if (!$user->exists || $authUser->can('editRoles', $user)) {
            $attributes[] = 'role_id';
        }
        if (!$user->exists || $authUser->can('changeUserStatus', $user)) {
            $attributes[] = 'row_status';
        }


        foreach ($inputs as $field => $value) {
            if (in_array($field, $attributes)) {
                $user->{$field} = $value;
            }
        }


        $this->uploadAvatar($request, $user);
        if ($request->password) {
            $user->password = Hash::make($request->password);
        }

        $user->save();


        if ($request->department_id && !$request->group_id) {
            $data = [
                'user_id' => $user->id,
                'department_id' => $request->department_id,

            ];
            $checkDpt = UserGroup::where('user_id', $user->id)->update($data);
            if(!$checkDpt) {
                UserGroup::insert($data);
            }

        }


        if ($request->department_id && $request->group_id) {
            $data = [
                'user_id' => $user->id,
                'department_id' => $request->department_id,
                'group_id' => $request->group_id,

            ];
            $checkDpt = UserGroup::where('user_id', $user->id)->update($data);
            if(!$checkDpt) {
                UserGroup::insert($data);
            }

        }


        if (!$user->exists && $request->user_belongstomany_role_relationship) {
            $user->roles()->attach($request->user_belongstomany_role_relationship);
        } else if ($user->exists && $authUser->can('editRoles', $user) && $request->user_belongstomany_role_relationship) {
            $user->roles()->sync($request->user_belongstomany_role_relationship);
        }
    }

    public function userPermissionValidator(Request $request)
    {
        return $request->validate([
            'user_id' => 'required|integer',
            'permission_id' => 'required|integer',
        ]);
    }

    public function changeUserPermission(Request $request)
    {
        $data = $request->only(['user_id', 'permission_id']);
        $checkboxStatus = !empty($request->checkboxStatus) && $request->checkboxStatus == 'true' ? 1 : 0;

        Cache::forget('userwise_permissions_' . $data['user_id']);

        return UsersPermission::updateOrCreate(
            $data,
            ['status' => $checkboxStatus]
        );
    }

    public function userPermissionsValidator(Request $request)
    {
        return $request->validate([
            'user_id' => 'required|integer',
            'permission_ids' => 'required|array|min:1',
        ]);
    }

    public function changeUserPermissions(Request $request)
    {
        $data = $request->only(['user_id', 'permission_ids']);
        $checkboxStatus = !empty($request->checkboxStatus) && $request->checkboxStatus == 'true' ? 1 : 0;
        $userId = $data['user_id'];
        $collection = collect($data['permission_ids'])->unique()
            ->filter(function ($element) {
                $element = is_scalar($element) ? (int)$element : $element;
                return is_integer($element);
            })->map(function ($permissionId) use ($userId) {
                return ['permission_id' => $permissionId, 'user_id' => $userId];
            });

        /** @var $collection Collection */

        $count = 0;
        if ($collection->isNotEmpty()) {
            Cache::forget('userwise_permissions_' . $userId);
            $collection->each(function ($entity) use ($checkboxStatus, &$count) {
                $userPermission = UsersPermission::updateOrCreate(
                    $entity,
                    ['status' => $checkboxStatus]
                );
                if (!empty($userPermission) && $userPermission->id) {
                    $count++;
                }
            });
        }
        return ($collection->isEmpty() || $collection->count() != $count);
    }

    public function getDataTableData(Request $request)
    {
        /** @var User $user */
        $authUser = Auth::user();

        $fixed_roles = [1, 6];
        if (auth()->user()->role_id == 6) {
            $fixed_roles = [1, 6];
        } else if (auth()->user()->role_id == 7) {
            $fixed_roles = [1, 6, 7];
        } else if (auth()->user()->role_id == 3) {
            $fixed_roles = [1, 6, 7, 3];
        } else if (auth()->user()->role_id == 1) {
            $fixed_roles = [];
        }

        /** @var Builder $users */
        $users = User::select([
            'users.id as id',
            'users.name',
            'users.username',
            'users.cell_phone',
            'users.email',
            'users.avatar',
            'users.created_at',
            'users.updated_at',
            'roles.display_name as role_name',
        ]);

        /** relations */
        $users->leftJoin('roles', 'users.role_id', '=', 'roles.id');


        /** check access control */
        $rc = " 1=1";

        $users = User::staticAclScope($users, $alias = "users.")->whereNotIn('users.role_id', $fixed_roles)->whereRaw($rc);

        /** action buttons */
        return DataTables::eloquent($users)
            ->addColumn('avatar', function (User $user) {
                return '<img src="' . Voyager::image($user->avatar) . '" border="0" width="40" class="img-rounded" align="center" />';
            })
            ->addColumn('action', function (User $user) use ($authUser) {
                $str = "";
                if ($authUser->can('read', $user)) {
                    // $str .= ' <a class="btn btn-sm btn-warning view" href="' . route('voyager.users.show', $user->id) . '"><i class="voyager-eye"></i> ' . __('voyager::generic.view') . '</a>';
                }

                if ($authUser->can('edit', $user)) {
                    $str .= ' <a class="btn btn-sm btn-primary edit" href="' . route('voyager.users.edit', $user->id) . '"><i class="voyager-edit"></i> ' . __('voyager::generic.edit') . '</a>';
                }
                return $str;
            })
            ->rawColumns(['avatar', 'action'])
            ->toJson();
    }
}
