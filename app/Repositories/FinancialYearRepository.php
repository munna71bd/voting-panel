<?php


namespace App\Repositories;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\FinancialYear;

class FinancialYearRepository
{

    /**
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validator(array $data, $id = null): \Illuminate\Contracts\Validation\Validator
    {
        $rules = [
            'title' => 'required|string|unique:financial_years,title,'. $id,
        ];

        return Validator::make($data, $rules);
    }

    public function index()
    {
        return DB::table('financial_years')
            ->select('id', 'title','row_status', 'created_by', 'created_at')
            ->get();
    }

    /**
     * @param array $data
     * @return FinancialYear
     */
    public function create(array $data): FinancialYear
    {
        return FinancialYear::create($data);
    }

    /**
     * @param array $data
     * @param $financialYear
     * @return bool
     */
    public function update(array $data, FinancialYear $financialYear): bool
    {
        return $financialYear->update($data);
    }

    /**
     * @param FinancialYear $financialYear
     */
    public function destroy(FinancialYear $financialYear)
    {

    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return app(FinancialYear::class);
    }


}
