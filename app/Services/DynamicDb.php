<?php

namespace App\Services;

use RuntimeException;
use DB;
use App\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Artisan;


class DynamicDb
{


    /**
     * @param string $source
     * @param string $target
     */


    /**
     * @param array $replacements
     *
     * @return false|int
     * @throws \RuntimeException
     */
    //Artisan::call('migrate', array('database' => $databaseConnection, 'path' => 'app/database/tenants'));
    function createSchema($schemaName)
    {
        // We will use the `statement` method from the connection class so that
        // we have access to parameter binding.
        try {
            DB::connection()->statement( 'CREATE DATABASE IF NOT EXISTS ' . $schemaName );
            return true;
        } catch (\Throwable $exception) {
            return false;
        }
    }

    function DropSchema($schemaName)
    {
        // We will use the `statement` method from the connection class so that
        // we have access to parameter binding.
        try {
            DB::connection()->statement( 'DROP DATABASE IF  EXISTS ' . $schemaName );
            return true;
        } catch (\Throwable $exception) {
            return false;
        }
    }


    public function UpdateConfigFile($dbName)
    {
        $configData = "
            '" . $dbName . "' => [
                'host' => env('DB_HOST', 'localhost'),
                'port' => env('DB_PORT', '3306'),
                'master_db' => false,
                'district_bbs' => null,
                'database' => '" . $dbName . "',
                'username' => env('DB_USERNAME', 'root'),
                'password' => env('DB_PASSWORD', ''),
        ],
        ";
        try {
            $rootPath = dirname( dirname( __FILE__ ) );
            $rootPath = substr( $rootPath, 0, strlen( $rootPath ) - 3 );
            $rootPath .= 'config/';

            $filecontent = file_get_contents( $rootPath . 'database.php' );


            $pos = strpos( $filecontent, '];' );
            $filecontent = substr( $filecontent, 0, $pos ) . "\r\n" . $configData . "\r\n" . substr( $filecontent, $pos );
            file_put_contents( $rootPath . 'database.php', $filecontent );


        } catch (\Throwable $exception) {
            return false;
        }
        return true;
    }

    function createDynamicTable($type, $id, $cluster_id = 0)
    {

        Artisan::call( 'config:clear' );
        Artisan::call( 'config:cache' );

        if ( $cluster_id == 0 && $type != 'notification' ) {

            $cluster_id = auth()->user()->cluster_id;
        }


        if ( $type == 'notification' ) {

            $this->createDynamicScmpTable( "mysql_cluster_" . $id );
            $this->createIncomeExpenditureStatementTable( "mysql_cluster_" . $id );
            $this->createProfitCalculationTable( "mysql_cluster_" . $id );
            $this->createProfitCalculationCdcTable( "mysql_cluster_" . $id );
            $this->createProfitCalculationScgTable( "mysql_cluster_" . $id );
            $this->createBalanceSheetTable( "mysql_cluster_" . $id );
            $this->createCashbookBalanceTable( "mysql_cluster_" . $id );


            /*
            $sql = config( 'sql' );

            foreach ($sql as $query) {
                DB::connection( 'mysql_cluster_' . $id )->select( $query );
            }
            */


            return true;
        } else if ( $type == 'cdc' ) {

            $connection = "mysql_cluster_" . $cluster_id;
            $year = date( "Y" );

            $this->createDynamicCdcBalanceTable( $connection, $id, null );
            $this->createDynamicCdcLoanBalanceTable( $connection, $id, null );
            $this->createDynamicCdcTeamLoanBalanceSummaryTable( $connection, $id, null );
            $this->createDynamicCdcCashbookTable( $connection, $id );

            return true;

        } else if ( $type == 'scg' ) {

            $connection = "mysql_cluster_" . $cluster_id;
            $year = date( "Y" );

            $this->createDynamicTransactionTable( $connection, $id, $year );
            $this->createDynamicTeamBalanceTable( $connection, $id, $year );
            $this->createDynamicLoanApplicationTable( $connection, $id );
            $this->createDynamicLoanScheduleTable( $connection, $id );
            $this->createDynamicTeamLoanBalanceTable( $connection, $id, $year );

            return true;
        }
    }


    public function createDynamicScmpTable($con)
    {
        $tbl = 'scmps';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('scmps', function (Blueprint $table) {
                    $table->increments('id');

                    $table->unsignedInteger('scg_id');

                    $table->string('member_code', 255);
                    $table->string('member_name_en', 255);
                    $table->string('member_name_bn', 255);
                    $table->string('member_dob')->nullable();
                    $table->string('member_picture', 50)->nullable();
                    $table->string('fathers_name', 255)->nullable();
                    $table->string('mothers_name', 255)->nullable();
                    $table->string('spouse_name', 255)->nullable();
                    $table->string('member_cell_no', 15)->nullable();

                    $table->string('gender', 20)->default('male')->nullable();
                    $table->string('religion', 20)->default('islam')->nullable();
                    $table->string('marital_status', 20)->default('unmarried')->nullable();

                    $table->string('education', 255)->nullable();
                    $table->string('profession', 255)->nullable();
                    $table->string('amount1', 255)->nullable();
                    $table->string('amount2', 255)->nullable();

                    $table->date('active_date')->nullable();
                    $table->date('deactive_date')->nullable();


                    $table->text('present_address')->nullable();
                    $table->text('permanent_address')->nullable();
                    $table->text('extra')->nullable();
                    $table->unsignedInteger('total_loan_number')->default(0);
                    $table->string('starting_saving_month', 20)->nullable;
                    $table->string('member_nid', 20)->default('pg');

                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }


    public function createDynamicTransactionTable($con, $team_id, $year)
    {
        $tbl = 'team_' . $team_id . '_transactions_';
        if(!Schema::connection( $con )->hasTable($tbl)) {

            Schema::connection($con)->create('team_' . $team_id . '_transactions_', function ($table) {

                $table->increments('id');
                $table->unsignedInteger('cluster_id');
                $table->unsignedInteger('cdc_id');
                $table->unsignedInteger('scg_id');
                $table->unsignedInteger('scmp_id');

                $table->string('tran_no', 50);
                $table->double('amount');
                $table->string('tran_type', 50)->default('Saving');

                $table->date('tran_date');
                $table->string('tran_month')->default('01');
                $table->string('tran_year')->default('2020');
                $table->unsignedInteger('cdc_approve_by');
                $table->date('cdc_approve_date');
                $table->unsignedInteger('cluster_approve_by');
                $table->date('cluster_approve_date');
                $table->text('remarks')->nullable();
                $table->unsignedInteger('loan_application_id')->default(0);


                $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                    ->default(1)
                    ->comment("1 Active, 0 Deactivate, 2 Deleted");

                $table->timestamps();
                $table->softDeletes();

            });
        }
    }

    public function createDynamicTeamBalanceTable($con, $team_id, $year)
    {
        $tbl = 'team_' . $team_id . '_balances_';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('team_' . $team_id . '_balances_', function ($table) {

                    $table->increments('id');

                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('scg_id');
                    $table->unsignedInteger('scmp_id');

                    $table->double('initial_balance');
                    $table->double('amount_week_1');
                    $table->double('amount_week_2');
                    $table->double('amount_week_3');
                    $table->double('amount_week_4');
                    $table->double('amount_week_5');
                    $table->double('total_collected_amount');
                    $table->double('profit_amount');
                    $table->double('withdrawal_amount');
                    $table->double('closing_balance');

                    $table->string('month');
                    $table->string('year');


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();
                    $table->unsignedInteger('approved_by')->default(0);
                    $table->string('approved_by_name')->nullable();

                });
        }
    }

    public function createDynamicCdcBalanceTable($con, $cdc_id, $year)
    {
        $tbl = 'cdc_' . $cdc_id . '_balances_';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('cdc_' . $cdc_id . '_balances_', function ($table) {

                    $table->increments('id');

                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('scg_id');
                    $table->unsignedInteger('no_of_member');
                    $table->double('initial_balance');
                    $table->double('amount_week_1');
                    $table->double('amount_week_2');
                    $table->double('amount_week_3');
                    $table->double('amount_week_4');
                    $table->double('amount_week_5');
                    $table->double('total_collected_amount');
                    $table->double('profit_amount');
                    $table->double('withdrawal_amount');
                    $table->unsignedInteger('no_of_withdrawal_member');
                    $table->double('closing_balance');

                    $table->string('month');
                    $table->string('year');


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                    $table->unsignedInteger('approved_by')->default(0);
                    $table->string('approved_by_name')->nullable();

                });
        }
    }

    public function createDynamicLoanApplicationTable($con, $team_id)
    {
        $tbl = 'team_' . $team_id . '_loan_applications';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('team_' . $team_id . '_loan_applications', function ($table) {

                    $table->increments('id');
                    $table->Integer('scmp_id');
                    $table->Integer('cluster_id');
                    $table->Integer('cdc_id');
                    $table->Integer('scg_id');
                    $table->double('applied_amount');
                    $table->double('approved_amount');
                    $table->double('service_charge_rate');
                    $table->double('service_charge');
                    $table->date('application_date');
                    $table->string('loan_purpose');
                    $table->string('loan_duration');
                    $table->string('installment_type');
                    $table->string('grace_period');
                    $table->string('status');
                    $table->date('status_date');
                    $table->string('loan_type')->default('');

                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }

    public function createDynamicLoanScheduleTable($con, $team_id)
    {
        $tbl = 'team_' . $team_id . '_loan_schedules';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('team_' . $team_id . '_loan_schedules', function ($table) {

                    $table->increments('id');
                    $table->Integer('scmp_id');
                    $table->Integer('cluster_id');
                    $table->Integer('cdc_id');
                    $table->Integer('scg_id');
                    $table->double('amount');
                    $table->double('loan_principal')->default(0);
                    $table->double('service_charge');
                    $table->string('installment_no');
                    $table->string('installment_date');
                    $table->string('status');
                    $table->date('collection_date');
                    $table->Integer('collected_by');
                    $table->Integer('loan_application_id');

                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }

    public function createDynamicTeamLoanBalanceTable($con, $team_id, $year)
    {
        $tbl = 'team_' . $team_id . '_loan_balances_';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('team_' . $team_id . '_loan_balances_', function ($table) {

                    $table->increments('id');

                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('scg_id');
                    $table->unsignedInteger('scmp_id');

                    $table->decimal('initial_balance', 20, 3)->default(0);
                    $table->unsignedBigInteger('total_installment')->default(0);
                    $table->decimal('amount_week_1', 20, 3)->default(0);
                    $table->decimal('amount_week_2', 20, 3)->default(0);
                    $table->decimal('amount_week_3', 20, 3)->default(0);
                    $table->decimal('amount_week_4', 20, 3)->default(0);
                    $table->decimal('amount_week_5', 20, 3)->default(0);
                    $table->decimal('total_collected_amount', 20, 3)->default(0);
                    $table->unsignedBigInteger('remaining_installment')->default(0);
                    $table->decimal('nett_collection', 20, 3)->default(0);
                    $table->decimal('service_charge', 20, 3)->default(0);
                    $table->decimal('expired_collection', 20, 3)->default(0);
                    $table->decimal('advance_collection', 20, 3)->default(0);
                    $table->decimal('closing_balance', 20, 3)->default(0);

                    $table->string('month');
                    $table->string('year');


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                    $table->unsignedInteger('approved_by')->default(0);
                    $table->string('approved_by_name')->nullable();

                });
        }
    }

    public function createDynamicCdcLoanBalanceTable($con, $cdc_id, $year)
    {
        $tbl = 'cdc_' . $cdc_id . '_loan_balances_';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('cdc_' . $cdc_id . '_loan_balances_', function ($table) {

                    $table->increments('id');

                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('scg_id');
                    $table->unsignedBigInteger('no_of_member')->default(0);

                    $table->decimal('initial_balance', 20, 3)->default(0);
                    $table->decimal('disburse_running', 20, 3)->default(0);
                    $table->decimal('disburse_total', 20, 3)->default(0);
                    $table->unsignedBigInteger('disburse_member_running')->default(0);
                    $table->unsignedBigInteger('disburse_member_total')->default(0);
                    $table->decimal('collection_target_running', 20, 3)->default(0);
                    $table->decimal('collection_amount_running', 20, 3)->default(0);
                    $table->decimal('service_charge_running', 20, 3)->default(0);
                    $table->decimal('collection_amount_total', 20, 3)->default(0);
                    $table->unsignedBigInteger('collection_members_running')->default(0);
                    $table->decimal('closing_balance', 20, 3)->default(0);
                    $table->decimal('nett_loan_amount', 20, 3)->default(0);
                    $table->unsignedBigInteger('nett_member')->default(0);
                    $table->decimal('expired_installment_amount', 20, 3)->default(0);
                    $table->decimal('expired_installment_member', 20, 3)->default(0);

                    $table->string('month');
                    $table->string('year');

                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                    $table->unsignedInteger('approved_by')->default(0);
                    $table->string('approved_by_name')->nullable();

                });
        }
    }

    public function createDynamicCdcTeamLoanBalanceSummaryTable($con, $cdc_id, $year)
    {
        $tbl = 'cdc_' . $cdc_id . '_team_loan_balances_summaries_';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('cdc_' . $cdc_id . '_team_loan_balances_summaries_', function ($table) {

                    $table->increments('id');

                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('scg_id');
                    $table->unsignedBigInteger('no_of_member')->default(0);

                    $table->decimal('initial_balance', 20, 3)->default(0);
                    $table->unsignedBigInteger('total_installment')->default(0);
                    $table->decimal('amount_week_1', 20, 3)->default(0);
                    $table->decimal('amount_week_2', 20, 3)->default(0);
                    $table->decimal('amount_week_3', 20, 3)->default(0);
                    $table->decimal('amount_week_4', 20, 3)->default(0);
                    $table->decimal('amount_week_5', 20, 3)->default(0);
                    $table->decimal('total_collected_amount', 20, 3)->default(0);
                    $table->unsignedBigInteger('remaining_installment')->default(0);
                    $table->decimal('nett_collection', 20, 3)->default(0);
                    $table->decimal('service_charge', 20, 3)->default(0);
                    $table->decimal('expired_collection', 20, 3)->default(0);
                    $table->decimal('advance_collection', 20, 3)->default(0);
                    $table->decimal('closing_balance', 20, 3)->default(0);

                    $table->string('month');
                    $table->string('year');


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }

    public function createDynamicCdcCashbookTable($con, $cdc_id)
    {
        $tbl = 'cdc_' . $cdc_id . '_cashbooks';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('cdc_' . $cdc_id . '_cashbooks', function ($table) {

                    $table->increments('id');

                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->string('t_type', 20)->nullable();

                    $table->date('transaction_date')->nullable();
                    $table->char('transaction_type', 30)->comment("bank_deposit, bank_withdraw, collection, expense, pay");
                    $table->char('transaction_sector', 30)
                        ->comment("cash, bank_interest, bank_service_charge, loan_installment, service_charge, saving, onudan, fine, loan_form_sell, office_expense, academic_expense, other_expense, loan_amount, profit");
                    $table->char('transaction_media', 30)->comment("cash, bank");
                    $table->decimal('amount', 20, 3)->default(0);
                    $table->string('voucher_no', 199)->nullable();
                    $table->text('remark')->nullable();
                    $table->decimal('cash_in_hand', 20, 3)->default(0);
                    $table->decimal('cash_in_bank', 20, 3)->default(0);
                    $table->string('month', 10);
                    $table->string('year', 10);
                    $table->unsignedInteger('cdc_approve_by')->nullable();
                    $table->date('cdc_approve_date')->nullable();
                    $table->unsignedInteger('cashier_approve_by')->nullable();
                    $table->date('cashier_approve_date')->nullable();

                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->unsignedInteger('created_by')->nullable();
                    $table->unsignedInteger('updated_by')->nullable();
                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }

    public function createCashbookBalanceTable($con)
    {
        $tbl = 'cashbook_balances';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('cashbook_balances', function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');

                    $table->decimal('cash_in_hand', 20, 3)->default(0);
                    $table->decimal('cash_in_bank', 20, 3)->default(0);
                    $table->decimal('lock_amount', 20, 3)->default(0);

                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }


    public function createIncomeExpenditureStatementTable($con)
    {
        $tbl = 'income_expenditure_statement';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('income_expenditure_statement', function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->string('month', 2);
                    $table->string('year', 5);

                    $table->float('loan_service_charge');
                    $table->float('bank_interest');
                    $table->float('others_income');
                    $table->float('total_income');


                    $table->float('office_expenditure');
                    $table->float('bank_charge_amount');
                    $table->float('others_expenditure');
                    $table->float('total_expenditure');

                    $table->float('total_profit');


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }

    public function createProfitCalculationTable($con)
    {
        $tbl = 'profit_calculations';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('profit_calculations', function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('incomeexpenditure_id');

                    $table->string('month', 2);
                    $table->string('year', 5);

                    $table->float('cdc_fund');
                    $table->float('cf_fund');
                    $table->float('cluster_fund');
                    $table->float('scg_fund');
                    $table->float('team_leader_fund');
                    $table->float('cdc_office_fund');
                    $table->float('total_profit');
                    $table->tinyInteger('is_profit_disbursed')->default(0);
                    $table->date('profit_disburse_date')->nullable();
                    $table->string('profit_disburse_month', 2)->nullable();
                    $table->string('profit_disburse_year', 5)->nullable();


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }

    public function createProfitCalculationCdcTable($con)
    {
        $tbl = 'profit_calculation_cdcs';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('profit_calculation_cdcs', function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('scg_id');
                    $table->unsignedInteger('incomeexpenditure_id');

                    $table->string('year', 5);

                    $table->unsignedInteger('no_of_member');
                    $table->unsignedInteger('no_of_pg_member');
                    $table->float('scg_closing_balance');
                    $table->float('cdc_closing_balance');
                    $table->float('cdc_total_profit');
                    $table->float('scg_total_profit');


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }

    public function createProfitCalculationScgTable($con)
    {
        $tbl = 'profit_calculation_scgs';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('profit_calculation_scgs', function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('scg_id');
                    $table->unsignedInteger('scmp_id');
                    $table->unsignedInteger('incomeexpenditure_id');

                    $table->string('year', 5);

                    $table->unsignedInteger('poverty_score')->default(0);
                    $table->float('scmp_closing_balance');
                    $table->float('scg_closing_balance');
                    $table->float('scg_total_profit');
                    $table->float('scmp_total_profit');


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }


    public function createBalanceSheetTable($con)
    {
        $tbl = 'balance_sheets';
        if(!Schema::connection( $con )->hasTable($tbl)) {
            Schema::connection($con)
                ->create('balance_sheets', function (Blueprint $table) {
                    $table->increments('id');
                    $table->unsignedInteger('cluster_id');
                    $table->unsignedInteger('cdc_id');
                    $table->unsignedInteger('incomeexpenditure_id');

                    $table->string('month', 2);
                    $table->string('year', 5);

                    $table->float('group_savings');
                    $table->float('cf_fund');
                    $table->float('cdc_fund');
                    $table->float('cluster_fund');
                    $table->float('others_fund');
                    $table->float('passbook_fund');
                    $table->float('total_liability');

                    $table->float('cash_in_hand');
                    $table->float('cash_in_bank');
                    $table->float('loan_outstanding');
                    $table->float('other_invest');
                    $table->float('receivables');
                    $table->float('total_asset');

                    $table->float('profit_disburse_amount')->default(0);
                    $table->date('profit_disburse_date');


                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->timestamps();
                    $table->softDeletes();

                });
        }
    }
}
