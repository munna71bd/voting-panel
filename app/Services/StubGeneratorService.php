<?php

namespace App\Services;

use RuntimeException;

class StubGeneratorService
{
    /**
     * @var string
     */
    protected $source;

    /**
     * @var string
     */
    protected $target;

    /**
     * @param string $source
     * @param string $target
     */
    public function __construct(string $source, string $target)
    {
        $this->source = $source;
        $this->target = $target;
    }

    /**
     * @param array $replacements
     *
     * @return false|int
     * @throws \RuntimeException
     */
    public function generate(array $replacements)
    {
        if (file_exists($this->target)) {
            throw new RuntimeException('Cannot generate file. Target ' . $this->target . ' already exists.');
        }

        $contents = file_get_contents($this->source);

        $path = pathinfo($this->target, PATHINFO_DIRNAME);

        if (! file_exists($path)) {
            mkdir($path, 0776, true);
        }

        return file_put_contents($this->target, strtr($contents, $replacements)) ? 'Generated' : 'Failed';
    }
}
