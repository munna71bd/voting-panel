<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class WithoutBreadCustomPolicy
{
    use HandlesAuthorization;

    public function before(User $user, $ability)
    {
        /** @var \App\Models\User $user */
        if ($user->row_status != 1) {
            return false;
        }

    }

    protected function preCheck($user, $model)
    {
        return true;
    }
}
