<?php

namespace App\Policies;

use App\Models\User;


class UserPolicy extends CustomBasePolicy
{

    public function editUserPermission(User $user, User $model)
    {
        $another = $user->id != $model->id;
        $sameOffice = $this->preCheck($user, $model);
        return $another && $sameOffice && $this->checkPermission($user, $model, 'edit_user_permissions');
    }

    public function changeUserStatus(User $user, User $model)
    {
        $another = $user->id != $model->id;
        $sameOffice = $this->preCheck($user, $model);
        return $another && ($sameOffice && $user->hasPermission('edit_status'));
    }
    public function changeDataAccessArea(User $user, User $model)
    {
        $another = $user->id != $model->id;
        $sameOffice = $this->preCheck($user, $model);
        return $another && ($sameOffice && $user->hasPermission('edit_user_access_area'));
    }
    public function read(User $user, User $model)
    {
        $current = $user->id === $model->id;
        $sameOffice = $this->preCheck($user, $model);
        return $current || ($sameOffice && $this->checkPermission($user, $model, 'read'));
    }

    public function edit(User $user, User $model)
    {
        $current = $user->id === $model->id;
        $sameOffice = $this->preCheck($user, $model);
        return $current || ($sameOffice && $this->checkPermission($user, $model, 'edit'));
    }

    public function editRoles(User $user, User $model)
    {
        $another = $user->id != $model->id;
        $sameOffice = $this->preCheck($user, $model);
        return $another && ($sameOffice && $user->hasPermission('edit_user_role'));
    }

    /**
     * Determine whether the user can restore the attendance shift.
     * @param User $user
     * @param User $model
     * @return mixed
     */
    public function restore(User $user, User $model)
    {
        $another = $user->id != $model->id;
        $sameOffice = $this->preCheck($user, $model);
        return $another && ($sameOffice && $this->checkPermission($user, $model, 'restore'));
    }

    /**
     * Determine whether the user can permanently delete the attendance shift.
     * @param User $user
     * @param User $model
     * @return mixed
     */
    public function forceDelete(User $user, User $model)
    {
        $another = $user->id != $model->id;
        $sameOffice = $this->preCheck($user, $model);
        return $another && ($sameOffice && $this->checkPermission($user, $model, 'force_delete'));
    }

}
