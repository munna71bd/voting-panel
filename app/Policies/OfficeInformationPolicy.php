<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class OfficeInformationPolicy extends CustomBasePolicy
{
    /**
     * @param User $user
     * @param Model $model
     * @return bool
     */
    protected function preCheck(User $user, $model)
    {
        return true;
    }
}
