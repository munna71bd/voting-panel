<?php

namespace App\Policies;

use App\Models\FinancialYear;
use App\Models\User;

class FinancialYearPolicy extends WithoutBreadCustomPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function browse(User $user)
    {
        return true;
    }

    public function read(User $user, FinancialYear $model)
    {
        return true;
    }

    public function edit(User $user, FinancialYear $model)
    {
        return true;
    }

    /**
     * @param User $user
     * @param FinancialYear $model
     * @return mixed
     */
    public function destroy(User $user, FinancialYear $model)
    {
        return true;
    }
}
