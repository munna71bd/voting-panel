<?php

namespace App\Policies;

use App\Facades\Voyager;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Model;

class CustomBasePolicy
{
    use HandlesAuthorization;
    protected static $datatypes = [];

    public function before($user, $ability)
    {
        /** @var \App\Models\User $user */
        if ($user->row_status != 1) {
            return false;
        }

    }


    /**
     * @param User $user
     * @param Model $model
     * @return bool
     */
    protected function preCheck(User $user, $model)
    {
        return true;
    }

    /**
     * Handle all requested permission checks.
     *
     * @param string $name
     * @param array $arguments
     *
     * @return bool
     */
    public function __call($name, $arguments)
    {
        if (count($arguments) < 2) {
            throw new \InvalidArgumentException('not enough arguments');
        }
        /** @var \App\Models\User $user */
        $user = $arguments[0];

        /** @var $model */
        $model = $arguments[1];

        return $this->checkPermission($user, $model, $name);
    }

    /**
     * Check if user has an associated permission.
     * @param \App\Models\User $user
     * @param object $model
     * @param string $action
     * @return bool
     */
    protected function checkPermission(User $user, $model, $action)
    {
        if (!isset(self::$datatypes[get_class($model)])) {
            $dataType = Voyager::model('DataType');
            self::$datatypes[get_class($model)] = $dataType->where('model_name', get_class($model))->first();
        }

        $dataType = self::$datatypes[get_class($model)];

        return $this->preCheck($user, $model) && $user->hasPermission($action . '_' . $dataType->name);
    }

}
