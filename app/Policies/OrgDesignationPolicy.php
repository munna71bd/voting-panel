<?php

namespace App\Policies;

use App\Models\User;
use App\Models\OrgDesignation;

class OrgDesignationPolicy extends CustomBasePolicy
{
    /**
     * Determine whether the user can view any org designations.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        //
    }

    /**
     * Determine whether the user can view the org designation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrgDesignation  $orgDesignation
     * @return mixed
     */
    public function view(User $user, OrgDesignation $orgDesignation)
    {
        //
    }

    /**
     * Determine whether the user can create org designations.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the org designation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrgDesignation  $orgDesignation
     * @return mixed
     */
    public function update(User $user, OrgDesignation $orgDesignation)
    {
        //
    }

    /**
     * Determine whether the user can delete the org designation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrgDesignation  $orgDesignation
     * @return mixed
     */
    public function delete(User $user, OrgDesignation $orgDesignation)
    {
        //
    }

    /**
     * Determine whether the user can restore the org designation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrgDesignation  $orgDesignation
     * @return mixed
     */
    public function restore(User $user, OrgDesignation $orgDesignation)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the org designation.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\OrgDesignation  $orgDesignation
     * @return mixed
     */
    public function forceDelete(User $user, OrgDesignation $orgDesignation)
    {
        //
    }
}
