<?php

namespace App\Policies;

use App\Models\Bank;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class BankPolicy extends WithoutBreadCustomPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function browse(User $user)
    {
        return true;
    }

    public function read(User $user, Bank $model)
    {
        return true;
    }

    public function edit(User $user, Bank $model)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Bank $model
     * @return mixed
     */
    public function restore(User $user, Bank $model)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Bank $model
     * @return mixed
     */
    public function forceDelete(User $user, Bank $model)
    {
        return true;
    }
}
