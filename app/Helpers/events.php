<?php
if (!function_exists('eventSoftbd')) {
    function eventSoftbd($eventClass, $payload)
    {
        if (!empty($eventClass) && is_string($eventClass)) {
            if (class_exists($eventClass)) {
                $eventObject = new $eventClass($payload);
                return event($eventObject);
            }
        }
        return false;
    }
}
