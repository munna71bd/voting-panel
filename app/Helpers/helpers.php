<?php

use App\Models\User;
use Illuminate\Support\Str;

if (!function_exists('setting')) {
    function setting($key, $default = null)
    {
        return App\Facades\Voyager::setting($key, $default);
    }
}

if (!function_exists('menu')) {
    function menu($menuName, $type = null, array $options = [])
    {
        return App\Facades\Voyager::model('Menu')->display($menuName, $type, $options);
    }
}

if (!function_exists('voyager_asset')) {
    function voyager_asset($path, $secure = null)
    {
        return route('voyager.voyager_assets') . '?path=' . urlencode($path);
    }
}

if (!function_exists('get_file_name')) {
    function get_file_name($name)
    {
        preg_match('/(_)([0-9])+$/', $name, $matches);
        if (count($matches) == 3) {
            return Illuminate\Support\Str::replaceLast($matches[0], '', $name) . '_' . (intval($matches[2]) + 1);
        } else {
            return $name . '_1';
        }
    }
}

if (!function_exists('getUserDataAccessAreasList')) {
    function getUserDataAccessAreasList(string $for = User::ACCESS_AREA_VILLAGE)
    {
        return User::getFilteredDataAccessAreas($for);
    }
}

if (!function_exists('bn2en')) {
    function bn2en($number)
    {
        if (app()->getLocale() == 'bn') {
            return $number;
        }
        $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
        $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
        return str_replace($bn, $en, $number);
    }
}

if (!function_exists('en2bn')) {
    function en2bn($number)
    {
        if (app()->getLocale() == 'en') {
            return $number;
        }
        $bn = array("১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯", "০");
        $en = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
        return str_replace($en, $bn, $number);
    }
}

if (!function_exists('localizedMenu')) {
    function localizedMenu($menuName, $type = null, array $options = [])
    {
        $localizedMenu = App\Facades\Voyager::model('Menu')->display($menuName, $type, $options);
        return $localizedMenu;
    }
}

if (!function_exists('isCommonLocalKey')) {
    function isCommonLocalKey($key = null)
    {
        return !is_null($key) && in_array(Str::snake($key), config('voyager.common_local_keys'), true);
    }
}

if (!function_exists('randomPassword')) {
    function randomPassword($length, $onlyDigit = false)
    {
        $alphabet = $onlyDigit ? '1234567890' : 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }
}

if (!function_exists('getGender')) {
    function getGender($id = null)
    {
        return is_null($id) ? ''
            : (isset(config('microcredit.gender')[$id])
                ? config('microcredit.gender')[$id] : '');
    }
}

if (!function_exists('getMonthName')) {
    function getMonthName($id = null)
    {
        $month = ["", "January", "February", "March", "April", "May", "Jun", "July", "Agust", "September", "October", "November", "December"];
        return is_null($id) ? ''
            : $month[$id];
    }
}

if (!function_exists('getMonthNameBn')) {
    function getMonthNameBn($id = null)
    {
        $month = ["", "জানুয়ারী", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টোবর", "নভেম্বর", "ডিসেম্বর"];
        return is_null($id) ? ''
            : $month[$id];
    }
}


if (!function_exists('enDate2bnDate')) {
    function enDate2bnDate($date)
    {
        if (empty($date)) {
            return '';
        }

        $months = [
            '01' => 'জানুয়ারী',
            '02' => 'ফেব্রুয়ারী',
            '03' => 'মার্চ',
            '04' => 'এপ্রিল',
            '05' => 'মে',
            '06' => 'জুন',
            '07' => 'জুলাই',
            '08' => 'আগস্ট',
            '09' => 'সেপ্টেম্বর',
            '10' => 'অক্টোবর',
            '11' => 'নভেম্বর',
            '12' => 'ডিসেম্বর',
        ];
        $months_en = [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];
        $dateArr = explode('-', $date);
        $m = $months[$dateArr[1]];
        if (app()->getLocale() == 'en') {
            $m = $months_en[$dateArr[1]];
        }

        $banglaDate = en2bn($dateArr[2]) . ' ' . $m . ', ' . en2bn($dateArr[0]);
        return $banglaDate;
    }
}

if (!function_exists('enMonth2bnMonth')) {
    function enMonth2bnMonth($month)
    {
        $months = [
            '01' => 'জানুয়ারী',
            '02' => 'ফেব্রুয়ারী',
            '03' => 'মার্চ',
            '04' => 'এপ্রিল',
            '05' => 'মে',
            '06' => 'জুন',
            '07' => 'জুলাই',
            '08' => 'আগস্ট',
            '09' => 'সেপ্টেম্বর',
            '10' => 'অক্টোবর',
            '11' => 'নভেম্বর',
            '12' => 'ডিসেম্বর',
        ];
        $months_en = [
            '01' => 'January',
            '02' => 'February',
            '03' => 'March',
            '04' => 'April',
            '05' => 'May',
            '06' => 'Jun',
            '07' => 'July',
            '08' => 'August',
            '09' => 'September',
            '10' => 'October',
            '11' => 'November',
            '12' => 'December',
        ];

        if (isset($months[$month])) {
            if (app()->getLocale() == 'en') {
                return $months_en[$month];
            }

            return $months[$month];
        }

        return "";
    }

    if (!function_exists('getMaserDbName')) {
        function getMaserDbName()
        {
            $db = config('database.mysql_connections');
            if (isset($db) && isset($db['mysql_master']) && $db['mysql_master']['database']) {
                $db = $db['mysql_master']['database'];
            } else {
                $db = 'undp_microcredit_master';
            }
            return $db;
        }
    }

    if (!function_exists('pushNotification')) {
        function pushNotification($noti)
        {
            $api_url = "https://fcm.googleapis.com/fcm/send";
            $authKey = "key=AAAAQeZURvQ:APA91bFxNT55_pXcS6wDwCinKVzQjno1rZQC2MN5xdj7LpYoeLzeHE5aVVU4c60yE-noSky6yuYNAx9V-vDOwHfYBXTlsfCQB-rOqZAQuOTWB5RskYKuv6zq4XXvzemk-1QCr04un1nl";


            $users = json_decode($noti->users);
            $single_device = [];

            if (count($users) > 0) {

                foreach ($users as $u) {

                    $device_ids = \Module\Notification\Models\UserDevice::where('user_id', $u)->get();

                    foreach ($device_ids as $dvc) {


                        array_push($single_device, $dvc->device_id);
                    }
                }


                $data = [
                    "registration_ids" => $single_device,
                    "mutable_content" => true,
                    "content_available" => true,
                    "priority" => "high",
                    "notification" => [
                        "title" => $noti->title,
                        "body" => $noti->description,
                    ],
                    "data" => [
                        "channelKey" => "BCPS",
                        "title" => $noti->title,
                        "body" => $noti->description,
                        "notificationLayout" => "Inbox",
                        "showWhen" => true,
                        "autoCancel" => false,
                        "privacy" => "Public",
                        "images" => json_decode($noti->images),
                        "videos" => json_decode($noti->videos),
                        "files" => json_decode($noti->files),
                        'sending_time' => date('Y-m-d h:i:s a'),
                    ]
                ];


                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $api_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $authKey));


                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);

                curl_close($ch);

                $msg = $server_output;
                $check_msg = json_decode($msg);
                if ($check_msg) {
                    if (isset($check_msg->message_id) || isset($check_msg->multicast_id)) {

                        $noti->sent_date = \Illuminate\Support\Carbon::now();
                        $noti->is_sent = 1;
                        $noti->save();
                    }

                }


            } else {

                $to = "/topics/all";

                if ($noti->grpup) {

                    $to = "/topics/" . strtolower($noti->group->title);
                }

                $data = [
                    "to" => $to,
                    "mutable_content" => true,
                    "content_available" => true,
                    "priority" => "high",
                    "notification" => [
                        "title" => $noti->title,
                        "body" => $noti->description,
                    ],
                    "data" => [
                        "channelKey" => "BCPS",
                        "title" => $noti->title,
                        "body" => $noti->description,
                        "notificationLayout" => "Inbox",
                        "showWhen" => true,
                        "autoCancel" => false,
                        "privacy" => "Public",
                        "images" => json_decode($noti->images),
                        "videos" => json_decode($noti->videos),
                        "files" => json_decode($noti->files),
                        'sending_time' => date('Y-m-d h:i:s a'),
                    ]
                ];


                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $api_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: ' . $authKey));


                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);

                curl_close($ch);

                $msg = $server_output;
                $check_msg = json_decode($msg);
                if ($check_msg) {
                    if (isset($check_msg->message_id) || isset($check_msg->multicast_id)) {

                        $noti->sent_date = \Illuminate\Support\Carbon::now();
                        $noti->is_sent = 1;
                        $noti->save();
                    }

                }
                return $msg;
            }
        }
    }

    if (!function_exists('generateOtp')) {
        function generateOtp($digits)
        {
            return rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        }
    }


    if (!function_exists('sendOtp')) {
        function sendOtp($msg, $mobile)
        {

            $sendSms = new \App\Services\Sms\SmsSenderService();
            $res = $sendSms->sendSms($msg, $mobile);

            if (isset($res['result']) && $res['result'][0] == 'S') {
                return true;
            }
            return false;
        }

    }


    if (!function_exists('sendEmail')) {
        function sendEmail($msg, $address)
        {

            $details = [
                'email' => $address,
                'name' => "",
                'contact' => "",
                'body' => $msg,
                'subject' => 'OTP Code'
            ];
            $result = Mail::to($address)->send(new \App\Mail\ContactMailer($details));
        }

    }

}
