<?php

namespace App\Http\Controllers;

use App\Facades\Voyager;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\Models\LocUnion;
use App\Models\User;
use App\Voyager\Actions\AbstractAction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class LocUnionController extends VoyagerBaseController
{
    private function index111(Request $request)
    {
        /** GET THE SLUG, ex. 'posts', 'pages', etc. */
        $slug = $this->getSlug($request);

        /** GET THE DataType based on the slug */
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        /** Check permission */
        $this->authorize('browse', app($dataType->model_name));

        $usesSoftDeletes = true;
        $isServerSide = false;
        $showSoftDeleted = false;

        $view = "voyager::".$slug.".datatable.browse";

        return Voyager::view($view, compact(
            'dataType',
            'isServerSide',
            'usesSoftDeletes',
            'showSoftDeleted'
        ));
    }

    private function getListDataServerSide11(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        try {
            $dataType = Voyager::model('DataType')->where('slug', '=', 'loc-unions')->first();
            /** @var Builder $locUnions */
            $locUnions = LocUnion::select([
                'loc_unions.id as id',
                'loc_divisions.title as division_title',
                'loc_districts.title as district_title',
                'loc_upazilas.title as upazila_title',
                'loc_unions.title_en',
                'loc_unions.title',
                'loc_unions.bbs_code',
                'loc_unions.row_status',
                'loc_unions.created_at',
                'loc_unions.updated_at'
            ]);

            $locUnions->leftJoin('loc_divisions', 'loc_unions.loc_division_id', '=', 'loc_divisions.id');
            $locUnions->leftJoin('loc_districts', 'loc_unions.loc_district_id', '=', 'loc_districts.id');
            $locUnions->leftJoin('loc_upazilas', 'loc_unions.loc_upazila_id', '=', 'loc_upazilas.id');

            $locUnions = LocUnion::staticAcl($locUnions, 'loc_unions.');

            return DataTables::eloquent($locUnions)
                ->editColumn('row_status', function (LocUnion $union) {
                    return $union->row_status ? __('Active') : __('Inactive');
                })
                ->addColumn('action', function (LocUnion $union) use ($user, $dataType) {
                    $str = "";
                    foreach (Voyager::actions() as $action) {
                        if (!method_exists($action, 'massAction')) {
                            /** @var AbstractAction $action */
                            $action = new $action($dataType, $union);
                            if ($action->shouldActionDisplayOnDataType()) {
                                $policies = $action->getPolicy();
                                if (is_array($policies)) {
                                    $show = false;
                                    foreach ($policies as $policy) {
                                        if ($user->can($policy, $union)) {
                                            $show = true;
                                        }
                                    }
                                    if ($show) {
                                        $str .= '<a href="' . $action->getRoute($dataType->name) . '"
                                        title="' . $action->getTitle() . '"' . str_replace('pull-right', 'union-action-button', $action->convertAttributesToHtml()) . '>' .
                                            '<i class="' . $action->getIcon() . '"></i> <span class="hidden-xs hidden-sm">
                                        ' . $action->getTitle() . '</span></a>';
                                    }
                                } else {
                                    if ($user->can($policies, $union)) {
                                        $str .= '<a href="' . $action->getRoute($dataType->name) . '"
                                        title="' . $action->getTitle() . '"' . str_replace('pull-right', 'union-action-button', $action->convertAttributesToHtml()) . '>' .
                                            '<i class="' . $action->getIcon() . '"></i> <span class="hidden-xs hidden-sm">
                                        ' . $action->getTitle() . '</span></a>';
                                    }
                                }
                            }
                        }
                    }

                    return $str;
                })
                ->rawColumns(['action'])
                ->toJson();

        } catch (\Exception $ex) {
            return response()->json([], 404);
        }
    }
}
