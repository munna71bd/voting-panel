<?php

namespace App\Http\Controllers\Voyager;

use App\Facades\Voyager;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\Rule;

class VoyagerAuthController extends Controller
{
    protected $cusUsername = 'username';
    protected $loginFieldName = 'username';

    use AuthenticatesUsers;

    public function login()
    {
        if ($this->guard()->user()) {
            return redirect()->route('voyager.dashboard');
        }

        return Voyager::view('voyager::login');
    }

    public function username()
    {
        return $this->cusUsername;
    }

    protected function checkUsernameType(Request $request)
    {
        $usernameValue = $request->get($this->loginFieldName, 'username');
        if (preg_match('/^(\+88)?01[1-9][0-9]{8}$/', $usernameValue, $output_array)) {
            $this->cusUsername = 'cell_phone';
        }
    }

    /**
     * Validate the user login request.
     *
     * @param \Illuminate\Http\Request $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $usernameMinLen = (int)setting('admin.username_min_len');
        $passwordMinLen = (int)setting('admin.password_min_len');

        if ($this->username() === 'cell_phone') {
            $rules = [
                $this->loginFieldName => 'required|string|regex:/^(\+88)?01[1-9][0-9]{8}$/',
                'password' => [
                    'required',
                    'string',
                    'min:' . $passwordMinLen,
                    function ($attribute, $value, $fail) {
                        $isValid = true;
                        if (!preg_match('/[\d]/', $value, $output_array)) {
                            $isValid = false;
                        } else if (!preg_match('/[a-z]/', $value, $output_array)) {
                            $isValid = false;
                        } else if (!preg_match('/[A-Z]/', $value, $output_array)) {
                            $isValid = false;
                        }
                        if (!$isValid) {
                            $fail(__("generic.invalid_credentails"));
                        }
                    }
                ],
            ];
        } else if ($this->username() === 'username') {
            $rules = [
                $this->loginFieldName => [
                    'required',
                    'string',
                    'min:' . $usernameMinLen
                ],
                'password' => [
                    'required',
                    'string',
                    'min:' . $passwordMinLen,
                    function ($attribute, $value, $fail) {
                        $isValid = true;
                        if (!preg_match('/[\d]/', $value, $output_array)) {
                            $isValid = false;
                        } else if (!preg_match('/[a-z]/', $value, $output_array)) {
                            $isValid = false;
                        } else if (!preg_match('/[A-Z]/', $value, $output_array)) {
                            $isValid = false;
                        }
                        if (!$isValid) {
                            $fail(__("generic.invalid_credentails"));
                        }
                    }
                ],
            ];
        } else {
            $rules = [
                $this->loginFieldName => 'required|string|email',
                'password' => [
                    'required',
                    'string',
                    'min:' . $passwordMinLen,
                    function ($attribute, $value, $fail) {
                        $isValid = true;
                        if (!preg_match('/[\d]/', $value, $output_array)) {
                            $isValid = false;
                        } else if (!preg_match('/[a-z]/', $value, $output_array)) {
                            $isValid = false;
                        } else if (!preg_match('/[A-Z]/', $value, $output_array)) {
                            $isValid = false;
                        }
                        if (!$isValid) {
                            $fail(__("generic.invalid_credentails"));
                        }
                    }
                ],
            ];
        }

        $data = $request->only([$this->loginFieldName, 'password']);
        $customAttributes = [];
        $fields = [$this->loginFieldName, 'password'];
        foreach ($fields as $key) {
            $customAttributes[$key] = __('bread.users.' . $key);
        }
        $messages = [
            'required' => __('generic.invalid_credentails'),
            'min' => __('generic.invalid_credentails'),
        ];

        \Illuminate\Support\Facades\Validator::make($data, $rules, $messages, $customAttributes)->validate();
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return [
            $this->username() => $request->get($this->loginFieldName),
            "password" => $request->get('password')
        ];
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\Response|void
     * @throws \Illuminate\Validation\ValidationException
     */
    public function postLogin(Request $request)
    {
        $this->checkUsernameType($request);
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $credentials = $this->credentials($request);

        if ($this->guard()->attempt($credentials, $request->has('remember'))) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    /*
     * Preempts $redirectTo member variable (from RedirectsUsers trait)
     */
    public function redirectTo()
    {
        return config('voyager.user.redirect', route('voyager.dashboard'));
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return app('VoyagerAuth');
    }

    protected function authenticated(Request $request, $user)
    {
    }
}
