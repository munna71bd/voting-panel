<?php

namespace App\Http\Controllers\Voyager;

use App\Facades\Voyager;
use App\Http\Middleware\Locale;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Constraint;
use Intervention\Image\Facades\Image;
use Module\Bank\Models\BankAccount;
use Module\Cdc\Models\Cdc;
use Module\Cluster\Models\Notification;
use Module\Scg\Models\Scg;

class VoyagerController extends Controller
{

    public function index()
    {

        return Voyager::view('voyager::index');
    }

    public function logout()
    {
        app('VoyagerAuth')->logout();

        return redirect()->route('voyager.login');
    }

    public function upload(Request $request)
    {
        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug . '/' . date('F') . date('Y') . '/';

        $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension());
        $filename_counter = 1;

        /** Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc... */
        while (Storage::disk(config('voyager.storage.disk'))->exists($path . $filename . '.' . $file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.' . $file->getClientOriginalExtension()) . (string)($filename_counter++);
        }

        $fullPath = $path . $filename . '.' . $file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            /** move uploaded file from temp to uploads directory */
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string)$image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        /** echo out script that TinyMCE can handle and update the image in the editor */
        return "<script> parent.helpers.setImageValue('" . Voyager::image($fullFilename) . "'); </script>";
    }

    public function assets(Request $request)
    {
        $path = Str::start(str_replace(['../', './'], '', urldecode($request->path)), '/');

        $path = base_path('public/backend_resources' . $path);

        if (File::exists($path)) {
            $mime = '';
            if (Str::endsWith($path, '.js')) {
                $mime = 'text/javascript';
            } elseif (Str::endsWith($path, '.css')) {
                $mime = 'text/css';
            } else {
                $mime = File::mimeType($path);
            }
            $response = response(File::get($path), 200, ['Content-Type' => $mime]);
            $response->setSharedMaxAge(31536000);
            $response->setMaxAge(31536000);
            $response->setExpires(new \DateTime('+1 year'));

            return $response;
        }

        return response('', 404);
    }

    public function getDashboardData()
    {
        $data = [];

        $con = 'mysql_cluster_';
        $cluster_id = 0;
        $cdc_id = 0;
        $scg_id = 0;

        $cdcids = [];
        $scgids = [];

        if (auth()->user()->cluster_id > 0) {
            $cluster_id = auth()->user()->cluster_id;
        }


        $cdcids = json_decode(auth()->user()->cdc_id);

        if (auth()->user()->cdc_id > 0) {
            $cdcids = [$cdcids];
        }

        $scgids = json_decode(auth()->user()->scg_id);

        if (auth()->user()->scg_id > 0) {
            $scgids = [$scgids];
        }


        $clusters = Notification::pluck('id');
        $cdcs = Cdc::pluck('id');
        $scgs = Scg::pluck('id');


        if ($cluster_id > 0) {
            $clusters = Notification::where('id', $cluster_id)->pluck('id');
            $cdcs = Cdc::where('cluster_id', $cluster_id)->pluck('id');
            $scgs = Scg::where('cluster_id', $cluster_id)->pluck('id');

        }

        if ($cdcids) {
            $cdcs = Cdc::where('cluster_id', $cluster_id)->whereIn('id', $cdcids)->pluck('id');
            $scgs = Scg::where('cluster_id', $cluster_id)->whereIn('cdc_id', $cdcids)->pluck('id');
        }


        if ($scgids) {
            $scgs = Scg::whereIn('id', $scgids)->pluck('id');
        }


        $total_member_male = 0;
        $total_member_female = 0;
        $total_member_pwd = 0;
        $total_saving_withdrawn = 0;
        $total_saving_amount = 0;

        $total_cash_in_hand = 0;
        $total_cash_in_bank = 0;
        $total_balance_sheet = [];
        $total_balance_sheet['group_saving'] = 0;
        $total_balance_sheet['cf_fund'] = 0;
        $total_balance_sheet['cdc_fund'] = 0;
        $total_balance_sheet['cluster_fund'] = 0;
        $total_balance_sheet['others_fund'] = 0;
        $total_balance_sheet['cash_in_hand'] = 0;
        $total_balance_sheet['cash_in_bank'] = 0;
        $total_balance_sheet['loan_outstanding'] = 0;
        $total_balance_sheet['other_invest'] = 0;
        $total_balance_sheet['receivables'] = 0;
        $total_balance_sheet['total'] = 0;

        $total_urgent_loan = 0;
        $total_social_loan = 0;
        $total_iga_loan = 0;
        $total_house_loan = 0;
        $total_disbursed_loan = 0;
        $total_loan_member = 0;
        $total_current_loan = 0;
        $total_collected_loan = 0;
        $total_loan_outstanding = 0;

        $last_reporting_month = date('m') - 1;
        $last_reporting_year = date('Y');
        $total_saving_balance = 0;
        $f = 0;
        $total_scg_member_transgender = 0;

        $total_approved_scg = 0;
        $approved_scg_list = [];
        $approved_scg_list_not = [];

        $cdc_updated = 0;
        $cdc_updated_not = 0;

        $current_date = date("Y-m-t", strtotime("- 1 month"));
        $current_month = date("m", strtotime("- 1 month"));
        $current_year = date("Y", strtotime("- 1 month"));


        foreach ($clusters as $c) {

            $total_member_male += DB::connection($con . $c)
                ->table('scmps')
                ->where('gender', 'Male')
                ->whereIn('scg_id', $scgs)
                ->count();
            $total_member_female += DB::connection($con . $c)
                ->table('scmps')
                ->where('gender', 'Female')
                ->whereIn('scg_id', $scgs)
                ->count();
            $total_member_pwd += DB::connection($con . $c)
                ->table('scmps')
                ->whereNotIn('gender', ['Male', 'Female'])
                ->whereIn('scg_id', $scgs)
                ->count();


            foreach ($scgs as $cdc) {
                /*
                $tbl = 'team_' . $cdc . '_balances_';

                if (Schema::connection($con . $c)->hasTable($tbl)) {
                    $last_reporting_d = DB::connection($con . $c)
                        ->table($tbl)
                        ->orderBy('id','DESC')
                        ->first();
                    if($last_reporting_d && $f==0)
                    {
                        $last_reporting_month = $last_reporting_d->month;
                        $last_reporting_year = (int)$last_reporting_d->year;
                        $f = 1;
                    }
                    else if($last_reporting_d && $f==1)
                    {
                        if($last_reporting_d->month>$last_reporting_month && (int)$last_reporting_d->year>= $last_reporting_year )
                        {
                            $last_reporting_month = $last_reporting_d->month;
                            $last_reporting_year = (int)$last_reporting_d->year;
                        }

                    }

                    $total_saving_amount += DB::connection($con . $c)
                        ->table($tbl)
                        ->where('month',  $last_reporting_month)
                        ->where('year',$last_reporting_year)
                        ->sum('initial_balance');

                    $total_saving_amount += DB::connection($con . $c)
                        ->table($tbl)
                        ->where('month',  $last_reporting_month)
                        ->where('year',$last_reporting_year)
                        ->sum('total_collected_amount');



                    $total_saving_withdrawn += DB::connection($con . $c)
                        ->table($tbl)
                        ->where('month', $last_reporting_month)
                        ->where('year',$last_reporting_year)
                        ->sum('withdrawal_amount');

                    $total_saving_balance += DB::connection($con . $c)
                        ->table($tbl)
                        ->where('month',  $last_reporting_month)
                        ->where('year',$last_reporting_year)
                        ->sum('closing_balance');

                }
                */
                $tbl = 'team_' . $cdc . '_transactions_';

                if (Schema::connection($con . $c)->hasTable($tbl)) {
                    $last_reporting_d = DB::connection($con . $c)
                        ->table($tbl)
                        ->orderBy('id', 'DESC')
                        ->first();
                    if ($last_reporting_d && $f == 0) {
                        $last_reporting_month = $last_reporting_d->tran_month;
                        $last_reporting_year = (int)$last_reporting_d->tran_year;
                        $f = 1;
                    } else if ($last_reporting_d && $f == 1) {
                        if ($last_reporting_d->tran_month > $last_reporting_month && (int)$last_reporting_d->tran_year >= $last_reporting_year) {
                            $last_reporting_month = $last_reporting_d->tran_month;
                            $last_reporting_year = (int)$last_reporting_d->tran_year;
                        }

                    }


                    $s = DB::connection($con . $c)
                        ->table($tbl)
                        ->where('tran_type', 'Saving')
                        ->sum('amount');
                    $total_saving_amount += $s;


                    $w = DB::connection($con . $c)
                        ->table($tbl)
                        ->where('tran_type', 'Withdraw')
                        ->sum('amount');
                    $total_saving_withdrawn += $w;

                    $total_saving_balance += ($s - $w);

                }


                $tbl = 'team_' . $cdc . '_loan_balances_';

                if (Schema::connection($con . $c)->hasTable($tbl)) {
                    $last_reporting_date = $last_reporting_year . "-" . $last_reporting_month . "-01";
                    $last_reporting_date_next_month = date("Y-m-t", strtotime($last_reporting_date));

                    $total_urgent_loan += DB::connection($con . $c)
                        ->table('team_' . $cdc . '_loan_applications')
                        ->whereIn('scg_id', $scgs)
                        //->where('application_date', '<=',$last_reporting_date_next_month)
                        ->where('loan_type', 'Urgent Loan')
                        ->whereIn('status', ['completed', 'collected'])
                        ->sum('approved_amount');

                    $total_social_loan += DB::connection($con . $c)
                        ->table('team_' . $cdc . '_loan_applications')
                        ->whereIn('scg_id', $scgs)
                        // ->where('application_date', '<=',$last_reporting_date_next_month)
                        ->where('loan_type', 'Social Loan')
                        ->whereIn('status', ['completed', 'collected'])
                        ->sum('approved_amount');

                    $total_iga_loan += DB::connection($con . $c)
                        ->table('team_' . $cdc . '_loan_applications')
                        ->whereIn('scg_id', $scgs)
                        //->where('application_date', '<=',$last_reporting_date_next_month)
                        ->where('loan_type', 'IGA Loan')
                        ->whereIn('status', ['completed', 'collected'])
                        ->sum('approved_amount');

                    $total_house_loan += DB::connection($con . $c)
                        ->table('team_' . $cdc . '_loan_applications')
                        ->whereIn('scg_id', $scgs)
                        //->where('application_date', '<=',$last_reporting_date_next_month)
                        ->where('loan_type', 'House Loan')
                        ->whereIn('status', ['completed', 'collected'])
                        ->sum('approved_amount');

                    $total_disbursed_loan += DB::connection($con . $c)
                        ->table('team_' . $cdc . '_loan_applications')
                        ->whereIn('scg_id', $scgs)
                        //->where('application_date', '<=',$last_reporting_date_next_month)
                        ->whereIn('status', ['completed', 'collected'])
                        ->sum('approved_amount');

                    $total_loan_member += DB::connection($con . $c)
                        ->table('team_' . $cdc . '_loan_applications')
                        ->whereIn('scg_id', $scgs)
                        //->where('application_date', '<=',$last_reporting_date_next_month)
                        ->whereIn('status', ['completed', 'collected'])
                        ->groupBy('scmp_id')
                        ->count();

                    $total_current_loan += DB::connection($con . $c)
                        ->table('team_' . $cdc . '_loan_applications')
                        ->whereIn('scg_id', $scgs)
                        //->where('application_date', '<=',$last_reporting_date_next_month)
                        ->where('status', 'completed')
                        ->sum('approved_amount');
                    $total_current_loan += DB::connection($con . $c)
                        ->table('team_' . $cdc . '_loan_applications')
                        ->whereIn('scg_id', $scgs)
                        //->where('application_date', '<=',$last_reporting_date_next_month)
                        ->where('status', 'completed')
                        ->sum('service_charge');

                    $total_collected_loan += DB::connection($con . $c)
                        ->table($tbl)
                        ->whereIn('scg_id', $scgs)
                        //->where('month', $last_reporting_month)
                        //->where('year', $last_reporting_year)
                        ->sum('total_collected_amount');


                }
                $s = Scg::find($cdc);
                if ($s && $s->isCdcApproved($last_reporting_month, $last_reporting_year, 'saving')) {
                    $total_approved_scg++;
                    $approved_scg_list[] = $cdc;
                }
                if ($s && !$s->isCdcApproved($last_reporting_month, $last_reporting_year, 'saving')) {
                    $approved_scg_list_not[] = $cdc;
                }
            }

            $tbl = 'cashbook_balances';

            if (Schema::connection($con . $c)->hasTable($tbl)) {

                $total_cash_in_hand += DB::connection($con . $c)
                    ->table($tbl)
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('cash_in_hand');


            }

            if (Schema::connection($con . $c)->hasTable($tbl)) {

                $total_cash_in_bank += DB::connection($con . $c)
                    ->table($tbl)
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('cash_in_bank');


            }

            $tbl = 'balance_sheets';
            if (Schema::connection($con . $c)->hasTable($tbl)) {

                $total_balance_sheet['group_saving'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('group_savings');

                $total_balance_sheet['cf_fund'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('cf_fund');

                $total_balance_sheet['cdc_fund'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('cdc_fund');

                $total_balance_sheet['cluster_fund'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('cluster_fund');

                $total_balance_sheet['others_fund'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('others_fund');

                $total_balance_sheet['cash_in_hand'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('cash_in_hand');

                $total_balance_sheet['cash_in_bank'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('cash_in_bank');

                $total_balance_sheet['loan_outstanding'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('loan_outstanding');

                $total_balance_sheet['other_invest'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('other_invest');

                $total_balance_sheet['receivables'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('receivables');

                $total_balance_sheet['total'] += DB::connection($con . $c)
                    ->table($tbl)
                    ->where('year', date('Y'))
                    ->whereIn('cdc_id', $cdcs)
                    ->sum('total_asset');


            }


        }


        $approved_scg_list = Scg::whereIn('id', $approved_scg_list_not)->get();

        foreach ($approved_scg_list as $s) {
            $s->cdc_name = $s->cdc->name;
        }


        $cdcm = Cdc::whereIn('id', $cdcs)->get();

        foreach ($cdcm as $c) {
            $cdc_updated += $c->isUpdated($current_date);
            $cdc_updated_not += $c->isNotUpdated($current_date);
        }


        $data['total_cluster'] = Notification::WhereIn('id', $clusters)->count();
        $data['total_cdc'] = Cdc::whereIn('id', $cdcs)->count();
        $data['total_cdc_old'] = Scg::WhereIn('cluster_id', $clusters)->withTrashed()->count();
        $data['total_scg'] = Scg::WhereIn('id', $scgs)->count();

        $data['total_pg'] = 1;
        $data['total_pg_member'] = 0;

        $data['total_scg_member'] = $total_member_male + $total_member_female + $total_member_pwd;
        $data['total_scg_member_male'] = $total_member_male;
        $data['total_scg_member_female'] = $total_member_female;
        $data['total_scg_member_pwd'] = $total_member_pwd;
        $data['total_saving_amount'] = $total_saving_amount;
        $data['total_saving_withdrawn'] = $total_saving_withdrawn;
        $data['total_saving_balance'] = $total_saving_balance;
        $data['total_saving_outstanding'] = 0;

        $data['total_urgent_loan'] = $total_urgent_loan;
        $data['total_iga_loan'] = $total_iga_loan;
        $data['total_social_loan'] = $total_social_loan;
        $data['total_house_loan'] = $total_house_loan;
        $data['total_disbursed_loan'] = $total_disbursed_loan;
        $data['total_loan_member'] = $total_loan_member;
        $data['total_current_loan'] = $total_current_loan;
        $data['total_collected_loan'] = $total_collected_loan;
        $data['total_loan_outstanding'] = $total_loan_outstanding;

        $data['total_cash_in_hand'] = $total_cash_in_hand;
        $data['total_cash_in_bank'] = $total_cash_in_bank;
        $data['total_balance_sheet'] = $total_balance_sheet;
        $data['last_reporting_month'] = enMonth2bnMonth($last_reporting_month);
        $data['last_reporting_year'] = en2bn($last_reporting_year);
        $data['total_scg_member_transgender'] = $total_scg_member_transgender;
        $data['total_approved_scg'] = $total_approved_scg;
        $data['approved_scg_list'] = $approved_scg_list;
        $data['cdc_updated'] = $cdc_updated;
        $data['cdc_updated_not'] = $cdc_updated_not;

        return $data;
    }

    public function launch(Request $request)
    {

        return Voyager::view('voyager::launch');
    }

    public function getDashboardCdclist(Request $request)
    {
        $data = [];
        $cdcids = [];
        $cluster_id = 0;
        $clusters = [];
        $cdcs = [];
        $current_date = date("Y-m-t", strtotime("- 1 month"));


        if (auth()->user()->cluster_id > 0) {
            $cluster_id = auth()->user()->cluster_id;
        }


        $cdcids = json_decode(auth()->user()->cdc_id);

        if (auth()->user()->cdc_id > 0) {
            $cdcids = [$cdcids];
        }

        if ($cluster_id > 0) {
            $clusters = Notification::where('id', $cluster_id)->pluck('id');
            $cdcs = Cdc::where('cluster_id', $cluster_id)->pluck('id');

        }

        if ($cdcids) {
            $cdcs = Cdc::where('cluster_id', $cluster_id)->whereIn('id', $cdcids)->pluck('id');
        }

        $cdcm = Cdc::whereIn('id', $cdcs)->get();
        $cdc_list = [];

        foreach ($cdcm as $c) {
            if ($request->key == 'updated' && $c->isUpdated($current_date)) {
                $c->lats_updated_month_year = $c->lastUpdatedMonthYear();
                $cdc_list [] = $c;
            } else if ($request->key == 'updated_not' && $c->isNotUpdated($current_date)) {
                $c->lats_updated_month_year = $c->lastUpdatedMonthYear();
                $cdc_list [] = $c;
            }
        }
        $data['cdc_list'] = $cdc_list;
        return $data;
    }
}
