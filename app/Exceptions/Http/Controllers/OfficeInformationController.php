<?php

namespace App\Http\Controllers;

use App\Facades\Voyager;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\Models\OfficeInformation;
use App\Models\User;
use App\Voyager\Actions\AbstractAction;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Yajra\DataTables\Facades\DataTables;

class OfficeInformationController extends VoyagerBaseController
{
    private function index11(Request $request)
    {

        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        $this->authorize('browse', app($dataType->model_name));

        $usesSoftDeletes = true;
        $isServerSide = false;
        $showSoftDeleted = false;

        $view = "voyager::" . $slug . ".datatable.browse";

        return Voyager::view($view, compact(
            'dataType',
            'isServerSide',
            'usesSoftDeletes',
            'showSoftDeleted'
        ));
    }

    private function getListDataServerSide11(Request $request)
    {
        /** @var User $user */
        $user = Auth::user();
        try {
            $dataType = Voyager::model('DataType')->where('slug', '=', 'office-informations')->first();
            /** @var Builder $offices */
            $offices = OfficeInformation::select([
                'office_informations.id as id',
                'loc_divisions.title as division_title',
                'loc_districts.title as district_title',
                'loc_upazilas.title as upazila_title',
                'loc_unions.title as union_title',
                'parent_office.title_bn as parent_office_title',
                'office_informations.title_en',
                'office_informations.title_bn',
                'office_informations.office_type',
                'office_informations.contact_mobile',
                'office_informations.row_status',
                'office_informations.created_at',
                'office_informations.updated_at'
            ]);

            $offices->leftJoin('loc_divisions', 'loc_divisions.id', '=', 'office_informations.loc_division_id');
            $offices->leftJoin('loc_districts', 'loc_districts.id', '=', 'office_informations.loc_district_id');
            $offices->leftJoin('loc_upazilas', 'loc_upazilas.id', '=', 'office_informations.loc_upazila_id');
            $offices->leftJoin('loc_unions', 'loc_unions.id', '=', 'office_informations.loc_union_id');
            $offices->leftJoin('office_informations as parent_office', 'parent_office.id', '=', 'office_informations.parent_office_id');

            $offices = OfficeInformation::staticAcl($offices, 'office_informations.');

            return DataTables::eloquent($offices)
                ->editColumn('row_status', function (OfficeInformation $office) {
                    $rowStatuses = OfficeInformation::getRowStatuses();
                    return !empty($rowStatuses[$office->row_status]) ? $rowStatuses[$office->row_status] : "";
                })
                ->editColumn('office_type', function (OfficeInformation $office) {
                    $arr = OfficeInformation::getOfficeTypes();
                    return !empty($arr[$office->office_type]) ? $arr[$office->office_type] : "";
                })
                ->addColumn('action', function (OfficeInformation $office) use ($user, $dataType) {
                    $str = "";
                    foreach (Voyager::actions() as $action) {
                        if (!method_exists($action, 'massAction')) {
                            /** @var AbstractAction $action */
                            $action = new $action($dataType, $office);
                            if ($action->shouldActionDisplayOnDataType()) {
                                $policies = $action->getPolicy();
                                if (is_array($policies)) {
                                    $show = false;
                                    foreach ($policies as $policy) {
                                        if ($user->can($policy, $office)) {
                                            $show = true;
                                        }
                                    }
                                    if ($show) {
                                        $str .= '<a href="' . $action->getRoute($dataType->name) . '"
                                        title="' . $action->getTitle() . '"' . str_replace('pull-right', 'office-action-button', $action->convertAttributesToHtml()) . '>' .
                                            '<i class="' . $action->getIcon() . '"></i> <span class="hidden-xs hidden-sm">
                                        ' . $action->getTitle() . '</span></a>';
                                    }
                                } else {
                                    if ($user->can($policies, $office)) {
                                        $str .= '<a href="' . $action->getRoute($dataType->name) . '"
                                        title="' . $action->getTitle() . '"' . str_replace('pull-right', 'office-action-button', $action->convertAttributesToHtml()) . '>' .
                                            '<i class="' . $action->getIcon() . '"></i> <span class="hidden-xs hidden-sm">
                                        ' . $action->getTitle() . '</span></a>';
                                    }
                                }
                            }
                        }
                    }

                    return $str;
                })
                ->rawColumns(['action'])
                ->toJson();

        } catch (\Exception $ex) {
            return response()->json([], 404);
        }
    }

    public function parentOffices(Request $request)
    {
        $slug = $this->getSlug($request);
        $page = $request->input('page');
        $on_page = 50;
        $search = $request->input('search', false);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $filterConditions = $request->input('filter_condition');

        $rows = $request->input('method', 'add') == 'add' ? $dataType->addRows : $dataType->editRows;
        if (!(!empty($filterConditions) && is_array($filterConditions))) {
            return response()->json([
                'results' => [],
                'pagination' => [
                    'more' => false,
                ],
            ]);
        }

        try {
            foreach ($rows as $key => $row) {
                if ($row->field === $request->input('type')) {
                    $options = $row->details;
                    $additionalOptions = $row->additional_details;

                    $skip = $on_page * ($page - 1);

                    /** @var Builder $model */

                    $model = app($options->model);

                    $columnsToSelect = [$options->key, $options->label];
                    if (!empty($additionalOptions) && isset($additionalOptions->additional_fields) && is_array($additionalOptions->additional_fields)) {
                        $columnsToSelect = array_merge($columnsToSelect, $additionalOptions->additional_fields);
                    }
                    $model = $model->select($columnsToSelect);
                    $filterCollection = collect($filterConditions)->filter(function ($item, $key) {
                        return isset($item['field']) && isset($item['value']);
                    })->flatMap(function ($item, $key) {
                        return [$item['field'] => $item['value']];
                    });
                    if ($filterCollection->has('office_type')) {
                        switch ($filterCollection->get('office_type')) {
                            case OfficeInformation::OFFICE_TYPE_DIVISION:
                                $filterCollection->forget(['loc_division_id', 'loc_district_id', 'loc_upazila_id', 'loc_union_id']);
                                $model->whereIn('office_type', [OfficeInformation::OFFICE_TYPE_MINISTRY, OfficeInformation::OFFICE_TYPE_HQ]);
                                break;
                            case OfficeInformation::OFFICE_TYPE_REGIONAL:
                                $filterCollection->forget(['loc_district_id', 'loc_upazila_id', 'loc_union_id']);
                                $model->where('office_type', OfficeInformation::OFFICE_TYPE_DIVISION);
                                break;
                            case OfficeInformation::OFFICE_TYPE_ZILA:
                                $filterCollection->forget(['loc_district_id', 'loc_upazila_id', 'loc_union_id']);
                                $model->whereIn('office_type', [OfficeInformation::OFFICE_TYPE_DIVISION, OfficeInformation::OFFICE_TYPE_REGIONAL]);
                                break;
                            case OfficeInformation::OFFICE_TYPE_UPAZILA:
                                $filterCollection->forget(['loc_upazila_id', 'loc_union_id']);
                                $model->where('office_type', OfficeInformation::OFFICE_TYPE_ZILA);
                                break;
                            case OfficeInformation::OFFICE_TYPE_UNION:
                                $filterCollection->forget(['loc_union_id']);
                                $model->where('office_type', OfficeInformation::OFFICE_TYPE_UPAZILA);
                                break;
                            default:
                                return response()->json([
                                    'results' => [],
                                    'pagination' => [
                                        'more' => false,
                                    ],
                                ]);
                        }
                    }

                    $filterCollection->forget('office_type')->each(function ($value, $columnName) use ($model) {
                        if (!empty($columnName)) {
                            $model->where($columnName, $value);
                        }
                    });

                    /** If search query, use LIKE to filter results depending on field label */
                    if ($search) {
                        if (!empty($additionalOptions) && isset($additionalOptions->search_fields) && is_array($additionalOptions->search_fields)) {
                            $searchFields = $additionalOptions->search_fields;
                            $model->where(function (Builder $q) use ($search, $searchFields) {
                                foreach ($searchFields as $searchField) {
                                    $q->orWhere($searchField, 'LIKE', '%' . $search . '%');
                                }
                                return $q;
                            });
                        } else {
                            $model->where($options->label, 'LIKE', '%' . $search . '%');
                        }

                        $total_count = $model->count();
                        $relationshipOptions = $model->take($on_page)->skip($skip)
                            ->get();
                    } else {
                        $total_count = $model->count();
                        $relationshipOptions = $model->take($on_page)->skip($skip)->get();
                    }

                    $label = $options->label;
                    if (!empty($additionalOptions) && isset($additionalOptions->label)) {
                        $label = $additionalOptions->label;
                    }

                    $results = [];
                    foreach ($relationshipOptions as $relationshipOption) {
                        $results[] = [
                            'id' => $relationshipOption->{$options->key},
                            'text' => $relationshipOption->{$label},
                        ];
                    }

                    return response()->json([
                        'results' => $results,
                        'pagination' => [
                            'more' => ($total_count > ($skip + $on_page)),
                        ],
                    ]);
                }
            }
        } catch (\Exception $ex) {
            Log::debug($ex->getTraceAsString());
            return response()->json(["error" => $ex->getMessage()], 500);

        }
        return response()->json(['message' => __("Not Found")], 404);
    }
}
