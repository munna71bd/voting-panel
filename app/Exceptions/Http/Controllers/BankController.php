<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;
use App\Http\Controllers\Voyager\VoyagerBaseController;
use App\Models\Bank;
use App\Repositories\BankRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;

class BankController extends Controller
{
    protected $bankRepository;

    public function __construct(BankRepository $bankRepository)
    {
        $this->bankRepository = $bankRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('browse', $this->bankRepository->getModel());
        $banks = $this->bankRepository->index();
        return view("voyager::banks.browse", compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('browse', $this->bankRepository->getModel());

        $bank = $this->bankRepository->getModel();

        return view("voyager::banks.edit-add", compact('bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('browse', app(Bank::class));

        $data = $request->all();

        $this->bankRepository->validator($data)->validate();

        $data['created_by'] = Auth::id();

        try {

            $this->bankRepository->create($data);

        } catch (\Throwable $exception) {
            Log::debug($exception->getMessage());

            return Response::json([
                'alert-type' => 'error',
                'message' => __('voyager::generic.something_wrong')
            ], 404);
        }

        return Response::json([
            'message' => __('voyager::generic.successfully_created'),
            'alert-type' => 'success'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bank $bank
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Bank $bank)
    {
        $this->authorize('read', app(Bank::class));

        return view("voyager::banks.read", compact('bank'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Bank $bank
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Bank $bank)
    {
        $this->authorize('edit', $bank);

        return view("voyager::banks.edit-add", compact('bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Bank $bank
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Bank $bank): JsonResponse
    {
        $this->authorize('edit', $bank);

        $data = $request->all();
        $this->bankRepository->validator($data, $bank->id)->validate();

        $data['updated_by'] = Auth::id();

        try {

            $this->bankRepository->update($data, $bank);

        } catch (\Throwable $exception){
            Log::debug($exception->getMessage());

            return Response::json([
                'alert-type' => 'error',
                'message' => __('voyager::generic.something_wrong')
            ], 404);
        }

        return Response::json([
            'message' => __('voyager::generic.successfully_updated'),
            'alert-type' => 'success'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        //
    }
}
