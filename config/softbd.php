<?php
return [
    'gender' => [
        1 => 'Male',
        2 => 'Female',
        3 => 'Transgender'
    ],
    'file_extension' => [
        0 => 'png',
        1 => 'jpg',
        2 => 'jpeg',
        3 => 'pdf',
    ],
    'file_extension_r' => [
        'png' => 0,
        'jpg' => 1,
        'jpeg' => 2,
        'pdf' => 3,
    ],
    

    'candidates' => [
        
        1 => [
            'name' => "ক. পারভেজুর রহমান",
            'picture_src' => "assets/img/c1.jpg"
        ],
        2 => [
            'name' => "খ. আশরাফুল আলম",
            'picture_src' => "assets/img/c2.jpg"
        ]
        
       

    ],

    'vote_start_date_time' => '2022-05-14 09:00:00',
    'vote_end_date_time' => '2022-05-15 09:00:00',
    'result_key' => 'D5yYNps3Jy',
];
