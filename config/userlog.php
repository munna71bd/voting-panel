<?php


return [
    'allowed_methods' => [
        'index',
        'create',
        'store',
        'update',
        'delete',
        'sourceOfFund',
        'halfYearlyLoanSavingReportA1',
        'login',
        'logout',

    ],
    'allowed_method_actions' => [
        'index' => 'viewed',
        'create' => 'opened create page',
        'store' => 'saved',
        'update' => 'updated',
        'delete' => 'deleted',
        'sourceOfFund' => 'viewed',
        'halfYearlyLoanSavingReportA1' => 'viewed',
        'login' => 'viewed',
        'logout' => 'logout',
    ],

    'module_names' => [
        'Notification' => 'Notification',
        'Cdc' => 'CDC',
        'Scg' => 'SCG',
        'Scm' => 'Member',
        'Saving' => 'Savings',
        'Loan' => 'Loan',
        'Book' => 'Member Passbook',
        'FinancialStatement' => 'Financial Statement',
        'Report' => 'Report',
        'Http' => 'Root',
        'Bank' => 'Bank',

    ]

];
