@php
    $edit = !is_null($dataTypeContent->getKey());
    $add  = is_null($dataTypeContent->getKey());
    $title = __(($edit ? 'Edit ' : 'Add ') . 'User');
@endphp
@extends('voyager::master')

@section('page_title', $title)

@section('css')


    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .error {
            color: red;
        }

        .form-group label.required::after {
            color: red;
            content: "*";
            left: 5px;
            position: relative;
        }

        [class*="col-"] {
            margin-bottom: 0px !important;
        }

        .voyager input[type="file"] {
            font-size: 14px;
        }

        #cluster_div, #cdc_div, #scg_div {
            display: none;
        }

        #cluster_id, #cdc_id, #scg_id {
            display: block;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i>
        {{ $title }}

        @can('browse', app(\App\Models\User::class))
            <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
                <span class="glyphicon glyphicon-list"></span>&nbsp;
                {{ __('voyager::generic.return_to_list') }}
            </a>
        @endcan
    </h1>
@stop

@section('content')
    <div class="page-content container-fluid">
        <form class="form-edit-add" role="form" id="user_add_edit"
              action="@if(!is_null($dataTypeContent->getKey())){{ route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) }}@else{{ route('voyager.'.$dataType->slug.'.store') }}@endif"
              method="POST" enctype="multipart/form-data" autocomplete="off">
            <!-- PUT Method if we are editing -->
            @if(isset($dataTypeContent->id))
                {{ method_field("PUT") }}
                <input type="hidden" name="id" id="id" value="{{$dataTypeContent->getKey()}}"/>
            @endif
            {{ csrf_field() }}
            <div class="row">
                <div class="col-xl-8">
                    <div class="panel panel-bordered">
                        {{-- <div class="panel"> --}}
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <div class="panel-body">
                            <div class="row">
                                @if(auth()->user()->id != $dataTypeContent->id)


                                    @php
                                        $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                    @endphp
                                    @can('editRoles', $dataTypeContent)
                                        <div class="col-xl-6">
                                            <div class="form-group">
                                                <label
                                                    for="default_role">{{ __('Role') }}</label>
                                            @php
                                                $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                                                $row     = $dataTypeRows->where('field', 'user_belongsto_role_relationship')->first();
                                                $options = $row->details;
                                            @endphp
                                            <!--
                                            @include('voyager::formfields.relationship',  ['options' => $options, 'row' => $row])
                                                -->

                                                <select name="role_id" id="role_id"
                                                        class="form-controll select2">
                                                    <option @if(!$edit) selected @endif
                                                    disabled>{{ __('Select Role') }}</option>
                                                    @php
                                                        $fixed_roles = [1,4];
                                                        $roles = \App\Models\Role::whereNotIn('id',$fixed_roles)->get();


                                                    @endphp

                                                    @foreach($roles as $key => $type)

                                                        <option
                                                            @if($type->id==$dataTypeContent->role_id)selected
                                                            @endif value="{{$type->id}}">{{__($type->display_name)}}
                                                        </option>

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @if($edit)

                                            <div style="display: none;" class="col-xl-6">
                                                <div class="form-group">
                                                    <label
                                                        for="additional_roles">{{ __('voyager::profile.roles_additional') }}</label>
                                                    @php
                                                        $row     = $dataTypeRows->where('field', 'user_belongstomany_role_relationship')->first();
                                                        $options = $row->details;
                                                    @endphp
                                                    @include('voyager::formfields.relationship',  ['options' => $options, 'row' => $row])
                                                </div>
                                            </div>
                                        @endif
                                    @endcan


                                    <div id="department_div" class="col-xl-6">
                                        <div class="form-group">
                                            <label for="department_id">{{ __('Department') }}</label>
                                            <select class="form-control select2"
                                                    id="department_id"
                                                    name="department_id"
                                                    data-placeholder="{{__("Department")}}">

                                                    @foreach(\Module\Notification\Models\Department::where('row_status',1)->orderBy('created_at','DESC')->get() as $d)
                                                        <option   @if( $dataTypeContent->userGroup &&  $dataTypeContent->userGroup->department && $d->id==$dataTypeContent->userGroup->department->id)selected
                                                                  @endif value="{{$d->id}}" >{{$d->title}}</option>
                                                    @endforeach
                                            </select>

                                        </div>


                                    </div>



                                    <div id="group_div" class="col-xl-6">
                                        <div class="form-group">
                                            <label for="group_id">{{ __('Group') }}</label>
                                            <select class="form-control select2"
                                                    id="group_id"
                                                    name="group_id"
                                                    data-placeholder="{{__("Group")}}">
                                                @foreach(\Module\Notification\Models\Group::where('row_status',1)->orderBy('created_at','DESC')->get() as $d)
                                                    <option @if( $dataTypeContent->userGroup && $d->id==$dataTypeContent->userGroup->group->id)selected
                                                            @endif value="{{$d->id}}">{{$d->title}}</option>
                                                @endforeach
                                            </select>

                                        </div>


                                    </div>





                                @else
                                    <input type="hidden" name="user_type" id="user_type"
                                           value="{{$dataTypeContent->user_type}}">
                                @endif
                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label for="name">{{ __('voyager::generic.name') }}</label>
                                        <input type="text" class="form-control" id="name" name="name"
                                               placeholder="{{ __('voyager::generic.name') }}"
                                               value="{{ old('name', $dataTypeContent->name) }}">
                                    </div>


                                </div>
                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label for="username">{{ __('generic.username') }}</label>
                                        <input type="text" class="form-control" id="username" name="username"
                                               placeholder="{{ __('generic.username') }}"
                                               value="{{ old('username', $dataTypeContent->username) }}">
                                    </div>
                                </div>

                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label for="email">{{ __('Email') }}</label>
                                        <input type="email" class="form-control" id="email" name="email"
                                               placeholder="{{ __('Email') }}"
                                               value="{{ old('email', $dataTypeContent->email ) }}"
                                               aria-describedby="helpBlock_email">

                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <div class="form-group">
                                        <label for="password">{{ __('voyager::generic.password') }}</label>
                                        @if(isset($dataTypeContent->password))
                                            <br>
                                            <small>{{ __('voyager::profile.password_hint') }}</small>
                                        @endif
                                        <input type="password" class="form-control" id="password" name="password"
                                               value=""
                                               autocomplete="new-password">
                                        <small class="d-block text-danger" id="confirm_password_message"></small>
                                    </div>

                                </div>

                                <div class="col-xl-6">

                                    <div class="form-group">
                                        <label for="password_confirmation">{{ __('Confirm Password') }}
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="password" class="form-control" id="password_confirmation"
                                               name="password_confirmation" value=""
                                               autocomplete="new-password">
                                    </div>
                                </div>


                                @if((auth()->user()->id != $dataTypeContent->id) && $edit)

                                    @can('changeUserStatus', $dataTypeContent)
                                        <div class="col-xl-6">
                                            <div class="form-group">
                                                <label for="row_status">{{__('generic.row_status')}}</label>
                                                <select name="row_status" id="row_status"
                                                        class="form-control select2"
                                                >
                                                    <option value="1"
                                                            @if(old('row_status', $dataTypeContent->row_status) == "1") selected @endif>{{__('generic.active')}}</option>
                                                    <option value="0"
                                                            @if(old('row_status', $dataTypeContent->row_status) == "0") selected @endif>{{__('generic.inactive')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    @endcan
                                @endif
                                <input type="hidden" name="locale" value="bn" id="locale">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-body">
                            <div class="form-group">
                                @if(isset($dataTypeContent->avatar))
                                    <img id="user_img"
                                         src="{{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }}"
                                         style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;"/>
                                @endif
                                <input accept="image/jpeg, image/png" onchange="previewFile()" type="file"
                                       data-name="avatar" name="avatar">
                            </div>
                        </div>
                    </div>
                    <div class="panel panel panel-bordered panel-warning">
                        <div class="panel-body text-right">
                            <div class="form-group">
                                <!--user permission end here-->
                                <button type="submit" class="btn btn-primary save">
                                    {{ __('voyager::generic.save') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--user permission start from here-->
            @if(Auth::user()->id != $dataTypeContent->id && !is_null($dataTypeContent->getKey()))
                @can('editUserPermission', $dataTypeContent)
                    <label for="permission">{{ __('voyager::generic.permissions') }}</label><br>

                    <ul class="permissions checkbox">

                        @foreach($allPermissionTableNames as $table => $permission)
                            <li>
                                <input type="checkbox" id="{{$table|'general_permissions'}}"
                                       class="permission-group">
                                <label for="{{$table | "general_permissions"}}">
                                    @if(!empty($table))
                                        <strong>{{\Illuminate\Support\Str::title(str_replace('_',' ', $table))}}</strong>
                                    @else
                                        <strong>{{__("generic.general_permissions")}}</strong>
                                    @endif
                                </label>
                                <ul>
                                    @foreach($permission as $perm)
                                        <li>
                                            <input type="checkbox" id="permission-{{$perm->id}}"
                                                   name="permissions[]"
                                                   class="the-permission" value="{{$perm->id}}"
                                                   @if(in_array($perm->key, $userPermissions)) checked @endif>
                                            <label
                                                @if(in_array($perm->key, $customPermissions) ) class="user-custom-permission"
                                                @endif for="permission-{{$perm->id}}">{{\Illuminate\Support\Str::title(str_replace('_', ' ', $perm->key))}}</label>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                @endcan
            @endif
        </form>

        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
              enctype="multipart/form-data" style="width:0px;height:0;overflow:hidden">
            {{ csrf_field() }}
            <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="{{ $dataType->slug }}">
        </form>
    </div>
@stop

@section('javascript')
    <script type="text/javascript" src="{{ voyager_asset('js/form.js') }}"></script>
    <script>
        $('document').ready(function () {

            $('.toggleswitch').bootstrapToggle();

            @if(Auth::user()->id != $dataTypeContent->id && !is_null($dataTypeContent->getKey()))
            @can('editUserPermission', $dataTypeContent)
            $('.permission-select-all').on('click', function () {
                $('ul.permissions').find("input[type='checkbox']").prop('checked', true);
                return false;
            });

            $('.permission-deselect-all').on('click', function () {
                $('ul.permissions').find("input[type='checkbox']").prop('checked', false);
                return false;
            });

            function parentChecked() {
                $('.permission-group').each(function () {
                    var allChecked = true;
                    $(this).siblings('ul').find("input[type='checkbox']").each(function () {
                        if (!this.checked) allChecked = false;
                    });
                    $(this).prop('checked', allChecked);
                });
            }

            parentChecked();

            $('.the-permission').on('change', function () {
                parentChecked();
            });

            $('.the-permission').click(function () {

                var $this = $(this);
                var customClass = 'permission-' + $this.val();
                var user_id = {{$dataTypeContent->id}};
                let checked = $this.prop('checked');

                if (checked) {
                    if (!confirm("{{__('Are you sure you you want to add this permission?')}}")) {
                        return false;
                    }
                } else if (!confirm("{{__('Are you sure you want to remove this permission?')}}")) {
                    return false;
                }
                $this.hide();
                $this.parent().prepend("<img src='" + "{{asset('ajax-loader-tiny.gif')}}" + "' >");
                $.ajax({
                    data: {
                        permission_id: $this.val(),
                        user_id: user_id,
                        checkboxStatus: $(this).is(':checked'),
                    },
                    url: "{{ route('change_user_permission')  }}",
                    dataType: 'json',
                    type: 'POST',
                }).done(function (data, textStatus, jqXHR) {
                    $this.parent().find('img').remove();
                    $this.show();
                    $('label[for="' + customClass + '"]').addClass('user-custom-permission');
                    toastr.success("{{__("Permission has been updated successfully")}}");
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    $this.parent().find('img').remove();
                    $this.show();
                    toastr.error("{{__("Sorry! problem in updating the permission")}}");
                });
            });

            $('.permission-group').on('change', function () {
                var $this = $(this);
                var user_id = {{$dataTypeContent->id}};
                var checked = $this.prop('checked');
                if (checked) {
                    if (!confirm("{{__('Are you sure you you want to add all permissions under this group?')}}")) {
                        $this.prop('checked', false);
                        return false;
                    }
                } else if (!confirm("{{__('Are you sure you want to remove all permissions under this group?')}}")) {
                    $this.prop('checked', true);
                    return false;
                }
                $this.hide();
                $this.parent().prepend("<img src='" + "{{asset('ajax-loader-tiny.gif')}}" + "' >");
                var allPermissionIds = [];
                $(this).siblings('ul').find("input[type='checkbox']").each(function (key, element) {
                    if ($(element).val()) {
                        allPermissionIds.push($(element).val());
                    }
                });

                $.ajax({
                    data: {
                        permission_ids: allPermissionIds,
                        user_id: user_id,
                        checkboxStatus: checked,
                    },
                    url: "{{ route('change_user_permissions')  }}",
                    dataType: 'json',
                    type: 'POST',
                }).done(function (data, textStatus, jqXHR) {
                    $this.parent().find('img').remove();
                    $this.show();
                    $this.siblings('ul').find("input[type='checkbox']").each(function (key, element) {
                        if ($(element).val()) {
                            $('label[for="permission-' + $(element).val() + '"]').addClass('user-custom-permission');
                        }
                        $(element).prop('checked', checked);
                    });
                    toastr.success("{{__("Permissions have been updated successfully")}}");
                }).fail(function (jqXHR, textStatus, errorThrown) {
                    $this.parent().find('img').remove();
                    $this.show();
                    toastr.error("{{__("Sorry! problem in updating permissions")}}");
                });

            });
            @endcan
            @endif

            @if(auth()->user()->id != $dataTypeContent->id)
            $('#data_access_area').change(function (e) {
                var dataAccessArea = $(this).val();
                $(".geo-location").each(function (index, elem) {
                    $(elem).prop('required', false).parent().find('label').removeClass('required');
                });

            });
            @endif
        });


    </script>
@stop
<!-- frontend validation for bread -->
@section('validation')
    <script src="{{ asset('/backend_resources/js/jquery_validation_combine.js') }}"></script>
    @if(app()->getLocale() === 'bn')
        <script src="{{ asset('/backend_resources/js/validation_localization_bn.js') }}"></script>
    @endif

    <script>


        let userEditUrl = '{{route('voyager.users.edit', ['user' => ($dataTypeContent->id ? $dataTypeContent->id : -99) ])}}';

        let $usernameMinLen = {{(int)setting('admin.username_min_len')}};
        let $passwordMinLen = {{(int)setting('admin.password_min_len')}};
        $('document').ready(function () {

            /** Adding Jquery Validation rules  */

            jQuery.extend(jQuery.validator.messages, {
                required: "{{__('front_vallidations.required')}}",
            });


            $.validator.addMethod(
                "regex",
                function (value, element, regexp) {
                    var re = new RegExp(regexp);
                    return this.optional(element) || re.test(value);
                },
                "{{__("Invalid Input")}}"
            );

            $.validator.addMethod(
                "password_validate",
                function (value, element, minlength) {
                    if (value.length < $passwordMinLen) {
                        return false;
                    }
                    if (value.search(/[\d]/) === -1) {
                        return false;
                    }
                    if (value.search(/[a-z]/) === -1) {
                        return false;
                    }
                    if (value.search(/[A-Z]/) === -1) {
                        return false;
                    }
                    return true;
                },
                "{{__("front_vallidations.user.password.password_validate")}}"
            );
            var addEditForm = $('form.form-edit-add');

            if (addEditForm.length) {
                var rules = {
                    name: {
                        "required": true,
                        "minlength": 2
                    },

                    username: {
                        "required": true,
                        "minlength": $usernameMinLen,
                        "remote": {
                            "url": "/admin/users/server-validate",
                            "type": "post",
                            "data": {
                                "field": "username",
                                "except_field": "id",
                                "except_val": function () {
                                    return $('#id').val();
                                }
                            }
                        }
                    },

                    'department_id': {
                        required: true,
                    },

                    'group_id': {
                        required: true,
                    },


                    @if(!$edit)
                    password: {
                        required: @if(!$edit) true @else false @endif,
                        password_validate: 6
                    },
                    @endif
                    password_confirmation: {
                        equalTo: "#password"
                    },
                    role_id: {
                        required: true
                    },
                    'avatar': {
                        extension: "jpg|jpeg|png"
                    }

                };
                $.each(rules, function (key, value) {
                    if (typeof value.required != 'undefined' && value.required) {
                        $('input[name="' + key + '"], select[name="' + key + '"]').prop('required', true).parent().find('label').addClass('required');
                    }
                });

                addEditForm.validate({
                    onkeyup: false,
                    //ignore: [],
                    rules: rules,
                    messages: {
                        username: {
                            remote: "{{__("front_vallidations.user.username.remote")}}"
                        },
                        cell_phone: {
                            regex: "{{__("front_vallidations.user.cell_phone.regex")}}"
                        },
                        password_confirmation: {
                            equalTo: "{{__("front_vallidations.user.password_confirmation.equalTo")}}"
                        }
                    },
                    errorElement: "em",
                    errorPlacement: function (error, element) {

                        error.addClass("help-block");

                        element.parents(".form-group").addClass("has-feedback");

                        if (element.prop("type") === "checkbox") {
                            error.insertAfter(element.parent("label"));
                        } else if (element.hasClass('select2-ajax') || element.hasClass('select2') || element.hasClass('select2-ajax-custom')) {
                            error.insertAfter(element.parents(".form-group").find('.select2-container'));
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    success: function (label, element) {
                    },
                    highlight: function (element, errorClass, validClass) {
                        $(element).parents(".form-group").addClass("has-error").removeClass("has-success");
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        $(element).parents(".form-group").addClass("has-success").removeClass("has-error");
                    },
                    submitHandler: function (form) {
                        $('#voyager-loader').show();
                        var formData = new FormData(form);
                        $.ajax({
                            url: addEditForm.attr('action'),
                            type: "POST",
                            processData: false,
                            contentType: false,
                            data: formData
                        })
                            .done((data) => {
                                console.log(data);
                                toastr.success(data.message);
                                @if(!$edit)
                                if (typeof data.user_id !== 'undefined') {
                                    userEditUrl = userEditUrl.replace(/\-99/g, data.user_id);
                                    location.href = userEditUrl;
                                }
                                @endif
                            })
                            .fail((data) => {
                                data.responseJSON.errors !== undefined
                                    ? $.each(data.responseJSON.errors, (key, value) => toastr.error(value))
                                    : toastr.error(data.responseJSON.message);
                            })
                            .always(() => {
                                $('#voyager-loader').hide();
                            });
                    }
                });
            }
        });

        function previewFile() {
            const file = document.querySelector('input[type=file]').files[0];
            const reader = new FileReader();

            reader.addEventListener("load", function () {
                $('#user_img').attr('src', reader.result)
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
        }


        $('#role_id').on('change', function () {
            $('#department_div').hide();
            $('#group_div').hide();

            is_department_required = false;
            is_group_required = false;

            if (this.value == 2) {
                $('#department_div').show();
                is_department_required = true;

            } else if (this.value == 3) {

                $('#group_div').show();
                is_group_required = true;

            }
        });

        var x = $('#role_id').val();

        $('#role_id').val(x).change();

        $('#department_div').hide();
        $('#group_div').hide();
        $('#group_div').hide();

        is_department_required = false;
        is_group_required = false;

        if (x == 2) {
            $('#department_div').show();
            is_department_required = true;

        } else if (x == 3) {

            $('#group_div').show();
            is_group_required = true;

        }


        $(document).ajaxSuccess(function (event, xhr, settings) {


        });


    </script>

@stop

