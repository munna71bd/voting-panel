<link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
<link rel="apple-touch-icon" type="image/png" href="{{asset('backend_resources/img/logo.png')}}"/>
<meta name="apple-mobile-web-app-title" content="CodePen">

<link rel="shortcut icon" type="image/x-icon" href="{{asset('backend_resources/img/logo.png')}}"/>

<link rel="mask-icon" type="image/x-icon" href="{{asset('backend_resources/img/logo.png')}}" color="#111"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="admin login">
<title> {{ __('brand_title') }}</title>
<style>
    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }

    body {
        width: 100%;
        min-height: 100vh;
        background: linear-gradient(-45deg, rgb(127, 116, 208), rgb(193, 191, 250));


    }

    .Alert {
        padding: 10px;
    }

    @media (min-width: 1024px)
        .Alert {
            padding: 10px;
        }

        .box {
            height: 70%;
            width: 70%;
            background: linear-gradient(-45deg, rgb(190, 190, 240), rgb(123, 120, 201));
            position: relative;
            overflow: hidden;
            border-radius: 10px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }

        .graphic1 {
            height: 180px;
            width: 200px;
            background: rgb(109, 99, 190);
            position: absolute;
            top: -120px;
            right: 20px;
            transform: rotate(45deg);
            border-radius: 20px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }

        .graphic2 {
            height: 600px;
            width: 180px;
            background: rgb(107, 115, 187);
            position: absolute;
            top: -100px;
            right: -15%;
            transform: rotate(45deg);
            border-radius: 30px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }

        .graphic3 {
            height: 300px;
            width: 250px;
            background: rgb(107, 100, 172);
            position: absolute;
            right: -30px;
            bottom: -160px;
            transform: rotate(45deg);
            border-radius: 30px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }

        .graphic4 {
            height: 600px;
            width: 80%;
            position: absolute;
            background: #fff;
            transform: rotate(0deg);
            position: absolute;
            left: -30%;
            top: -10%;
            border-radius: 100px 0 0 0;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);

        }

        form {
            position: absolute;
            width: 45%;
            top: 150px;
            left: 30px;
            font-size: 20px;
        }

        form span {
            margin-bottom: 30px;
            display: block;
            width: 100%;
            border-bottom: 2px solid grey;
        }

        form span input {
            border: none;
            margin-left: 10px;
            height: 2rem;

        }

        form span input:focus {
            outline: none;
        }

        form span ion-icon {
            font-size: 20px;
            color: rgb(102, 16, 214);
        }

        .box .social {
            position: absolute;
            bottom: 60px;
            right: 30px;
            color: #fff;
            font-family: 'nunito', sans-serif;

        }

        .social ion-icon {
            margin-top: 15px;
            font-size: 18px;
            margin-right: 10px;
        }

        .box button {
            position: absolute;
            border: 2px solid grey;
            background: #fff;
            left: 10%;
            width: 230px;
            color: rgb(109, 99, 190);
            font-weight: bolder;
            font-size: 1.2rem;
            border-radius: 30px;
            padding: 6px;
            box-shadow: -2px 5px 10px 2px rgb(180, 180, 245);
            cursor: pointer;
        }

        button ion-icon {
            font-size: 1.5rem;
            float: right;
            margin-right: 20px;
            width: 20%;
        }

        a {
            color: #fff;
            text-decoration: none;
        }

        .market-btn {
            display: inline-block;
            padding: 0.3125rem 0.875rem;
            padding-left: 2.8125rem;
            -webkit-transition: border-color 0.25s ease-in-out, background-color 0.25s ease-in-out;
            transition: border-color 0.25s ease-in-out, background-color 0.25s ease-in-out;
            border: 1px solid #e7e7e7;
            background-position: center left 0.75rem;
            background-color: #fff;
            background-size: 1.5rem 1.5rem;
            background-repeat: no-repeat;
            text-decoration: none;
        }

        .market-btn .market-button-title {
            display: block;
            color: #222;
            font-size: 1.125rem;
        }

        .market-btn .market-button-subtitle {
            display: block;
            margin-bottom: -0.25rem;
            color: #888;
            font-size: 0.75rem;
        }

        .market-btn:hover {
            background-color: #f7f7f7;
            text-decoration: none;
        }


        .google-btn {
            color: #ff5722;
            font-weight: bold;
            background-image: url(data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMC4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4Ij4KPHBvbHlnb24gc3R5bGU9ImZpbGw6IzVDREFERDsiIHBvaW50cz0iMjkuNTMsMCAyOS41MywyNTEuNTA5IDI5LjUzLDUxMiAyOTkuMDA0LDI1MS41MDkgIi8+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiNCREVDQzQ7IiBwb2ludHM9IjM2OS4wNjcsMTgwLjU0NyAyNjIuMTc1LDExOS40NjcgMjkuNTMsMCAyOTkuMDA0LDI1MS41MDkgIi8+Cjxwb2x5Z29uIHN0eWxlPSJmaWxsOiNEQzY4QTE7IiBwb2ludHM9IjI5LjUzLDUxMiAyOS41Myw1MTIgMjYyLjE3NSwzODMuNTUxIDM2OS4wNjcsMzIyLjQ3IDI5OS4wMDQsMjUxLjUwOSAiLz4KPHBhdGggc3R5bGU9ImZpbGw6I0ZGQ0E5NjsiIGQ9Ik0zNjkuMDY3LDE4MC41NDdsLTcwLjA2Myw3MC45NjFsNzAuMDYzLDcwLjk2MWwxMDguNjg4LTYyLjg3N2M2LjI4OC0zLjU5Myw2LjI4OC0xMS42NzcsMC0xNS4yNyAgTDM2OS4wNjcsMTgwLjU0N3oiLz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==);
            margin-top: 30px;
        }

        main {
            width: 100%;
            font-size: 25px;
        }

        h1 {
            font-family: 'Josefin Sans', sans-serif;
            font-weight: 100;
            letter-spacing: -2px;
            text-align: center;
            font-size: 40px;
        }

        /*#vote-list {
            max-width: 60%;
            margin: 0 auto;
        }*/
        #model {
            opacity: 0;
        }

        .vote-list-element {
            padding: 5px;
            animation: comeOn .5s linear forwards;
        }

        .vote-list-element > div {
            padding: 15px;

            display: flex;
        }

        .vote-list-element > div > div {
            width: 50%;
            padding: 5px;
        }

        .vote-name {
            display: flex;
            align-items: center;
        }


        .vote-percentage-block.active > label {
            opacity: 1;
            transform: translateX(30px);
        }

        @keyframes comeOn {
            0% {
                transform: translateX(-20px);
                opacity: 0;
            }
            50% {
                transform: translateX(10px);
                opacity: 1;
            }
            100% {
                transform: translateX(0px);
                opacity: 1;
            }
        }

</style>

<center>

    <div class="box-p">

        <br>
        <h2 style="color: white">
            <span>
             <img height="70px;" src="{{asset('assets/img/logo.png')}}">
        </span>
            {{__('brand_title')}}
        </h2>

        <br>

        <div class="box">

            <main class="results">
                <h2>ভোট কাস্টিং</h2>
                <hr>
                <div id="vote-list">
                    <div id="model" class="vote-list-element">

                        <b>মোট ভোটার: <b
                                    style="font-size: 30px;color: yellow;">{{en2bn(\Illuminate\Support\Facades\DB::table('users')->where('role_id',2)->count())}} </b>
                            জন এবং মোট ভোট দিয়েছেন: <b
                                    style="font-size: 30px;color: yellow;">{{en2bn($total_vote_taken)}} </b>
                            জন</b>

                        <hr>

                        <div class="vote-details">
                            <div class="vote-name">
                                <label>নাম : {{config('softbd.candidates')[1]['name']}}</label>
                            </div>
                            <div class="vote-percentage">
                                <div class="vote-percentage-block">
                                    <label>ভোট পেয়েছেন: <b
                                                style="color: #fff;font-size: 50px;">{{$c1 ? en2bn($c1->total_vote) : en2bn(0)}}</b>
                                        টি </label>
                                </div>
                            </div>
                        </div>
                        <div class="vote-details">
                            <div class="vote-name">
                                <label>নাম : {{config('softbd.candidates')[2]['name']}}</label>
                            </div>
                            <div class="vote-percentage">
                                <div class="vote-percentage-block">
                                    <label>ভোট পেয়েছেন: <b
                                                style="color: #fff;font-size: 50px;">{{$c2 ? en2bn($c2->total_vote) : en2bn(0)}}</b>
                                        টি </label>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </main>


        </div>
        <div class="row copyright">
            <div class="col">
                <p>পরিকল্পনা ও বাস্তবায়নে</p>
                <a href="#">

                    {{__('client_title')}}
                </a>

            </div>


        </div>
    </div>


</center>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>

    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });

</script>

