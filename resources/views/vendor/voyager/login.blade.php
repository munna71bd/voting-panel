<link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
<link rel="apple-touch-icon" type="image/png" href="{{asset('backend_resources/img/logo2.png')}}"/>
<meta name="apple-mobile-web-app-title" content="CodePen">

<link rel="shortcut icon" type="image/x-icon" href="{{asset('backend_resources/img/logo2.png')}}"/>

<link rel="mask-icon" type="image/x-icon" href="{{asset('backend_resources/img/logo2.png')}}" color="#111"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="admin login">
<title> {{ __('brand_title') }}</title>
<style>
    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }

    body {
        width: 100%;
        min-height: 100vh;
        background: linear-gradient(-45deg, rgb(127, 116, 208), rgb(193, 191, 250));


    }

    .Alert {
        padding: 10px;
    }

    @media (min-width: 1024px)
        .Alert {
            padding: 10px;
        }

        .box {
            height: 70%;
            width: 70%;
            background: linear-gradient(-45deg, rgb(190, 190, 240), rgb(123, 120, 201));
            position: relative;
            overflow: hidden;
            border-radius: 10px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }

        .graphic1 {
            height: 180px;
            width: 200px;
            background: rgb(109, 99, 190);
            position: absolute;
            top: -120px;
            right: 20px;
            transform: rotate(45deg);
            border-radius: 20px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }

        .graphic2 {
            height: 600px;
            width: 180px;
            background: rgb(107, 115, 187);
            position: absolute;
            top: -100px;
            right: -15%;
            transform: rotate(45deg);
            border-radius: 30px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }

        .graphic3 {
            height: 300px;
            width: 250px;
            background: rgb(107, 100, 172);
            position: absolute;
            right: -30px;
            bottom: -160px;
            transform: rotate(45deg);
            border-radius: 30px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }

        .graphic4 {
            height: 600px;
            width: 80%;
            position: absolute;
            background: #fff;
            transform: rotate(0deg);
            position: absolute;
            left: -30%;
            top: -10%;
            border-radius: 100px 0 0 0;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);

        }

        form {
            position: absolute;
            width: 45%;
            top: 150px;
            left: 30px;
            font-size: 20px;
        }

        form span {
            margin-bottom: 30px;
            display: block;
            width: 100%;
            border-bottom: 2px solid grey;
        }

        form span input {
            border: none;
            margin-left: 10px;
            height: 2rem;

        }

        form span input:focus {
            outline: none;
        }

        form span ion-icon {
            font-size: 20px;
            color: rgb(102, 16, 214);
        }


</style>
<body>

<center>

    <div class="box-p">

        <br>
        <h2 style="color: white">
            <span>
             <img height="70px;" src="{{asset('assets/img/logo.png')}}">
        </span>
            {{__('brand_title')}}
        </h2>

        <br>

        <div class="box">

            <div class="graphic1"></div>
            <div class="graphic2"></div>
            <div class="graphic3"></div>
            <div class="graphic4"></div>


            @if(\Illuminate\Support\Facades\Session::has('voterUserName'))

                <div id="login-div" class="otp-varify">

                    <form action="{{ route('verify-otp') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="username"
                               value="{{\Illuminate\Support\Facades\Session::get('voterUserName')}}">

                        <p style="color: #eba845">OTP expires in <label id="countdowntimer"> </label> Seconds</p>

                        <span>
                            <ion-icon name="person-outline">

                            </ion-icon>
                            <input autofocus id="otp_code" style="width: 100%;font-size: 15px;" name="otp_code"
                                   type="text"
                                   placeholder="{{__("Enter OTP Code")}}">
                        </span>


                        @if(!$errors->isEmpty())
                            <div class="alert alert-red">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $err)
                                        <li>{{ $err }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <button style="margin-left: 9%;background: #4e9b51;color: white;" type="submit">
                            {{__('Confirm OTP')}}
                            <ion-icon name="chevron-forward-outline"></ion-icon>
                        </button>


                    </form>

                    <form action="{{ route('login-voter') }}" method="POST">

                        {{ csrf_field() }}

                        <input type="hidden" name="username"
                               value="{{\Illuminate\Support\Facades\Session::get('voterUserName')}}">

                        <button class="otp_code"
                                type="submit">
                            {{__('Resend OTP')}}
                        </button>
                    </form>


                </div>


            @else

                <div id="login-div" class="login-form">


                    <input type="hidden" id="countdowntimer">
                    <form action="{{ route('login-voter') }}" method="POST">
                        {{ csrf_field() }}


                        <span>
                <ion-icon name="person-outline"></ion-icon>
            <input autofocus id="username" style="width: 100%" name="username" type="text"
                   placeholder="{{__("Mobile No Or Email ID")}}">
        </span>


                        @if(!$errors->isEmpty())
                            <div class="alert alert-red">
                                <ul class="list-unstyled">
                                    @foreach($errors->all() as $err)
                                        <li>{{ $err }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <button onclick="hideButton(this)" style="margin-left: -10%" type="submit">
                            {{__('Enter')}}
                            <ion-icon name="chevron-forward-outline"></ion-icon>
                        </button>


                    </form>


                </div>
            @endif


        </div>
        <div class="row copyright">
            <div class="col">
                <p>পরিকল্পনা ও বাস্তবায়নে</p>
                <a href="#">

                    {{__('client_title')}}
                </a>
            </div>


      


        </div>
    </div>


</center>
</body>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>

    document.addEventListener('contextmenu', function (e) {
        e.preventDefault();
    });


    var timeleft = 180;
    document.getElementById("countdowntimer").textContent
        = timeleft;
    var downloadTimer = setInterval(function () {
        timeleft--;
        document.getElementById("countdowntimer").textContent = timeleft;
        if (timeleft <= 0)
            clearInterval(downloadTimer);
    }, 1000);

    $('button').on('click', function () {
        $(this).fadeOut('fast')
    })

    function hideButton(t) {
        $(t).fadeOut('fast')
    }


    const targetNode = document.body;
    const config = {childList: true, subtree: true, attributes: true};

    const callback = function (mutationsList, observer) {
        for (let mutation of mutationsList) {

            if (mutation.type === 'attributes') {

                console.log(mutation.attributeName)
                if (mutation.attributeName != 'style') {
                    alert('সঠিক নিয়মে চেস্টা করুন')
                    location = '';
                }

            }
        }
    };

    const observer = new MutationObserver(callback);
    observer.observe(targetNode, config);

</script>

