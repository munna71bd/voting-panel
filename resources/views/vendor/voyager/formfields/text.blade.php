@if(isCommonLocalKey(\Illuminate\Support\Str::snake($row->display_name)))
    @php $placeHolder = __('voyager::generic.'. \Illuminate\Support\Str::snake($row->display_name)) @endphp
@elseif(isCommonLocalKey($row->field))
    @php $placeHolder =__('voyager::generic.'. $row->field) @endphp
@elseif($row->type == 'relationship')
    @php $placeHolder = __('bread.'.$dataType->slug .'.'.\Illuminate\Support\Str::snake($row->display_name)) @endphp
@else
    @php $placeHolder = __('bread.'.$dataType->slug .'.'.$row->field)  @endphp
@endif
<input type="text" class="form-control" name="{{ $row->field }}" id="{{ $row->field }}"
       placeholder="{{ $options->placeholder ?? $placeHolder}}"
       {!! isBreadSlugAutoGenerator($options) !!}
       value="{{ old($row->field, $dataTypeContent->{$row->field} ?? $options->default ?? '') }}"
>
