@extends('voyager::master')
<link rel="stylesheet" href="{{asset('backend_resources/css/morris.css')}}">
@section('css')
    <style>
        .card.top {
        }

        .card.top img {
            width: 100%;
        }

        .card.middle img {
            width: 100%;
        }

        .card-body {
            padding: 0 !important;
        }

        .chart {

            width: 100%;
            height: 300px;
        }

        .card-body.top {
            background: url("{{ asset('backend_resources/img/background1.png') }}");
            padding: 35px 44px !important;
            background-size: cover;
        }

        .card-body.cash-in-hand {
            background: url("{{ asset('backend_resources/img/cash_in_hand.png') }}");
            padding: 35px 44px !important;
            background-size: cover;
        }

        .card-body.cash-in-bank {
            background: url("{{ asset('backend_resources/img/cash_in_bank.png') }}");
            padding: 35px 44px !important;
            background-size: cover;
        }

        .card-body.chart {
            height: 350px;
            padding: 10px 10px !important;
            background-size: cover;
        }

        .titleH5 {
            font-size: 15px;
            font-weight: 600;
            color: #7a7a7a;
        }

        .titleH4 {
            font-size: 24px;
            font-weight: 600;
            color: #3f3f3f;
        }

        .highcharts-credits {
            display: none !important;
        }

        .voyager .table > thead > tr > th {
            border-color: #EAEAEA;
            background: #3f5061;
            color: white;
        }

        HTML CSS JSResult Skip Results Iframe
        .container {
            background-size: cover;
            background: rgb(226, 226, 226); /* Old browsers */
            background: -moz-linear-gradient(top, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(226, 226, 226, 1)), color-stop(50%, rgba(219, 219, 219, 1)), color-stop(51%, rgba(209, 209, 209, 1)), color-stop(100%, rgba(254, 254, 254, 1))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=0); /* IE6-9 */
            padding: 20px;
        }

        .led-box {
            height: 30px;
            width: 25%;
            margin: 10px 0;
            float: left;
        }

        .led-box p {
            font-size: 12px;
            text-align: center;
            margin: 1em;
        }

        .led-red {
            margin: -5px auto;
            width: 24px;
            height: 24px;
            background-color: #F00;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 12px;
            -webkit-animation: blinkRed 0.5s infinite;
            -moz-animation: blinkRed 0.5s infinite;
            -ms-animation: blinkRed 0.5s infinite;
            -o-animation: blinkRed 0.5s infinite;
            animation: blinkRed 0.5s infinite;
            cursor: pointer;
        }

        @-webkit-keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        @-moz-keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        @-ms-keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        @-o-keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        @keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        .led-yellow {
            margin: 0 auto;
            width: 24px;
            height: 24px;
            background-color: #FF0;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 12px;
            -webkit-animation: blinkYellow 1s infinite;
            -moz-animation: blinkYellow 1s infinite;
            -ms-animation: blinkYellow 1s infinite;
            -o-animation: blinkYellow 1s infinite;
            animation: blinkYellow 1s infinite;
        }

        @-webkit-keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        @-moz-keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        @-ms-keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        @-o-keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        @keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        .led-green {
            margin: -5px auto;
            width: 24px;
            height: 24px;
            background-color: #ABFF00;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #304701 0 -1px 9px, #89FF00 0 2px 12px;
            cursor: pointer;
        }

        .led-blue {
            margin: 0 auto;
            width: 24px;
            height: 24px;
            background-color: #24E0FF;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #006 0 -1px 9px, #3F8CFF 0 2px 14px;
        }

        .bold {
            font-weight: bold;
            font-size: 14px;
            cursor: pointer;
        }

        .led-green-t {
            color: #59f35f;
        }

        .led-red-t {
            color: #f44336;
        }

        .Table td, .Table th {
            border: 1px solid #d4d2d0;
            padding: .5em 1em;
            text-align: center;
        }


    </style>
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        @include('voyager::dimmers')
        <br>
        <div class="row" style="height: 20px;">
            <div class="col-md-12">
                <div class="row">
                </div>

            </div>
        </div>
    </div>
    <div id="container"></div>

    <div class="modal fade" id="modal-default" tabindex="-1" role="dialog"
         aria-labelledby="modal-default"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal- modal-dialog-centered modal-lg"
             role="document">
            <div class="modal-content">
                <div style="background: #0a3a51;color:white;"
                     class="modal-header">
                    <h4 class="modal-title" id="modal-title-default">
                        Modal Title
                    </h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>





    <script src="{{asset('backend_resources/js/jquery-2.2.4.min.js')}}"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="{{asset('backend_resources/js/raphael-min.js')}}"></script>


    <script src="{{asset('backend_resources/js/highcharts.js')}}"></script>

    <script>
        var vasa = '{{app()->getLocale()}}';

        function en2bn(locale, n) {
            if (locale == "en") {
                return n;
            }

            numbersArray = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'];
            numbersArray['-'] = '-';
            numbersArray['/'] = '/';
            numbersArray['='] = '=';
            numbersArray[''] = '০';
            numbersArray['.'] = '.';
            var output = '';
            n = n.toString();
            for (var i = 0; i < n.length; i++) {
                output += numbersArray[n[i]];
            }
            return output;
        }


        $.ajax({
            url: '{{route("dashboard.data")}}',
            type: 'GET',
            data: {},
            success: function (data) {
                if (data) {
                }
            })


    </script>

@stop


