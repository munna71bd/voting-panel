<link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
<link rel="apple-touch-icon" type="image/png" href="{{asset('backend_resources/img/logo.png')}}"/>
<meta name="apple-mobile-web-app-title" content="CodePen">

<link rel="shortcut icon" type="image/x-icon" href="{{asset('backend_resources/img/logo.png')}}"/>

<link rel="mask-icon" type="image/x-icon" href="{{asset('backend_resources/img/logo.png')}}" color="#111"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="admin login">
<title> {{ __('brand_title') }}</title>
<style>
    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }

    body {
        width: 100%;
        min-height: 100vh;
        background: linear-gradient(-45deg, rgb(127, 116, 208), rgb(193, 191, 250));


    }

    .Alert {
        padding: 10px;
    }

    @media (min-width: 1024px)
        .Alert {
            padding: 10px;
        }

        .box {
            width: 70%;
            background: linear-gradient(-45deg, rgb(190, 190, 240), rgb(123, 120, 201));
            position: relative;
            overflow: hidden;
            border-radius: 10px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }


        .box button {
            border: 2px solid grey;
            background: #fff;
            width: 230px;
            color: rgb(109, 99, 190);
            font-weight: bolder;
            font-size: 1.2rem;
            border-radius: 30px;
            padding: 6px;
            box-shadow: -2px 5px 10px 2px rgb(180, 180, 245);
            cursor: pointer;
        }


        a {
            color: #fff;
            text-decoration: none;
        }


        .market-btn .market-button-title {
            display: block;
            color: #222;
            font-size: 1.125rem;
        }

        .market-btn .market-button-subtitle {
            display: block;
            margin-bottom: -0.25rem;
            color: #888;
            font-size: 0.75rem;
        }


        input[type="radio"] {
            -ms-transform: scale(2); /* IE 9 */
            -webkit-transform: scale(2); /* Chrome, Safari, Opera */
            transform: scale(2);
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
            color: black !important;
        }

        .Table td, .Table th {
            border: 1px solid #d4d2d0;
            padding: 0.5em 1em;
            text-align: center;
        }

</style>

<center>

    <div class="box-p">

        <br>
        <h2 style="color: white">
            <span>
             <img height="70px;" src="{{asset('assets/img/logo.png')}}">
        </span>
            {{__('brand_title')}}
        </h2>

        <br>

        <div class="box">

            <h2>{{__('Select Secretary')}}</h2>
            <hr>


            <div id="login-div">
                <div style="background: #4caf50;" class="alert alert-green" id="msg-success-div">
                    <ul class="list-unstyled">
                        <li id="msg-success">{{  Session::has('special_message')  ? Session::get('special_message') :$voter_message}}</li>
                    </ul>
                </div>


            </div>


        </div>
        <div class="row copyright">
            <div class="col">
                <p>পরিকল্পনা ও বাস্তবায়নে</p>
                <a href="#" >

                    {{__('client_title')}}
                </a>

            </div>




        </div>
    </div>


</center>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>
    document.addEventListener('contextmenu', function(e) {
        e.preventDefault();
    });

</script>

