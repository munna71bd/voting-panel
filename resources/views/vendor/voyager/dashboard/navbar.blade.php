@php $name=app('VoyagerAuth')->user()->name; if(app()->getLocale() === 'bn') $name=app('VoyagerAuth')->user()->name_bn ;@endphp
<nav class="navbar navbar-default navbar-fixed-top navbar-top">
    <div class="container-fluid">

        <div class="navbar-header left" style="width: 280px;">
            <div class="logo-icon-container">

            </div>

            <h4 style="color: white">
                <img height="60px;" src="{{asset('backend_resources/img/logo.png')}}">
                {{__('brand_title_short')}}
            </h4>


        </div><!-- .navbar-header -->
        <div class="navbar-header justify-content-md-center">

            <button class="hamburger breadcrumb back-btn badge-pill">
                <i class="fa fa-bars"></i>

            </button>

        </div>


        <ul class="nav justify-content-md-center top-right navbar-nav @if (config('voyager.multilingual.rtl')) navbar-left @else navbar-right @endif">
            <li style="display: none" class="nav-item">

                <div class="btn-group language">
                    <button onclick="document.getElementById('change_language').submit()"
                            class="btn btn-sm btn-{{app()->getLocale() === 'en' ? 'primary': 'default'}} padding5 font-size-12 border-rad-left14">
                        English
                    </button>
                    <button onclick="document.getElementById('change_language').submit()"
                            class="btn btn-sm btn-{{app()->getLocale() === 'bn' ? 'primary': 'default'}} padding5 font-size-12 border-rad-right14">
                        বাংলা
                    </button>

                </div>
                <form method="post" action="{{route('change-language', app()->getLocale() === 'bn' ? 'en': 'bn')}}"
                      id="change_language">
                    @csrf
                </form>
            </li>
            <li style="visibility: hidden" class="dropdown dropdown-notification nav-item">
                <a class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="fa fa-envelope"></i><span
                        class="badge badge-pill badge-warning badge-up">3</span></a>

            </li>
            <li style="visibility: hidden" class="dropdown dropdown-notification nav-item"><a
                    class="nav-link nav-link-label" href="#" data-toggle="dropdown"><i class="fa fa-bell"></i><span
                        class="badge badge-pill badge-danger badge-up">5</span></a>

            </li>


            <li class="dropdown dropdown-user nav-item">
                <a href="#" class="dropdown-toggle"
                   type="" id="dropdownMenu1" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="false">
                    <img src="{{ $user_avatar }}" class="profile-img"> {{ $name }}<span
                        class="caret"></span>
                </a>
                <ul class="dropdown-menu popup" aria-labelledby="dropdownMenu1">
                    <li class="dropdown-item profile-img">
                        <img src="{{ $user_avatar }}" class="profile-img">
                        <div class="profile-body">
                            <h5>{{ $name }}</h5>
                            <h6>{{__('role')}}: {{App\Models\Role::find(auth()->user()->role_id)->display_name }}</h6>

                        </div>
                    </li>
                    </li>
                    <li class="dropdown-item divider"></li>
                    <?php $nav_items = config('voyager.dashboard.navbar_items'); ?>
                    @if(is_array($nav_items) && !empty($nav_items))
                        @foreach($nav_items as $name => $item)
                            @if($item['icon_class']!=='voyager-home')
                                <li {!! isset($item['classes']) && !empty($item['classes']) ? 'class="'.$item['classes'].'"' : '' !!}>
                                    @if(isset($item['route']) && $item['route'] == 'voyager.logout')
                                        <form action="{{ route('voyager.logout') }}" method="POST">
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-block">
                                                @if(isset($item['icon_class']) && !empty($item['icon_class']))
                                                    <i class="{!! $item['icon_class'] !!}"></i>
                                                @endif
                                                {{__($name)}}
                                            </button>
                                        </form>
                                    @else
                                        <a href="{{ isset($item['route']) && Route::has($item['route']) ? route($item['route']) : (isset($item['route']) ? $item['route'] : '#') }}" {!! isset($item['target_blank']) && $item['target_blank'] ? 'target="_blank"' : '' !!}>
                                            @if(isset($item['icon_class']) && !empty($item['icon_class']))
                                                <i class="{!! $item['icon_class'] !!}"></i>
                                            @endif
                                            {{__($name)}}
                                        </a>
                                    @endif
                                </li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </li>
        </ul>
    </div>
</nav>
