@extends('voyager::master')

@section('page_title', __('voyager::generic.viewing').' '.__($dataType->display_name_plural))

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class="{{ $dataType->icon }}"></i> {{ __($dataType->display_name_plural) }}
        </h1>
        @can('add', app($dataType->model_name))
            <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
                <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
            </a>
        @endcan

        @can('show_trashed', app($dataType->model_name))
            @if($usesSoftDeletes)
                <input type="checkbox" @if ($showSoftDeleted) checked @endif id="show_soft_deletes" data-toggle="toggle"
                       data-on="{{ __('voyager::bread.soft_deletes_off') }}"
                       data-off="{{ __('voyager::bread.soft_deletes_on') }}">
            @endif
        @endcan
        @foreach(Voyager::actions() as $action)
            @if (method_exists($action, 'massAction'))
                @include('voyager::bread.partials.actions', ['action' => $action, 'data' => null])
            @endif
        @endforeach
    </div>
@stop
@section('css')
    <style>
        .employee-action-button {
            padding: 5px 8px !important;
            margin-left: 5px !important;
            float: left !important;
        }
        .dropdown-menu {
            min-width: 400px !important;
        }
        .table-responsive .filter_employee {
            float: left;
        }
        .table-responsive .employee_letter {
            float: left;
        }
        .table-responsive .col-sm-6 {
            margin-bottom: 0px !important;
        }
    </style>
@stop
@section('content')
    <div class="page-content browse container-fluid">
        @include('voyager::alerts')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-bordered">
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table id="dataTable" class="table table-hover">
                                <thead>
                                <tr>
                                    @foreach($selectFields as $tableColumn)
                                        <th>
                                        @if(isCommonLocalKey(\Illuminate\Support\Str::snake($tableColumn['display_name'])))
                                            {{__('voyager::generic.'. \Illuminate\Support\Str::snake($tableColumn['display_name']))}}
                                        @elseif(isCommonLocalKey($tableColumn['field']))
                                            {{__('voyager::generic.'. $tableColumn['field'])}}
                                        @elseif($tableColumn['is_relationship'])
                                            {{ __('bread.'.$dataType->slug .'.'.\Illuminate\Support\Str::snake($tableColumn['display_name'])) }}
                                        @else
                                            {{ __('bread.'.$dataType->slug .'.'.$tableColumn['field']) }}
                                        @endif
                                        </th>
                                    @endforeach
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"><i
                            class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ __(ucfirst($dataType->display_name_plural)) }}
                        ?</h4>

                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="{{ __('voyager::generic.close') }}"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="delete_model_body"></div>
                <div class="modal-footer">
                    <form action="#" id="delete_form" method="POST">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right"
                            data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('css')
@stop

@section('javascript')
    <script>
        $(document).ready(function () {

            var dataTableParams = {!! json_encode(
                    array_merge([
                        "language" => __('voyager::datatable'),
                        "processing" => true,
                        "serverSide" => true,
                        "ordering" => true,
                        "searching" => true,
                        "stateSave"=> false,
                        "ajax" => [
                            "method" => "POST",
                            "url" => route('voyager.'.$dataType->slug.'.datatable'),
                        ],
                        "columns" => $dataTableColumns
                    ],
                    config('voyager.dashboard.data_tables', []))
                , true) !!};

            let table = $('#dataTable').DataTable(dataTableParams);

            $(document).on('focus', '.dataTables_filter input', function() {
                $(this).unbind().bind('keyup', function(e) {
                    if(e.keyCode === 13) {
                        table.search( this.value ).draw();
                    }
                });
            });
        });

        $(document, 'td').on('click', '.delete', function (e) {
            $('#delete_form')[0].action = '{{ route('voyager.'.$dataType->slug.'.destroy', ['id' => '__id']) }}'.replace('__id', $(this).data('id'));
            $('#delete_modal').modal('show');
        });

    </script>
@stop
