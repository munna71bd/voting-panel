@extends('voyager::master')
<link rel="stylesheet" href="{{asset('backend_resources/css/morris.css')}}">
@section('css')
    <style>
        .card.top {
        }

        .card.top img {
            width: 100%;
        }

        .card.middle img {
            width: 100%;
        }

        .card-body {
            padding: 0 !important;
        }

        .chart {

            width: 100%;
            height: 300px;
        }

        .card-body.top {
            background: url("{{ asset('backend_resources/img/background1.png') }}");
            padding: 35px 44px !important;
            background-size: cover;
        }

        .card-body.cash-in-hand {
            background: url("{{ asset('backend_resources/img/cash_in_hand.png') }}");
            padding: 35px 44px !important;
            background-size: cover;
        }

        .card-body.cash-in-bank {
            background: url("{{ asset('backend_resources/img/cash_in_bank.png') }}");
            padding: 35px 44px !important;
            background-size: cover;
        }

        .card-body.chart {
            height: 350px;
            padding: 10px 10px !important;
            background-size: cover;
        }

        .titleH5 {
            font-size: 15px;
            font-weight: 600;
            color: #7a7a7a;
        }

        .titleH4 {
            font-size: 24px;
            font-weight: 600;
            color: #3f3f3f;
        }

        .highcharts-credits {
            display: none !important;
        }

        .voyager .table > thead > tr > th {
            border-color: #EAEAEA;
            background: #3f5061;
            color: white;
        }

        HTML CSS JSResult Skip Results Iframe
        .container {
            background-size: cover;
            background: rgb(226, 226, 226); /* Old browsers */
            background: -moz-linear-gradient(top, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(226, 226, 226, 1)), color-stop(50%, rgba(219, 219, 219, 1)), color-stop(51%, rgba(209, 209, 209, 1)), color-stop(100%, rgba(254, 254, 254, 1))); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* IE10+ */
            background: linear-gradient(to bottom, rgba(226, 226, 226, 1) 0%, rgba(219, 219, 219, 1) 50%, rgba(209, 209, 209, 1) 51%, rgba(254, 254, 254, 1) 100%); /* W3C */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=0); /* IE6-9 */
            padding: 20px;
        }

        .led-box {
            height: 30px;
            width: 25%;
            margin: 10px 0;
            float: left;
        }

        .led-box p {
            font-size: 12px;
            text-align: center;
            margin: 1em;
        }

        .led-red {
            margin: -5px auto;
            width: 24px;
            height: 24px;
            background-color: #F00;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 12px;
            -webkit-animation: blinkRed 0.5s infinite;
            -moz-animation: blinkRed 0.5s infinite;
            -ms-animation: blinkRed 0.5s infinite;
            -o-animation: blinkRed 0.5s infinite;
            animation: blinkRed 0.5s infinite;
            cursor: pointer;
        }

        @-webkit-keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        @-moz-keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        @-ms-keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        @-o-keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        @keyframes blinkRed {
            from {
                background-color: #F00;
            }
            50% {
                background-color: #A00;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #441313 0 -1px 9px, rgba(255, 0, 0, 0.5) 0 2px 0;
            }
            to {
                background-color: #F00;
            }
        }

        .led-yellow {
            margin: 0 auto;
            width: 24px;
            height: 24px;
            background-color: #FF0;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 12px;
            -webkit-animation: blinkYellow 1s infinite;
            -moz-animation: blinkYellow 1s infinite;
            -ms-animation: blinkYellow 1s infinite;
            -o-animation: blinkYellow 1s infinite;
            animation: blinkYellow 1s infinite;
        }

        @-webkit-keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        @-moz-keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        @-ms-keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        @-o-keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        @keyframes blinkYellow {
            from {
                background-color: #FF0;
            }
            50% {
                background-color: #AA0;
                box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #808002 0 -1px 9px, #FF0 0 2px 0;
            }
            to {
                background-color: #FF0;
            }
        }

        .led-green {
            margin: -5px auto;
            width: 24px;
            height: 24px;
            background-color: #ABFF00;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #304701 0 -1px 9px, #89FF00 0 2px 12px;
            cursor: pointer;
        }

        .led-blue {
            margin: 0 auto;
            width: 24px;
            height: 24px;
            background-color: #24E0FF;
            border-radius: 50%;
            box-shadow: rgba(0, 0, 0, 0.2) 0 -1px 7px 1px, inset #006 0 -1px 9px, #3F8CFF 0 2px 14px;
        }

        .bold {
            font-weight: bold;
            font-size: 14px;
            cursor: pointer;
        }

        .led-green-t {
            color: #59f35f;
        }

        .led-red-t {
            color: #f44336;
        }

        .Table td, .Table th {
            border: 1px solid #d4d2d0;
            padding: .5em 1em;
            text-align: center;
        }


    </style>
@stop

@section('content')
    <div class="page-content container-fluid">
        @include('voyager::alerts')
        @include('voyager::dimmers')
        <br>
        <div class="row" style="height: 20px;">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="titleH4">{{__('voyager::generic.reporting_month')}}: <span
                                id="reporting_month"></span>,{{__('voyager::generic.reporting_year')}} <span
                                id="reporting_year"></span>
                        </h5>
                    </div>


                    @if(auth()->user()->role_id != 5)

                        <div class="col-md-6">
                            <div class="row">

                                <div class="col-md-3">
                                    @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                                        <label>{{__("Scg Data")}}:
                                        </label>
                                    @else
                                        <label>{{__("CDC Data")}}:
                                        </label>
                                    @endif
                                </div>

                                <div class="col-md-4">
                                    <label
                                        @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                                        onclick="loadScgList('updated')"
                                        @else
                                        onclick="loadCdcList('updated')"
                                        @endif
                                        data-toggle="modal"
                                        data-target="#modal-default"
                                        class="led-green"></label>
                                    <label>{{__("Updated")}}:
                                        <b
                                            @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                                            onclick="loadScgList('updated')"
                                            @else
                                            onclick="loadCdcList('updated')"
                                            @endif
                                            data-toggle="modal"
                                            data-target="#modal-default"
                                            class="bold" id="cdc_updated">

                                        </b>
                                    </label>
                                    </label>
                                </div>

                                <div class="col-md-4">
                                    <label
                                        @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                                        onclick="loadScgList('updated_not')"
                                        @else
                                        onclick="loadCdcList('updated_not')"
                                        @endif
                                        data-toggle="modal"
                                        data-target="#modal-default"
                                        class="led-red"></label>
                                    <label>{{__("Not Updated")}}:
                                        <b
                                            @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                                            onclick="loadScgList('updated_not')"
                                            @else
                                            onclick="loadCdcList('updated_not')"
                                            @endif
                                            data-toggle="modal"
                                            data-target="#modal-default"
                                            class="bold" id="cdc_updated_not">

                                        </b>
                                    </label>
                                    </label>
                                </div>


                            </div>
                        </div>
                    @endif


                </div>
            </div>
        </div>

        <hr>
        <div class="row">
            <div class="col-md-12">
                @if(Auth::user()->role->id!=4)
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card top">
                                <div style="height: 130px;" class="card-body top">
                                    <h5 class="titleH5">{{ __('voyager::generic.total_cluster') }}</h5>
                                    <h4 id="total_cluster" class="titleH4"></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card top">
                                <div style="height: 130px;" class="card-body top">
                                    <h5 class="titleH5">{{ __('voyager::generic.total_cdc') }}</h5>
                                    <h4 id="total_cdc" class="titleH4"></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card top">
                                <div style="height: 130px;" class="card-body top">
                                    <h5 class="titleH5">{{ __('Total SCG') }}</h5>
                                    <h4 id="total_scg" class="titleH4"></h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card top">
                                <div style="height: 130px;" class="card-body top">
                                    <h5 class="titleH5">{{ __('Total Approved CDC') }}</h5>
                                    <h4 id="total_approved_scg" class="titleH4"></h4>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            @endif


            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="card middle">
                            <div class="card-body  chart">
                                <h5 class="titleH5">{{ __('Total Scg Member') }}</h5>
                                <h4 id="total_scg_member" class="titleH4">0</h4>
                                <div class="chart" id="pie-chart-total-member"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card middle">
                            <div class="card-body  chart">
                                <h5 class="titleH5">{{ __('Savings Status') }}</h5>
                                <div class="chart" id="bar-chart-total-saving"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card middle">
                            <div class="card-body  chart">
                                <h5 class="titleH5">{{ __('Approved CDC List') }}</h5>
                                <h4 id="total_pg_member" class="titleH4">0</h4>
                                <div class="chart" id="approved_scg_list">
                                    <div style="overflow-y: scroll;height: 300px;">
                                        <table class="table">
                                            <thead>
                                            <th>{{__("SN")}}</th>
                                            <th>{{__("SCG")}}</th>
                                            </thead>
                                            <tbody id="approved_scg_list_data">

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>


            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card middle">
                            <div class="card-body  chart">
                                <h5 class="titleH5">{{ __('Loan Status') }}</h5>
                                <div class="chart" id="bar-chart-total-pg-loan"></div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card middle">
                            <div class="card-body  chart">
                                <h5 class="titleH5">{{ __('Total Fund Status') }}</h5>

                                <div style="height: 350px;" class="chart" id="pie-chart-total-fund"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card middle">
                            <div class="card-body  chart">
                                <h5 class="titleH5">{{ __('voyager::generic.cdc_performance') }}</h5>
                                <div class="chart" id="bar-chart-cdc-performance"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card middle">
                            <div class="card-body top">
                                <h5 class="titleH5">{{ __('Cash Status') }}</h5>

                                <div class="card top">
                                    <div class="card-body cash-in-hand">
                                        <h5 class="titleH5">{{ __('Cash in Hand') }}</h5>
                                        <h4 id="total_cash_in_hand" class="titleH4">0</h4>
                                    </div>
                                </div>


                                <div class="card top">
                                    <div class="card-body cash-in-bank">
                                        <h5 class="titleH5">{{ __('Cash at Bank') }}</h5>
                                        <h4 id="total_cash_in_bank" class="titleH4">0</h4>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div style="display: none;" class="col-md-12">
            <div class="row">

                <div class="col-md-12">
                    <div class="card middle">
                        <div class="card-body  chart">
                            <h5 class="titleH5">{{ __('voyager::generic.overdue_loan') }}</h5>
                            <div class="chart" id="bar-chart-loan-outstanding"></div>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>

    <div id="container"></div>

    <div class="modal fade" id="modal-default" tabindex="-1" role="dialog"
         aria-labelledby="modal-default"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal- modal-dialog-centered modal-lg"
             role="document">
            <div class="modal-content">
                <div style="background: #0a3a51;color:white;"
                     class="modal-header">
                    <h4 class="modal-title" id="modal-title-default">
                        @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                            {{__("Scg List")}}
                        @else
                            {{__("CDC List")}}
                        @endif
                        (
                        <span class="led-green-t" id="modal_title"></span>
                        )
                    </h4>
                    <button type="button" class="close" data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div style="overflow-y: scroll;height: 300px;">
                        <table class="table">
                            <thead>
                            <th>{{__("SN")}}</th>
                            @if(auth()->user()->role_id == 3 || auth()->user()->role_id == 4)
                                <th>{{__("SCG")}}</th>
                            @else
                                <th>{{__("CDC")}}</th>
                            @endif
                            <th>{{__("Last Transaction Month & Year")}}</th>
                            </thead>
                            <tbody id="cdc_list_data">

                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>





    <script src="{{asset('backend_resources/js/jquery-2.2.4.min.js')}}"
            integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="{{asset('backend_resources/js/raphael-min.js')}}"></script>


    <script src="{{asset('backend_resources/js/highcharts.js')}}"></script>

    <script>
        var vasa = '{{app()->getLocale()}}';

        function en2bn(locale, n) {
            if (locale == "en") {
                return n;
            }

            numbersArray = ['০', '১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯'];
            numbersArray['-'] = '-';
            numbersArray['/'] = '/';
            numbersArray['='] = '=';
            numbersArray[''] = '০';
            numbersArray['.'] = '.';
            var output = '';
            n = n.toString();
            for (var i = 0; i < n.length; i++) {
                output += numbersArray[n[i]];
            }
            return output;
        }


        $.ajax({
            url: '{{route("dashboard.data")}}',
            type: 'GET',
            data: {},
            success: function (data) {
                if (data) {
                    $('#total_cluster').html(en2bn(vasa, data.total_cluster));
                    $('#total_cdc').html(en2bn(vasa, data.total_cdc));
                    $('#total_scg').html(en2bn(vasa, data.total_scg));
                    $('#total_pg').html('');
                    $('#total_scg_member').html(en2bn(vasa, data.total_scg_member));
                    $('#total_pg_member').html('');

                    $('#reporting_month').html(data.last_reporting_month);
                    $('#reporting_year').html(data.last_reporting_year)

                    $('#total_approved_scg').html(en2bn(vasa, data.total_approved_scg));

                    $('#cdc_updated').html(en2bn(vasa, data.cdc_updated));
                    $('#cdc_updated_not').html(en2bn(vasa, data.cdc_updated_not));

                    var tbl_data = "";
                    $.each(data.approved_scg_list, function (a, b) {
                        tbl_data += "<tr>";
                        tbl_data += "<td>" + (en2bn(vasa, a + 1)) + "</td>";
                        tbl_data += "<td>" + b.name + "(CDC-" + b.cdc_name + ")</td>";
                        tbl_data += "</tr>";
                    });
                    $("#approved_scg_list_data").html(tbl_data);


                    // Make monochrome colors
                    var pieColors = (function () {
                        var colors = [],
                            base = Highcharts.getOptions().colors[0],
                            i;

                        for (i = 0; i < 4; i += 1) {
                            colors.push(Highcharts.color(base).brighten((i - 3) / 7).get());
                        }
                        return colors;
                    }());


                    Highcharts.chart('pie-chart-total-member', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            formatter: function () {
                                return '<b>' + this.series.name + ' ' + this.point.name + ': ' + this.y.toLocaleString(vasa) + '</b>';

                            },
                        },
                        accessibility: {
                            point: {
                                valueSuffix: ''
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                colors: pieColors,
                                dataLabels: {
                                    enabled: true,
                                    //format: '<b style="font-size: 18px;">{point.name}:{y}</b>',
                                    formatter: function () {
                                        return '<b style="color:white;font-size: 16px;">' + this.point.name + ': ' + this.y.toLocaleString(vasa) + '</b>';

                                    },
                                    distance: -50,
                                    filter: {
                                        property: 'percentage',
                                        operator: '>',
                                        value: 4
                                    }
                                }
                            }
                        },
                        series: [{
                            name: '{{__("Total")}}',
                            data: [
                                {name: '{{__("Male")}}', y: data.total_scg_member_male},
                                {name: '{{__("Female")}}', y: data.total_scg_member_female},
                                {name: '{{__("Transgender")}}', y: data.total_scg_member_transgender},
                                {name: '{{__("PWD")}}', y: data.total_scg_member_pwd},

                            ]
                        }]
                    });


                    // Savings bar chart
                    Highcharts.chart('bar-chart-total-saving', {
                        chart: {
                            type: 'column'
                        },
                        colors: ['#323b6b', '#ff9800', '#4caf50'],
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        accessibility: {
                            announceNewData: {
                                enabled: true
                            }
                        },
                        xAxis: {
                            type: 'category',
                            labels: {
                                style:
                                    {
                                        fontSize: '15px'
                                    }
                            }
                        },
                        yAxis: {
                            title: {
                                text: ''
                            },
                            labels: {
                                style:
                                    {
                                        fontSize: '15px'
                                    },
                                formatter: function () {
                                    return this.value.toLocaleString(vasa);
                                },
                            }

                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: true,
                                    //format: '{point.y:.2f}',
                                    formatter: function () {
                                        return this.y.toLocaleString(vasa)
                                    },
                                    rotation: -90,
                                    align: 'right',
                                    style:
                                        {
                                            fontSize: '15px',
                                            color: 'white',
                                        }

                                }
                            }
                        },

                        tooltip: {
                            formatter: function () {
                                return '<span style="color:' + this.color + '">' + this.series.name + '</span>: <b>' + this.y.toLocaleString(vasa) + '</b><br/>';

                            },
                        },

                        series: [
                            {
                                name: "{{__('Total')}}",
                                colorByPoint: true,
                                data: [
                                    {
                                        name: '{{__("Saving Deposit")}}',
                                        y: data.total_saving_amount,

                                    },
                                    {
                                        name: '{{__("Saving Withdraw")}}',
                                        y: data.total_saving_withdrawn,
                                    },
                                    {
                                        name: '{{__("Saving Balance")}}',
                                        y: data.total_saving_balance,

                                    },

                                ]
                            }
                        ],


                    });


                    //Loan bar chart
                    Highcharts.chart('bar-chart-total-pg-loan', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        accessibility: {
                            announceNewData: {
                                enabled: true
                            }
                        },
                        xAxis: {
                            type: 'category',
                            labels: {
                                style: {
                                    fontSize: '15px',

                                }
                            }
                        },
                        yAxis: {
                            title: {
                                text: ''
                            },
                            labels: {
                                style: {
                                    fontSize: '15px',

                                },
                                formatter: function () {
                                    return this.value.toLocaleString(vasa);
                                },
                            },


                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        return this.y.toLocaleString(vasa);
                                    },
                                    rotation: -90,
                                    align: 'right',
                                    style: {
                                        fontSize: '15px',
                                        color: 'white',

                                    }
                                },
                                colors: ['#00a65a', '#412770', '#10635b', 'gray', '#3a6173', '#380a40', '#3a3a4f'],
                            }
                        },

                        tooltip: {

                            formatter: function () {
                                return '<span style="color:{point.color}">' + this.series.name + '</span>: <b>' + this.y.toLocaleString(vasa) + '</b><br/>';

                            },
                        },

                        series: [
                            {
                                name: "{{__('Total')}}",
                                colorByPoint: true,
                                data: [
                                    {
                                        name: '{{__("Urgent Loan")}}',
                                        y: data.total_urgent_loan,

                                    },
                                    {
                                        name: '{{__("Social Loan")}}',
                                        y: data.total_social_loan,
                                    },
                                    {
                                        name: '{{__("IGA Loan")}}',
                                        y: data.total_iga_loan,

                                    },
                                    {
                                        name: '{{__("House Loan")}}',
                                        y: data.total_house_loan,

                                    },
                                    {
                                        name: '{{__("Total Loan Disbursement")}}',
                                        y: data.total_disbursed_loan,

                                    },
                                    {
                                        name: '{{__("Loan Recovery")}}',
                                        y: data.total_collected_loan,

                                    },
                                    {
                                        name: '{{__("Loan Outstanding")}}',
                                        y: (data.total_disbursed_loan - data.total_collected_loan),

                                    },


                                ]
                            }
                        ],


                    });


                    $('#total_cash_in_hand').html(en2bn(vasa, data.total_cash_in_hand));
                    $('#total_cash_in_bank').html(en2bn(vasa, data.total_cash_in_bank));


                    if (data.total_balance_sheet.group_saving < 0) {
                        data.total_balance_sheet.group_saving = 1;
                    }

                    //Cdc Fund Status Pie Chart

                    Highcharts.chart('pie-chart-total-fund', {
                        chart: {
                            plotBackgroundColor: null,
                            plotBorderWidth: null,
                            plotShadow: false,
                            type: 'pie'
                        },
                        title: {
                            text: ''
                        },
                        tooltip: {
                            formatter: function () {
                                return '<span style="color:{point.color}">' + this.series.name + '</span>: <b>' + this.y.toLocaleString(vasa) + '</b><br/>';

                            },
                        },
                        accessibility: {
                            point: {
                                valueSuffix: ''
                            }
                        },
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                colors: ["#558197", "black", "#0c2734", "chartreuse", "teal", "orange", "#375065", "", "green", "blue"],
                                dataLabels: {
                                    enabled: true,
                                    formatter: function () {
                                        return '<b style="white;font-size: 14px;">' + this.point.name + ' </b> <br><span style="font-size: 14px;">' + this.y.toLocaleString(vasa) + '</span>';

                                    },
                                    distance: -50,
                                    filter: {
                                        property: 'percentage',
                                        operator: '>',
                                        value: 4
                                    }
                                }
                            }
                        },
                        series: [{
                            name: '{{__("Total")}}',
                            data: [
                                {
                                    name: "{{__('voyager::generic.group_saving')}}",
                                    y: data.total_saving_balance
                                },
                                {
                                    label: "{{__('voyager::generic.cf_fund')}}",
                                    y: data.total_balance_sheet.cf_fund
                                },
                                {
                                    name: "{{__('voyager::generic.cdc_fund')}}",
                                    y: data.total_balance_sheet.cdc_fund
                                },
                                {
                                    name: "{{__('voyager::generic.cluster_fund')}}",
                                    y: data.total_balance_sheet.cluster_fund
                                },
                                {
                                    name: "{{__('voyager::generic.others_fund')}}",
                                    y: data.total_balance_sheet.others_fund
                                },
                                {
                                    name: "{{__('voyager::generic.total_cash_in_hand')}}",
                                    y: data.total_balance_sheet.cash_in_hand
                                },
                                {
                                    name: "{{__('Cash at Bank')}}",
                                    y: data.total_balance_sheet.cash_in_bank
                                },
                                {
                                    name: "{{__('voyager::generic.loan_outstanding')}}",
                                    y: data.total_balance_sheet.loan_outstanding
                                },
                                {
                                    name: "{{__('voyager::generic.other_invest')}}",
                                    y: data.total_balance_sheet.other_invest
                                },
                                {
                                    name: "{{__('voyager::generic.receivables')}}",
                                    y: data.total_balance_sheet.cdc_fund
                                },


                            ]
                        }]
                    });


                    // CDC Performance bar chart
                    Highcharts.chart('bar-chart-cdc-performance', {
                        chart: {
                            type: 'column'
                        },
                        title: {
                            text: ''
                        },
                        subtitle: {
                            text: ''
                        },
                        accessibility: {
                            announceNewData: {
                                enabled: true
                            }
                        },
                        xAxis: {
                            type: 'category',
                            labels: {
                                style: {
                                    fontSize: '15px',

                                }
                            }

                        },
                        yAxis: {
                            title: {
                                text: ''
                            },
                            labels: {
                                style: {
                                    fontSize: '15px',

                                }
                            }

                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            series: {
                                borderWidth: 0,
                                dataLabels: {
                                    enabled: true,
                                    format: '{point.y}',
                                    rotation: -90,
                                },
                                colors: ['darkseagreen', 'rebeccapurple', 'slategrey']

                            },
                        },

                        tooltip: {
                            formatter: function () {
                                return '<span style="color:' + this.color + '">' + this.series.name + '</span>: <b>' + this.y.toLocaleString(vasa) + '</b><br/>';

                            },
                        },

                        series: [
                            {
                                name: "{{__('Total')}}",
                                colorByPoint: true,
                                data: [
                                    {name: '{{__("Fully Effective")}}', y: null},
                                    {name: '{{__("Moderately Effective")}}', y: null},
                                    {name: '{{__("Weak")}}', y: null},

                                ]
                            }
                        ],


                    });


                }
            },
            error: function (error) {
                console.log(error)
            }
        });

        function loadCdcList(v) {
            $('#cdc_list_data').empty();
            var key = v;
            $.ajax({
                url: '{{route("dashboard.cdc_list")}}',
                type: 'GET',
                data: {key: key},
                success: function (data) {

                    var tbl_data = "";
                    $.each(data.cdc_list, function (a, b) {
                        tbl_data += "<tr>";
                        tbl_data += "<td>" + (en2bn(vasa, a + 1)) + "</td>";
                        tbl_data += "<td>" + b.name + "</td>";
                        tbl_data += "<td>" + b.lats_updated_month_year + "</td>";
                        tbl_data += "</tr>";
                    });
                    $("#cdc_list_data").html(tbl_data);
                },
                error: function (error) {

                    console.log(error)
                }
            });
            if (v == 'updated') {
                v = "{{__('Updated')}}";
                $('#modal_title').removeClass('led-red-t');
                $('#modal_title').addClass('led-green-t');
            } else {
                v = "{{__('Not Updated')}}";
                $('#modal_title').removeClass('led-green-t');
                $('#modal_title').addClass('led-red-t');
            }
            $('#modal_title').html(v);
        }

        function loadScgList(v) {
            $('#cdc_list_data').empty();
            var key = v;
            $.ajax({
                url: '{{route("dashboard.scg_list")}}',
                type: 'GET',
                data: {key: key},
                success: function (data) {

                    var tbl_data = "";
                    $.each(data.scg_list, function (a, b) {
                        tbl_data += "<tr>";
                        tbl_data += "<td>" + (en2bn(vasa, a + 1)) + "</td>";
                        tbl_data += "<td>" + b.name + "(CDC-" + b.cdc_name + ")</td>";
                        tbl_data += "<td>" + b.lats_updated_month_year + "</td>";
                        tbl_data += "</tr>";
                    });
                    $("#cdc_list_data").html(tbl_data);
                },
                error: function (error) {

                    console.log(error)
                }
            });
            if (v == 'updated') {
                v = "{{__('Updated')}}";
                $('#modal_title').removeClass('led-red-t');
                $('#modal_title').addClass('led-green-t');
            } else {
                v = "{{__('Not Updated')}}";
                $('#modal_title').removeClass('led-green-t');
                $('#modal_title').addClass('led-red-t');
            }
            $('#modal_title').html(v);
        }


    </script>

@stop


