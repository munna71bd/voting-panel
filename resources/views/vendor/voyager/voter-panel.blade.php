<link rel="stylesheet" href="{{ voyager_asset('css/app.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}">
<link rel="apple-touch-icon" type="image/png" href="{{asset('backend_resources/img/logo.png')}}"/>
<meta name="apple-mobile-web-app-title" content="CodePen">

<link rel="shortcut icon" type="image/x-icon" href="{{asset('backend_resources/img/logo.png')}}"/>

<link rel="mask-icon" type="image/x-icon" href="{{asset('backend_resources/img/logo.png')}}" color="#111"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
<meta name="description" content="admin login">
<title> {{ __('brand_title') }}</title>
<style>
    * {
        padding: 0;
        margin: 0;
        box-sizing: border-box;
    }

    body {
        width: 100%;
        min-height: 100vh;
        background: linear-gradient(-45deg, rgb(127, 116, 208), rgb(193, 191, 250));


    }

    .Alert {
        padding: 10px;
    }

    @media (min-width: 1024px)
        .Alert {
            padding: 10px;
        }

        .box {
            width: 70%;
            background: linear-gradient(-45deg, rgb(190, 190, 240), rgb(123, 120, 201));
            position: relative;
            overflow: hidden;
            border-radius: 10px;
            box-shadow: 0 0 10px 2px rgb(255, 255, 255, 0.4);
        }


        .box button {
            border: 2px solid grey;
            background: #fff;
            width: 230px;
            color: rgb(109, 99, 190);
            font-weight: bolder;
            font-size: 1.2rem;
            border-radius: 30px;
            padding: 6px;
            box-shadow: -2px 5px 10px 2px rgb(180, 180, 245);
            cursor: pointer;
        }


        a {
            color: #fff;
            text-decoration: none;
        }


        .market-btn .market-button-title {
            display: block;
            color: #222;
            font-size: 1.125rem;
        }

        .market-btn .market-button-subtitle {
            display: block;
            margin-bottom: -0.25rem;
            color: #888;
            font-size: 0.75rem;
        }


        input[type="radio"] {
            -ms-transform: scale(2); /* IE 9 */
            -webkit-transform: scale(2); /* Chrome, Safari, Opera */
            transform: scale(2);
        }

        .table thead th {
            vertical-align: bottom;
            border-bottom: 2px solid #dee2e6;
            color: black !important;
        }

        .Table td, .Table th {
            border: 1px solid #d4d2d0;
            padding: 0.5em 1em;
            text-align: center;
        }

</style>

<center>

    <div class="box-p">

        <br>
        <h2 style="color: white">
            <span>
             <img height="70px;" src="{{asset('assets/img/logo.png')}}">
        </span>
            {{__('brand_title')}}
        </h2>

        <br>

        <div class="box">

            <h2>{{__('Select Secretary')}}</h2>


            <div id="login-div">


                <form id="form" action="{{ route('make-vote') }}" method="POST">
                    {{ csrf_field() }}




                    @if(!$errors->isEmpty())
                        <div class="alert alert-red">
                            <ul class="list-unstyled">
                                @foreach($errors->all() as $err)
                                    <li>{{ $err }}</li>
                                @endforeach
                            </ul>
                        </div>
                @endif

                <!--desktop View start-->
                    <table style="background: transparent;" class="table desktop-mode" id="data-taable">
                        <thead>


                        <tr>
                            <th colspan="2" style="color: #a93611 !important;">
                                {{__('Voting Starting Date')}}:{{$vote_date_start}}
                                থেকে {{$vote_date_end}} পর্যন্ত

                            </th>
                        </tr>
                        @foreach($c as $k =>  $r)
                            <tr>
                                <th>
                                    <label style="cursor: pointer;margin-bottom: 2.5rem;font-size: 1.8rem">
                                        <input required type="radio" name="candidate" value="{{$k}}">
                                        <span> {{$r['name']}}</span>

                                        <img height="100px" src="{{$r['picture_src']}}">
                                    </label>


                                </th>


                            </tr>
                        @endforeach


                        </thead>
                    </table>
                    <!--desktop View end-->
                    <!--mobile View start-->
                    <table style="background: transparent;" class="table mobile-mode" id="data-taable">
                        <thead>


                        <tr>
                            <th style="font-weight: bold;">
                                {{__('Voting Starting Date')}}:{{$vote_date_start}}
                                থেকে {{$vote_date_end}} পর্যন্ত

                            </th>
                        </tr>
                        @foreach($c as $k =>  $r)
                            <tr>
                                <th>
                                    <label style="cursor: pointer;margin-bottom: 2.5rem;font-size: 1.8rem">
                                        <input required type="radio" name="candidate" value="{{$k}}">
                                        <span> {{$r['name']}}</span>

                                        <img height="100px" src="{{$r['picture_src']}}">
                                    </label>


                                </th>


                            </tr>
                        @endforeach


                        </thead>
                    </table>
                    <!--mobile View end-->

                    <button id="make-vote-btn"
                            style="background: #349956;color: white;" type="button"
                            class="btn btn-success vote-now">
                        {{__('Vote')}}
                        <ion-icon name="chevron-forward-outline"></ion-icon>
                    </button>


                </form>


            </div>


        </div>
        <div class="row copyright">
            <div class="col">
                <p>পরিকল্পনা ও বাস্তবায়নে</p>
                <a href="#" target="_blank">

                    {{__('client_title')}}
                </a>

            </div>


        </div>
    </div>


</center>


<style>
    button {
        background-color: #04AA6D;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 100%;
        opacity: 0.9;
    }

    button:hover {
        opacity: 1;
    }

    /* Float cancel and delete buttons and add an equal width */
    .cancelbtn, .deletebtn {
        float: left;
        width: 50%;
    }

    /* Add a color to the cancel button */
    .cancelbtn {
        background-color: #ccc;
        color: black;
    }

    /* Add a color to the delete button */
    .deletebtn {
        background-color: #349956;
    }

    /* Add padding and center-align text to the container */
    .container {
        padding: 16px;
        text-align: center;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        left: 26%;
        top: 26%;
        width: 45%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        padding-top: 50px;
    }

    /* Modal Content/Box */
    .modal-content {
        background-color: #fefefe;
        margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
        border: 1px solid #888;
        width: 80%; /* Could be more or less, depending on screen size */
    }

    /* Style the horizontal ruler */
    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }


    /* Clear floats */
    .clearfix::after {
        content: "";
        clear: both;
        display: table;
    }

    /* Change styles for cancel button and delete button on extra small screens */
    @media screen and (max-width: 300px) {
        .cancelbtn, .deletebtn {
            width: 100%;
        }
    }
</style>


<div id="id01" class="modal" data-backdrop="static" data-keyboard="false">

    <form class="modal-content">
        <div class="container">
            <h4>আপনার ভোট নিশ্চিত করুন </h4>
            <hr>
            <p>আপনি কি এখন ভোট দিতে চান?</p>

            <div class="clearfix">
                <button type="button" onclick="hideModal()" class="cancelbtn">
                    না
                </button>
                <button type="button" onclick="makeVote()" class="deletebtn">
                    হ্যাঁ
                </button>
            </div>
        </div>
    </form>
</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script>


    $('#make-vote-btn').on('click', function () {
        showModal();
    })

    function hideModal() {
        $('#make-vote-btn').fadeIn('fast')
        $('#id01').hide();
    }

    function showModal() {
        $('#id01').show();
    }

    function makeVote() {
        $('#form').submit()
    }

    document.addEventListener('contextmenu', function (e) {
        //e.preventDefault();
    });


    const targetNode = document.body;
    const config = {childList: true, subtree: true, attributes: true};

    const callback = function (mutationsList, observer) {
        for (let mutation of mutationsList) {

            if (mutation.type === 'attributes') {

                console.log(mutation.attributeName)
                if (mutation.attributeName != 'style') {
                    alert('সঠিক নিয়মে চেস্টা করুন')
                    location = '';
                }

            }
        }
    };

    const observer = new MutationObserver(callback);
    //observer.observe(targetNode, config);


</script>

