<?php

return [
    'required' => "This Field is Required",
    'regex' => "Data format is Invalid",
    'user' => [
        'username' => [
            'remote' => 'Username has already been taken',
            "username_validate" => "Username should contain at least one upper and lower case letter and one digit"
        ],
        'cell_phone' => [
            'regex' => 'Invalid Cell Phone'
        ],
        'password_confirmation' => [
            'equalTo' => "Passwords didn't match"
        ],
        'password' => [
            'password_validate' => "Password should contain at least one upper and lower case letter and one digit"
        ],
        'data_access_area' => [
            'required' => 'Data Access Area is required.'
        ]
    ]
];
