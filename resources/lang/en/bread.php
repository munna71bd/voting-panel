<?php

return [
    'permissions' => [
        'model_name' => 'Permissions',
        'key' => 'Key',
        'table_name' => 'Table Name',
        'is_user_defined' => 'Is User Defined',
        'sub_group' => 'Sub Group',
        'sub_group_order' => 'Sub Group Order',
    ],
    'roles' => [
        'model_name' => 'Role',
        'name' => 'Name',
        'display_name' => 'Display Name',
    ],
    'users' => [
        'model_name' => 'Users',
        'name' => 'Name',
        'name' => 'Name In English',
        'name_bn' => 'Name In Bengali',
        'email' => 'Email',
        'is_super_user' => 'Is Super User',
        'user_type' => 'User Type',
        'username' => 'User Name',
        'cell_phone' => 'Mobile Number',
        'avatar' => 'Avatar',
        'role' => 'Role',
        'roles' => 'Roles',
        'data_access_area' => 'Access',
        'Name' => 'Name',
        'Email' => 'Email',
        'Avatar' => 'Avatar',
        'Role' => 'Role',
        'Roles' => 'Roles',
    ],
    'loc-upazilas' => [
        'model_name' => 'Upazila',
        'division_bbs_code' => 'Division Bbs Code',
        'district_bbs_code' => 'District Bbs Code',
        'title_en' => 'Title (English)',
        'title' => 'Title (Bangla)',
        'bbs_code' => 'Bbs Code',
        'division' => 'Division',
        'district' => 'District',
    ],
    'loc-districts' => [
        'model_name' => 'District',
        'division_bbs_code' => 'Division Bbs Code',
        'title_en' => 'Title (English)',
        'title' => 'Title (Bangla)',
        'bbs_code' => 'Bbs Code',
        'division' => 'Division',
    ],
    'loc-divisions' => [
        'model_name' => 'Division',
        'title_en' => 'Title (English)',
        'title' => 'Title (Bangla)',
        'bbs_code' => 'Bbs Code'
    ],
    'loc-municipalities' => [
        'model_name' => 'Municipality',
        'geo_code' => 'Geo Code',
        'municipality_type' => 'Type'
    ],
    'org-designations' => [
        'parent_designation' => 'Parent',
        'ordering' => 'Order'
    ],
    'office-informations' => [
        'title_bn' => 'Title (bn)',
        'row_status' => 'Status',
        'loc_divisions_title' => 'Division',
        'loc_districts_title' => 'District ',
        'loc_upazilas_title' => 'Upazila',
        'loc_unions_title' => 'Union',
        'office_informations_title_bn' => 'Parent Office',
        'parent_office' => 'Parent Office',
        'contact_mobile' => 'Cell Phone',
        'office_type' => 'Office Type',
        "address" => 'Address',
        'contact_phone' => 'Phone',
        'contact_email' => 'Email',
        'contact_fax' => 'Fax',
        'action' => 'Action'
    ],
    'financial-years' => [
        'model_name' => 'Financial Year',
        'title' => 'Title',
        'row_status' => 'Status'
    ],
    'banks' => [
        'model_name' => 'Bank',
        'name' => 'Bank Name',
        'type' => 'Bank Type',
        'head_office' => 'Head Office',
        'swift_code' => 'Swift Code'
    ],
    'committee_types' => [
        'model_name' => 'Committee Type'
    ]
];
