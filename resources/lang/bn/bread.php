<?php

return [
    'permissions' => [
        'model_name' => 'অনুমতি',
        'key' => 'টীকা',
        'table_name' => 'টেবিলের নাম',
        'is_user_defined' => 'ব্যবহারকারী সংজ্ঞায়িত',
        'sub_group' => 'সাব গ্রুপ',
        'sub_group_order' => 'সাব গ্রুপ অর্ডার',
    ],
    'designations' => [
        'model_name' => 'পদবি',
        'Code' => 'কোড',
        'Name' => 'নাম'
    ],
    'roles' => [
        'model_name' => 'রোল',
        'name' => 'নাম (ইংরেজি)',
        'display_name' => 'প্রদর্শিত নাম',
    ],
    'users' => [
        'model_name' => 'ব্যবহারকারী',
        'name' => 'নাম (ইংরেজি)',
        'name_bn' => 'নাম (বাংলা)',
        'email' => 'ইমেইল',
        'is_super_user' => 'সুপার ইউজার?',
        'user_type' => 'ব্যবহারকারীর ধরণ',
        'username' => 'ব্যবহারকারীর নাম',
        'cell_phone' => 'মোবাইল নম্বর',
        'avatar' => 'ছবি',
        'role' => 'রোল',
        'roles' => 'রোলসমূহ',
        'password' => 'পাসওয়ার্ড',
        'data_access_area' => 'প্রবেশাধিকার ধরণ',
        'Name' => 'নাম',
        'Email' => 'ইমেইল',
        'Avatar' => 'ছবি',
        'Role' => 'রোল',
        'Roles' => 'রোলসমূহ',
    ],
    'loc-upazilas' => [
        'model_name' => 'উপজেলা',
        'division_bbs_code' => 'বিভাগ বিবিএস কোড',
        'district_bbs_code' => 'জেলা বিবিএস কোড',
        'title_en' => 'শিরোনাম (ইংরেজি)',
        'title' => 'শিরোনাম',
        'bbs_code' => 'বিবিএস কোড'
    ],
    'loc-districts' => [
        'model_name' => 'জেলা',
        'division_bbs_code' => 'বিভাগ বিবিএস কোড',
        'bbs_code' => 'বিবিএস কোড'
    ],
    'loc-divisions' => [
        'model_name' => 'বিভাগ',
        'title_en' => 'শিরোনাম (ইংরেজি)',
        'title' => 'শিরোনাম',
        'bbs_code' => 'বিবিএস কোড'
    ],
    'loc-municipalities' => [
        'model_name' => 'মিউনিসিপালিটি',
        'geo_code' => 'জিও কোড',
        'municipality_type' => 'ধরন'
    ],
    'financial-years' => [
        'model_name' => 'অর্থবছর',
        'title' => 'শিরোনাম',
        'row_status' => 'অবস্থা'
    ],
    'org-designations' => [
        'parent_designation' => 'প্যারেন্ট',
        'ordering' => 'ক্রম'
    ],
    'office-informations' => [
        'title_bn' => 'শিরোনাম (বাংলা)',
        'row_status' => 'অবস্থা',
        'loc_divisions_title' => 'বিভাগ',
        'loc_districts_title' => 'জেলা ',
        'loc_upazilas_title' => 'উপজেলা',
        'loc_unions_title' => 'ইউনিয়ন',
        'office_informations_title_bn' => 'প্যারেন্ট অফিস',
        'parent_office' => 'প্যারেন্ট অফিস',
        'contact_mobile' => 'সেল ফোন',
        'office_type' => 'অফিসের ধরন',
        "address" => 'ঠিকানা',
        'contact_phone' => 'ফোন',
        'contact_email' => 'ই-মেইল',
        'contact_fax' => 'ফ্যাক্স',
        'action' => 'পদক্ষেপ'
    ],
    'banks' => [
        'model_name' => 'ব্যাংক',
        'name' => 'ব্যাংকের নাম',
        'type' => 'ব্যাংকের ধরণ',
        'head_office' => 'প্রধান কার্যালয়ের ঠিকানা',
        'swift_code' => 'সুইফট কোড'
    ],
    'committee_types' => [
        'model_name' => 'কমিটির ধরন',
    ]
];
