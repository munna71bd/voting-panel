<?php

// DataTable translations from: https://github.com/DataTables/Plugins/tree/master/i18n
return [
    "sProcessing" => "প্রসেসিং হচ্ছে...",
    "sLengthMenu" => "_MENU_ টা এন্ট্রি",
    "sZeroRecords" => "আপনি যা অনুসন্ধান করেছেন তার সাথে মিলে যাওয়া কোন রেকর্ড খুঁজে পাওয়া যায় নাই",
    "sInfo" => "_TOTAL_ টা এন্ট্রির মধ্যে _START_ থেকে _END_ পর্যন্ত দেখানো হচ্ছে",
    "sInfoEmpty" => "কোন এন্ট্রি খুঁজে পাওয়া যায় নাই",
    "sInfoFiltered" => "(মোট _MAX_ টা এন্ট্রির মধ্যে থেকে বাছাইকৃত)",
    "sInfoPostFix" => "",
    "sSearch" => "অনুসন্ধান:",
    "sUrl" => "",
    "oPaginate" => [
        "sFirst" => "প্রথ",
        "sPrevious" => "পূর্ববর্তী",
        "sNext" => "পরবর্তী",
        "sLast" => "শেষ"
    ],
    'oAria' => [
        'sSortAscending'  => ': activate to sort column ascending',
        'sSortDescending' => ': activate to sort column descending',
    ],
];
