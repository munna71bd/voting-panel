<?php

return [
    'avatar'           => 'ফটো',
    'edit'             => 'প্রোফাইল সংশোধন করুন',
    'edit_user'        => 'ব্যবহারকারী সংশোধন করুন',
    'password'         => 'পাসওয়ার্ড',
    'password_hint'    => 'একই পাসওয়ার্ড রাখতে ফাকা রাখুন',
    'role'             => 'রোল',
    'roles'            => 'রোল সমূহ',
    'role_default'     => 'ডিফল্ট রোল',
    'roles_additional' => 'অতিরিক্ত রোল',
    'user_role'        => 'ব্যবহারকারীর রোল',
];
