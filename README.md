## Voting Panel

php artisan migrate
## Installation
  Create a virtual host for the project and edit the .env file.Create the master
  database named "voting_system".
  After that run the following commands. 
    
    php composer.phar dump-autoload
    php artisan setup:database --withoutmaster=true
    php artisan migrate
    php artisan db:seed --class=VoyagerDatabaseSeeder
    php artisan db:seed --class=GeoLocationDatabaseSeeder
    php artisan db:seed --class=UsersTableSeeder
    php artisan cache:clear
    php artisan config:clear
    php artisan view:clear
    php artisan route:clear
    
    
## Contributing
    Mehedi Hasan Munna

## License
Mehedi Hasan Munna all right reserves.

## Conditionally Loading Form input Drop-Down in bread
  ```
    {
	   "depend-on": ["loc_division_id"],
	   "additional-queries": {
	       "row_status": 1
	   }
	}
  ```
