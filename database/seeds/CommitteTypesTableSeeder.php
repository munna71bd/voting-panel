<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CommitteTypesTableSeeder extends Seeder
{
    public function run()
    {
        $now = \Carbon\Carbon::now('Asia/Dhaka');
        $data = [
            [
                'title' => "Pic Committee",
                'code' => "PIC",
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => $now->format('Y-m-d H:i:s'),
                'updated_at' => $now->format('Y-m-d H:i:s'),
            ],
            [
                'title' => "Village Committee",
                'code' => 'VILLAGE',
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => $now->format('Y-m-d H:i:s'),
                'updated_at' => $now->format('Y-m-d H:i:s'),
            ]
        ];

        $connections = config('database.mysql_connections');
        foreach ($connections as $connection => $config) {
            Schema::connection($connection)->disableForeignKeyConstraints();
            DB::connection($connection)->table('committee_types')->truncate();
            DB::connection($connection)->table('committee_types')->insert($data);
            Schema::connection($connection)->enableForeignKeyConstraints();
        }

    }
}
