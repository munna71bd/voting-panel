<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class LocUpazillasTableDistrictWiseSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $connections = config('database.mysql_connections');
        foreach ($connections as $connName => $config) {
            if ($connName === 'mysql_master' || empty($config['district_bbs'])) {
                continue;
            }

            $unions = DB::table('loc_upazilas')->where('district_bbs_code', $config['district_bbs'])->get();
            $dataToInsert = [];
            foreach ($unions as $union) {
                $unionArr = json_decode(json_encode($union), true);
                $dataToInsert[] = $unionArr;
            }
            Schema::connection($connName)->disableForeignKeyConstraints();
            DB::connection($connName)->table('loc_upazilas')->delete();
            DB::connection($connName)->table('loc_upazilas')->insert($dataToInsert);
            Schema::connection($connName)->enableForeignKeyConstraints();
        }

    }
}
