<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CusDataTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        DB::table('data_types')->truncate();

        DB::table('data_types')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'users',
                'slug' => 'users',
                'display_name_singular' => 'User',
                'display_name_plural' => 'Users',
                'icon' => 'voyager-person',
                'model_name' => 'App\\Models\\User',
                'policy_name' => 'App\\Policies\\UserPolicy',
                'controller' => 'App\\Http\\Controllers\\Voyager\\VoyagerUserController',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2020-02-29 14:39:31',
                'updated_at' => '2020-02-29 14:39:31',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'menus',
                'slug' => 'menus',
                'display_name_singular' => 'Menu',
                'display_name_plural' => 'Menus',
                'icon' => 'voyager-list',
                'model_name' => 'App\\Models\\Menu',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2020-02-29 14:39:31',
                'updated_at' => '2020-02-29 14:39:31',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'roles',
                'slug' => 'roles',
                'display_name_singular' => 'Role',
                'display_name_plural' => 'Roles',
                'icon' => 'voyager-lock',
                'model_name' => 'App\\Models\\Role',
                'policy_name' => NULL,
                'controller' => '',
                'description' => '',
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => NULL,
                'created_at' => '2020-02-29 14:39:31',
                'updated_at' => '2020-02-29 14:39:31',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'loc_divisions',
                'slug' => 'loc-divisions',
                'display_name_singular' => 'Loc Division',
                'display_name_plural' => 'Loc Divisions',
                'icon' => NULL,
                'model_name' => 'App\\Models\\LocDivision',
                'policy_name' => 'App\\Policies\\LocDivisionPolicy',
                'controller' => 'App\\Http\\Controllers\\LocDivisionController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"title_en","order_display_column":"title_en","order_direction":"asc","default_search_key":"title_en","scope":"acl"}',
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-03-13 17:00:10',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'loc_districts',
                'slug' => 'loc-districts',
                'display_name_singular' => 'Loc District',
                'display_name_plural' => 'Loc Districts',
                'icon' => 'voyager-move',
                'model_name' => 'App\\Models\\LocDistrict',
                'policy_name' => 'App\\Policies\\LocDistrictPolicy',
                'controller' => 'App\\Http\\Controllers\\LocDistrictController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"bbs_code","order_display_column":"bbs_code","order_direction":"asc","default_search_key":"bbs_code","scope":"acl"}',
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-03-13 17:25:35',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'loc_upazilas',
                'slug' => 'loc-upazilas',
                'display_name_singular' => 'Loc Upazila',
                'display_name_plural' => 'Loc Upazilas',
                'icon' => 'voyager-controller',
                'model_name' => 'App\\Models\\LocUpazila',
                'policy_name' => 'App\\Policies\\LocUpazillaPolicy',
                'controller' => 'App\\Http\\Controllers\\LocUpazilaController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"title_en","order_display_column":"title_en","order_direction":"asc","default_search_key":"title_en","scope":"acl"}',
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-03-13 21:50:47',
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'loc_unions',
                'slug' => 'loc-unions',
                'display_name_singular' => 'Loc Union',
                'display_name_plural' => 'Loc Unions',
                'icon' => 'voyager-upload',
                'model_name' => 'App\\Models\\LocUnion',
                'policy_name' => 'App\\Policies\\LocUnionPolicy',
                'controller' => 'App\\Http\\Controllers\\LocUnionController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"title_en","order_display_column":"title_en","order_direction":"asc","default_search_key":"title_en","scope":"acl"}',
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-03-13 17:37:00',
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'loc_villages',
                'slug' => 'loc-villages',
                'display_name_singular' => 'Loc Village',
                'display_name_plural' => 'Loc Villages',
                'icon' => 'voyager-dollar',
                'model_name' => 'App\\Models\\LocVillage',
                'policy_name' => 'App\\Policies\\LocVillegePolicy',
                'controller' => 'App\\Http\\Controllers\\LocVillageController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"title_en","order_display_column":"title_en","order_direction":"asc","default_search_key":"title_en","scope":"acl"}',
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-03-13 17:43:30',
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'loc_municipalities',
                'slug' => 'loc-municipalities',
                'display_name_singular' => 'Loc Municipality',
                'display_name_plural' => 'Loc Municipalities',
                'icon' => 'voyager-chat',
                'model_name' => 'App\\Models\\LocMunicipality',
                'policy_name' => 'App\\Policies\\LocMunicipalityPolicy',
                'controller' => 'App\\Http\\Controllers\\LocMunicipalityController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"title_en","order_display_column":"title_en","order_direction":"asc","default_search_key":"title_en","scope":"acl"}',
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-03-13 17:35:21',
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'permissions',
                'slug' => 'permissions',
                'display_name_singular' => 'Permission',
                'display_name_plural' => 'Permissions',
                'icon' => 'voyager-lock',
                'model_name' => 'App\\Models\\Permission',
                'policy_name' => NULL,
                'controller' => 'App\\Http\\Controllers\\Voyager\\VoyagerPermissionController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 0,
                'details' => '{"order_column":"table_name","order_display_column":"table_name","order_direction":"asc","default_search_key":"table_name","scope":null}',
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-03-02 12:26:16',
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'org_designations',
                'slug' => 'org-designations',
                'display_name_singular' => 'Org Designation',
                'display_name_plural' => 'Org Designations',
                'icon' => 'voyager-diamond',
                'model_name' => 'App\\Models\\OrgDesignation',
                'policy_name' => 'App\\Policies\\OrgDesignationPolicy',
                'controller' => 'App\\Http\\Controllers\\OrgDesignationsController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"title_en","order_display_column":"title_en","order_direction":"asc","default_search_key":null,"scope":"acl"}',
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-13 17:48:36',
            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'financial_years',
                'slug' => 'financial-years',
                'display_name_singular' => 'Financial Year',
                'display_name_plural' => 'Financial Years',
                'icon' => 'voyager-calendar',
                'model_name' => 'App\\Models\\FinancialYear',
                'policy_name' => NULL,
                'controller' => 'App\\Http\\Controllers\\FinancialYearController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"title","order_display_column":"title","order_direction":"asc","default_search_key":"title","scope":null}',
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-13 17:45:04',
            ),
            12 =>
            array (
                'id' => 13,
                'name' => 'office_informations',
                'slug' => 'office-informations',
                'display_name_singular' => 'Office Information',
                'display_name_plural' => 'Office Informations',
                'icon' => NULL,
                'model_name' => 'App\\Models\\OfficeInformation',
                'policy_name' => 'App\\Policies\\OfficeInformationPolicy',
                'controller' => 'App\\Http\\Controllers\\OfficeInformationController',
                'description' => NULL,
                'generate_permissions' => 1,
                'server_side' => 1,
                'details' => '{"order_column":"title_en","order_display_column":"title_en","order_direction":"asc","default_search_key":"title_en","scope":"acl"}',
                'created_at' => '2020-03-11 19:39:55',
                'updated_at' => '2020-03-13 17:46:56',
            ),
        ));

        Schema::enableForeignKeyConstraints();
    }
}
