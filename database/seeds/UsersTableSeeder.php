<?php

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        if (User::count() == 0) {
            $role = Role::where('name', 'super_admin')->firstOrFail();

            DB::table('users')->insert([
                [
                    'name' => 'Admin',
                    'email' => 'admin@admin.com',
                    'username' => 'admin',
                    'data_access_area' => User::ACCESS_AREA_SYSTEM,
                    'cell_phone' => '01790635944',
                    'password' => Hash::make('admiN123'),
                    'remember_token' => Str::random(60),
                    'role_id' => $role->id,

                ], [
                    'name' => 'mahmud',
                    'email' => 'mahmud@admin.com',
                    'username' => 'mahmud',
                    'data_access_area' => User::ACCESS_AREA_ALL,
                    'cell_phone' => '01790635943',
                    'password' => Hash::make('mahmuD123'),
                    'remember_token' => Str::random(60),
                    'role_id' => $role->id,

                ],

                [
                    'name' => 'Mehedi Munna',
                    'email' => 'munna@admin.com',
                    'username' => 'munna',
                    'data_access_area' => User::ACCESS_AREA_CLUSTER,
                    'cell_phone' => '01790635941',
                    'password' => Hash::make('munnA123'),
                    'remember_token' => Str::random(60),
                    'role_id' => 3,

                ],
                [
                    'name' => 'MD. SHAHED',
                    'email' => 'shahed@admin.com',
                    'username' => 'Shahed',
                    'data_access_area' => User::ACCESS_AREA_CDC,
                    'cell_phone' => '01716879647',
                    'password' => Hash::make('shaheD123'),
                    'remember_token' => Str::random(60),
                    'role_id' => 3,

                ],
                [
                    'name' => 'Pavel Akter',
                    'email' => 'pavel@admin.com',
                    'username' => 'pavel',
                    'data_access_area' => User::ACCESS_AREA_SCG,
                    'cell_phone' => '01716879649',
                    'password' => Hash::make('paveL123'),
                    'remember_token' => Str::random(60),
                    'role_id' => 4,

                ]
            ]);
        }
    }
}
