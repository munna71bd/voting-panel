<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class ScmpsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $data =array ();
        for($i=0;$i<70;$i++)
        {
            $x=array (

                'scg_id' => 1,
                'member_code' => rand(546567,56645645),
                'member_name_en' => str_random(14),
                'member_name_bn' => str_random(10),
                'member_nid' => rand(10678,106898),
                'member_dob' => '1984-01-0'.rand(1,9),
                'member_picture' => '',
                'fathers_name' =>  str_random(10),
                'mothers_name' =>  str_random(10),
                'spouse_name' =>  str_random(10),
                'member_cell_no' => '+880184017'.rand(1111,9999),
                'gender' => 'male',
                'religion' => 'islam',
                'marital_status' => 'married',
                'education' => 'N/A',
                'profession' => 'N/A',
                'active_date' => '2020-03-19',
                'deactive_date' => '',
                'present_address' => '',
                'permanent_address' => '',
                'row_status' => 1,
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            );
            array_push($data,$x);
            $x=array (

                'scg_id' => 2,
                'member_code' => rand(546567,56645645),
                'member_name_en' => str_random(14),
                'member_name_bn' => str_random(10),
                'member_nid' => rand(10678,106898),
                'member_dob' => '1984-01-0'.rand(1,9),
                'member_picture' => '',
                'fathers_name' =>  str_random(10),
                'mothers_name' =>  str_random(10),
                'spouse_name' =>  str_random(10),
                'member_cell_no' => '+880184017'.rand(1111,9999),
                'gender' => 'female',
                'religion' => 'hindu',
                'marital_status' => 'married',
                'education' => 'N/A',
                'profession' => 'N/A',
                'active_date' => '2020-03-19',
                'deactive_date' => '',
                'present_address' => '',
                'permanent_address' => '',
                'row_status' => 1,
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            );
            array_push($data,$x);
        }

        Schema::disableForeignKeyConstraints();
        DB::table('scmps')->truncate();

        DB::table('scmps')->insert($data);
        Schema::enableForeignKeyConstraints();

    }
}
