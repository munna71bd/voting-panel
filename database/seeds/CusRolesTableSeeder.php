<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CusRolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        DB::table('roles')->truncate();

        DB::table('roles')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'super_admin',
                'display_name' => 'Super Administrator',
                'created_at' => '2020-02-29 14:39:31',
                'updated_at' => '2020-02-29 14:39:31',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'user',
                'display_name' => 'Normal User',
                'created_at' => '2020-02-29 14:39:31',
                'updated_at' => '2020-02-29 14:39:31',
            ),

            2 =>
                array (
                    'id' => 3,
                    'name' => 'cf',
                    'display_name' => 'CF',
                    'created_at' => '2020-02-29 14:39:31',
                    'updated_at' => '2020-02-29 14:39:31',
                ),
            3 =>
                array (
                    'id' => 4,
                    'name' => 'team_leader',
                    'display_name' => 'Team Leader',
                    'created_at' => '2020-02-29 14:39:31',
                    'updated_at' => '2020-02-29 14:39:31',
                ),
        ));

        Schema::enableForeignKeyConstraints();
    }
}
