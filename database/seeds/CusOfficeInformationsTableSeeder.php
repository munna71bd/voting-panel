<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CusOfficeInformationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        DB::table('office_informations')->truncate();

        DB::table('office_informations')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title_en' => 'Headquarter',
                'title_bn' => 'সদর দপ্তর',
                'loc_division_id' => NULL,
                'loc_district_id' => NULL,
                'loc_upazila_id' => NULL,
                'loc_union_id' => NULL,
                'parent_office_id' => NULL,
                'address' => 'Dhaka',
                'contact_phone' => NULL,
                'contact_email' => NULL,
                'contact_fax' => NULL,
                'contact_mobile' => '01790325649',
                'office_type' => 2,
                'row_status' => 1,
                'created_by' => 2,
                'updated_by' => NULL,
                'created_at' => '2020-03-12 11:49:31',
                'updated_at' => '2020-03-12 11:49:31',
            ),
            1 =>
            array (
                'id' => 2,
                'title_en' => 'Ministry',
                'title_bn' => 'মন্ত্রণালয়',
                'loc_division_id' => NULL,
                'loc_district_id' => NULL,
                'loc_upazila_id' => NULL,
                'loc_union_id' => NULL,
                'parent_office_id' => NULL,
                'address' => 'Dhaka',
                'contact_phone' => NULL,
                'contact_email' => NULL,
                'contact_fax' => NULL,
                'contact_mobile' => '01752148963',
                'office_type' => 1,
                'row_status' => 1,
                'created_by' => 2,
                'updated_by' => NULL,
                'created_at' => '2020-03-12 12:17:28',
                'updated_at' => '2020-03-12 12:17:28',
            ),
            2 =>
            array (
                'id' => 3,
                'title_en' => 'Mymensingh Division Office',
                'title_bn' => 'ময়মনসিংহ বিভাগ অফিস',
                'loc_division_id' => 9,
                'loc_district_id' => NULL,
                'loc_upazila_id' => NULL,
                'loc_union_id' => NULL,
                'parent_office_id' => 1,
                'address' => 'Mymensingh',
                'contact_phone' => NULL,
                'contact_email' => NULL,
                'contact_fax' => NULL,
                'contact_mobile' => '01752489632',
                'office_type' => 3,
                'row_status' => 1,
                'created_by' => 2,
                'updated_by' => 2,
                'created_at' => '2020-03-12 12:19:00',
                'updated_at' => '2020-03-12 14:33:13',
            ),
            3 =>
            array (
                'id' => 4,
                'title_en' => 'Sherpur District Office',
                'title_bn' => 'শেরপুর জেলা অফিস',
                'loc_division_id' => 9,
                'loc_district_id' => 33,
                'loc_upazila_id' => NULL,
                'loc_union_id' => NULL,
                'parent_office_id' => 3,
                'address' => 'Sherpur',
                'contact_phone' => NULL,
                'contact_email' => 'asmmahmud@gmail.com',
                'contact_fax' => NULL,
                'contact_mobile' => '01790635942',
                'office_type' => 5,
                'row_status' => 1,
                'created_by' => 2,
                'updated_by' => NULL,
                'created_at' => '2020-03-12 14:35:18',
                'updated_at' => '2020-03-12 14:35:18',
            ),
            4 =>
            array (
                'id' => 5,
                'title_en' => 'Nakla Upazila Office',
                'title_bn' => 'নকলা উপজেলা অফিস',
                'loc_division_id' => 9,
                'loc_district_id' => 33,
                'loc_upazila_id' => 254,
                'loc_union_id' => NULL,
                'parent_office_id' => 4,
                'address' => 'Sherpur, Nakla',
                'contact_phone' => NULL,
                'contact_email' => NULL,
                'contact_fax' => NULL,
                'contact_mobile' => '01725963214',
                'office_type' => 6,
                'row_status' => 1,
                'created_by' => 2,
                'updated_by' => NULL,
                'created_at' => '2020-03-12 14:36:43',
                'updated_at' => '2020-03-12 14:36:43',
            ),
            5 =>
            array (
                'id' => 6,
                'title_en' => 'Gourdwar Union Office',
                'title_bn' => 'গৌড়দ্বার ইউনিয়ন অফিস',
                'loc_division_id' => 9,
                'loc_district_id' => 33,
                'loc_upazila_id' => 254,
                'loc_union_id' => 4398,
                'parent_office_id' => 5,
                'address' => 'Gourdwar, Nakla, Sherpur',
                'contact_phone' => NULL,
                'contact_email' => NULL,
                'contact_fax' => NULL,
                'contact_mobile' => '01745862356',
                'office_type' => 7,
                'row_status' => 1,
                'created_by' => 2,
                'updated_by' => NULL,
                'created_at' => '2020-03-12 14:42:07',
                'updated_at' => '2020-03-12 14:42:07',
            ),
        ));
        Schema::enableForeignKeyConstraints();

    }
}
