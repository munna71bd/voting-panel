<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class LoanApplicationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $data =array ();
        for($i=1;$i<30;$i++)
        {
            $status ='Pending';
            if($i%2==0){
                $status="Approved";
            }


            $x=array (

                'scmp_id' => $i,
                'cdc_id' => 1,
                'applied_amount' => rand(16000,17500),
                'approved_amount' => 16000,
                'service_charge' => 500,
                'application_date' => '2020-03-1'.rand(1,9),
                'loan_purpose' => 'need to open new business.',
                'loan_duration' => 6,
                'installment_type' => 'monthly',
                'grace_period' => 2,
                'status' => $status,
                'status_date' => '0000-00-00 00:00:00',
                'row_status' => 1,
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '0000-00-00 00:00:00',
            );

            array_push($data,$x);

        }

        Schema::disableForeignKeyConstraints();
        DB::table('loan_applications')->truncate();

        DB::table('loan_applications')->insert($data);
        Schema::enableForeignKeyConstraints();

    }
}
