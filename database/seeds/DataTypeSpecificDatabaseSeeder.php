<?php

use App\Voyager\Traits\Seedable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class DataTypeSpecificDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

//        $this->seed('AsmDataTypeLocVillageUnionSeeder');
//        $this->seed('AsmDataRowsLocVillageUnionSeeder');

        Schema::enableForeignKeyConstraints();
    }
}
