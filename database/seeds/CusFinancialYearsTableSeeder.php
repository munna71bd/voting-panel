<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CusFinancialYearsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        DB::table('financial_years')->truncate();

        DB::table('financial_years')->insert(array (
            0 =>
            array (
                'id' => 1,
                'title' => '2000-2001',
                'first_year' => 2000,
                'second_year' => 2001,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            1 =>
            array (
                'id' => 2,
                'title' => '2001-2002',
                'first_year' => 2001,
                'second_year' => 2002,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            2 =>
            array (
                'id' => 3,
                'title' => '2002-2003',
                'first_year' => 2002,
                'second_year' => 2003,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            3 =>
            array (
                'id' => 4,
                'title' => '2003-2004',
                'first_year' => 2003,
                'second_year' => 2004,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            4 =>
            array (
                'id' => 5,
                'title' => '2004-2005',
                'first_year' => 2004,
                'second_year' => 2005,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            5 =>
            array (
                'id' => 6,
                'title' => '2005-2006',
                'first_year' => 2005,
                'second_year' => 2006,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            6 =>
            array (
                'id' => 7,
                'title' => '2006-2007',
                'first_year' => 2006,
                'second_year' => 2007,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            7 =>
            array (
                'id' => 8,
                'title' => '2007-2008',
                'first_year' => 2007,
                'second_year' => 2008,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            8 =>
            array (
                'id' => 9,
                'title' => '2008-2009',
                'first_year' => 2008,
                'second_year' => 2009,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            9 =>
            array (
                'id' => 10,
                'title' => '2009-2010',
                'first_year' => 2009,
                'second_year' => 2010,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            10 =>
            array (
                'id' => 11,
                'title' => '2010-2011',
                'first_year' => 2010,
                'second_year' => 2011,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            11 =>
            array (
                'id' => 12,
                'title' => '2011-2012',
                'first_year' => 2011,
                'second_year' => 2012,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            12 =>
            array (
                'id' => 13,
                'title' => '2012-2013',
                'first_year' => 2012,
                'second_year' => 2013,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            13 =>
            array (
                'id' => 14,
                'title' => '2013-2014',
                'first_year' => 2013,
                'second_year' => 2014,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            14 =>
            array (
                'id' => 15,
                'title' => '2014-2015',
                'first_year' => 2014,
                'second_year' => 2015,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            15 =>
            array (
                'id' => 16,
                'title' => '2015-2016',
                'first_year' => 2015,
                'second_year' => 2016,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            16 =>
            array (
                'id' => 17,
                'title' => '2016-2017',
                'first_year' => 2016,
                'second_year' => 2017,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            17 =>
            array (
                'id' => 18,
                'title' => '2017-2018',
                'first_year' => 2017,
                'second_year' => 2018,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            18 =>
            array (
                'id' => 19,
                'title' => '2018-2019',
                'first_year' => 2018,
                'second_year' => 2019,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            19 =>
            array (
                'id' => 20,
                'title' => '2019-2020',
                'first_year' => 2019,
                'second_year' => 2020,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            20 =>
            array (
                'id' => 21,
                'title' => '2020-2021',
                'first_year' => 2020,
                'second_year' => 2021,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            21 =>
            array (
                'id' => 22,
                'title' => '2021-2022',
                'first_year' => 2021,
                'second_year' => 2022,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            22 =>
            array (
                'id' => 23,
                'title' => '2022-2023',
                'first_year' => 2022,
                'second_year' => 2023,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            23 =>
            array (
                'id' => 24,
                'title' => '2023-2024',
                'first_year' => 2023,
                'second_year' => 2024,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            24 =>
            array (
                'id' => 25,
                'title' => '2024-2025',
                'first_year' => 2024,
                'second_year' => 2025,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
            25 =>
            array (
                'id' => 26,
                'title' => '2025-2026',
                'first_year' => 2025,
                'second_year' => 2026,
                'row_status' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => '2020-03-11 15:17:29',
                'updated_at' => '2020-03-11 15:17:29',
            ),
        ));
        Schema::enableForeignKeyConstraints();

    }
}
