<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class LocMunicipalitiesTableDistrictWiseSeeder extends Seeder
{
    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $connections = config('database.mysql_connections');
        foreach ($connections as $connName => $config) {
            if ($connName === 'mysql_master' || empty($config['district_bbs'])) {
                continue;
            }

            $district = DB::table('loc_districts')->where('bbs_code', $config['district_bbs'])->first();
            $municipalities = DB::table('loc_municipalities')->where('loc_district_id', $district->id)->get();
            $dataToInsert = [];
            foreach ($municipalities as $municipality) {
                $unionArr = json_decode(json_encode($municipality), true);
                $dataToInsert[] = $unionArr;
            }
            Schema::connection($connName)->disableForeignKeyConstraints();
            DB::connection($connName)->table('loc_municipalities')->delete();
            DB::connection($connName)->table('loc_municipalities')->insert($dataToInsert);
            Schema::connection($connName)->enableForeignKeyConstraints();
        }
    }
}
