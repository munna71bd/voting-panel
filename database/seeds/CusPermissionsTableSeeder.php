<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CusPermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        Schema::disableForeignKeyConstraints();
        DB::table('permissions')->truncate();

        DB::table('permissions')->insert(array (
            0 =>
            array (
                'id' => 1,
                'key' => 'browse_admin',
                'table_name' => NULL,
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            1 =>
            array (
                'id' => 2,
                'key' => 'browse_bread',
                'table_name' => NULL,
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            2 =>
            array (
                'id' => 3,
                'key' => 'browse_database',
                'table_name' => NULL,
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            3 =>
            array (
                'id' => 4,
                'key' => 'browse_media',
                'table_name' => NULL,
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            4 =>
            array (
                'id' => 5,
                'key' => 'browse_compass',
                'table_name' => NULL,
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            5 =>
            array (
                'id' => 6,
                'key' => 'browse_menus',
                'table_name' => 'menus',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            6 =>
            array (
                'id' => 7,
                'key' => 'read_menus',
                'table_name' => 'menus',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            7 =>
            array (
                'id' => 8,
                'key' => 'edit_menus',
                'table_name' => 'menus',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            8 =>
            array (
                'id' => 9,
                'key' => 'add_menus',
                'table_name' => 'menus',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            9 =>
            array (
                'id' => 10,
                'key' => 'delete_menus',
                'table_name' => 'menus',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            10 =>
            array (
                'id' => 11,
                'key' => 'browse_roles',
                'table_name' => 'roles',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            11 =>
            array (
                'id' => 12,
                'key' => 'read_roles',
                'table_name' => 'roles',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            12 =>
            array (
                'id' => 13,
                'key' => 'edit_roles',
                'table_name' => 'roles',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            13 =>
            array (
                'id' => 14,
                'key' => 'add_roles',
                'table_name' => 'roles',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            14 =>
            array (
                'id' => 15,
                'key' => 'delete_roles',
                'table_name' => 'roles',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            15 =>
            array (
                'id' => 16,
                'key' => 'browse_users',
                'table_name' => 'users',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            16 =>
            array (
                'id' => 17,
                'key' => 'read_users',
                'table_name' => 'users',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            17 =>
            array (
                'id' => 18,
                'key' => 'edit_users',
                'table_name' => 'users',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            18 =>
            array (
                'id' => 19,
                'key' => 'add_users',
                'table_name' => 'users',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            19 =>
            array (
                'id' => 20,
                'key' => 'delete_users',
                'table_name' => 'users',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            20 =>
            array (
                'id' => 21,
                'key' => 'browse_settings',
                'table_name' => 'settings',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            21 =>
            array (
                'id' => 22,
                'key' => 'read_settings',
                'table_name' => 'settings',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            22 =>
            array (
                'id' => 23,
                'key' => 'edit_settings',
                'table_name' => 'settings',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            23 =>
            array (
                'id' => 24,
                'key' => 'add_settings',
                'table_name' => 'settings',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            24 =>
            array (
                'id' => 25,
                'key' => 'delete_settings',
                'table_name' => 'settings',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 14:39:32',
                'updated_at' => '2020-02-29 14:39:32',
            ),
            25 =>
            array (
                'id' => 26,
                'key' => 'browse_loc_divisions',
                'table_name' => 'loc_divisions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-02-29 15:26:51',
            ),
            26 =>
            array (
                'id' => 27,
                'key' => 'read_loc_divisions',
                'table_name' => 'loc_divisions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-02-29 15:26:51',
            ),
            27 =>
            array (
                'id' => 28,
                'key' => 'edit_loc_divisions',
                'table_name' => 'loc_divisions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-02-29 15:26:51',
            ),
            28 =>
            array (
                'id' => 29,
                'key' => 'add_loc_divisions',
                'table_name' => 'loc_divisions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-02-29 15:26:51',
            ),
            29 =>
            array (
                'id' => 30,
                'key' => 'delete_loc_divisions',
                'table_name' => 'loc_divisions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-02-29 15:26:51',
            ),
            30 =>
            array (
                'id' => 31,
                'key' => 'restore_loc_divisions',
                'table_name' => 'loc_divisions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-02-29 15:26:51',
            ),
            31 =>
            array (
                'id' => 32,
                'key' => 'force_delete_loc_divisions',
                'table_name' => 'loc_divisions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-02-29 15:26:51',
            ),
            32 =>
            array (
                'id' => 33,
                'key' => 'bulk_delete_loc_divisions',
                'table_name' => 'loc_divisions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:26:51',
                'updated_at' => '2020-02-29 15:26:51',
            ),
            33 =>
            array (
                'id' => 34,
                'key' => 'browse_loc_districts',
                'table_name' => 'loc_districts',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-02-29 15:35:49',
            ),
            34 =>
            array (
                'id' => 35,
                'key' => 'read_loc_districts',
                'table_name' => 'loc_districts',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-02-29 15:35:49',
            ),
            35 =>
            array (
                'id' => 36,
                'key' => 'edit_loc_districts',
                'table_name' => 'loc_districts',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-02-29 15:35:49',
            ),
            36 =>
            array (
                'id' => 37,
                'key' => 'add_loc_districts',
                'table_name' => 'loc_districts',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-02-29 15:35:49',
            ),
            37 =>
            array (
                'id' => 38,
                'key' => 'delete_loc_districts',
                'table_name' => 'loc_districts',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-02-29 15:35:49',
            ),
            38 =>
            array (
                'id' => 39,
                'key' => 'restore_loc_districts',
                'table_name' => 'loc_districts',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-02-29 15:35:49',
            ),
            39 =>
            array (
                'id' => 40,
                'key' => 'force_delete_loc_districts',
                'table_name' => 'loc_districts',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-02-29 15:35:49',
            ),
            40 =>
            array (
                'id' => 41,
                'key' => 'bulk_delete_loc_districts',
                'table_name' => 'loc_districts',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:35:49',
                'updated_at' => '2020-02-29 15:35:49',
            ),
            41 =>
            array (
                'id' => 42,
                'key' => 'browse_loc_upazilas',
                'table_name' => 'loc_upazilas',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-02-29 15:40:51',
            ),
            42 =>
            array (
                'id' => 43,
                'key' => 'read_loc_upazilas',
                'table_name' => 'loc_upazilas',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-02-29 15:40:51',
            ),
            43 =>
            array (
                'id' => 44,
                'key' => 'edit_loc_upazilas',
                'table_name' => 'loc_upazilas',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-02-29 15:40:51',
            ),
            44 =>
            array (
                'id' => 45,
                'key' => 'add_loc_upazilas',
                'table_name' => 'loc_upazilas',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-02-29 15:40:51',
            ),
            45 =>
            array (
                'id' => 46,
                'key' => 'delete_loc_upazilas',
                'table_name' => 'loc_upazilas',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-02-29 15:40:51',
            ),
            46 =>
            array (
                'id' => 47,
                'key' => 'restore_loc_upazilas',
                'table_name' => 'loc_upazilas',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-02-29 15:40:51',
            ),
            47 =>
            array (
                'id' => 48,
                'key' => 'force_delete_loc_upazilas',
                'table_name' => 'loc_upazilas',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-02-29 15:40:51',
            ),
            48 =>
            array (
                'id' => 49,
                'key' => 'bulk_delete_loc_upazilas',
                'table_name' => 'loc_upazilas',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:40:51',
                'updated_at' => '2020-02-29 15:40:51',
            ),
            49 =>
            array (
                'id' => 50,
                'key' => 'browse_loc_unions',
                'table_name' => 'loc_unions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-02-29 15:45:27',
            ),
            50 =>
            array (
                'id' => 51,
                'key' => 'read_loc_unions',
                'table_name' => 'loc_unions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-02-29 15:45:27',
            ),
            51 =>
            array (
                'id' => 52,
                'key' => 'edit_loc_unions',
                'table_name' => 'loc_unions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-02-29 15:45:27',
            ),
            52 =>
            array (
                'id' => 53,
                'key' => 'add_loc_unions',
                'table_name' => 'loc_unions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-02-29 15:45:27',
            ),
            53 =>
            array (
                'id' => 54,
                'key' => 'delete_loc_unions',
                'table_name' => 'loc_unions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-02-29 15:45:27',
            ),
            54 =>
            array (
                'id' => 55,
                'key' => 'restore_loc_unions',
                'table_name' => 'loc_unions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-02-29 15:45:27',
            ),
            55 =>
            array (
                'id' => 56,
                'key' => 'force_delete_loc_unions',
                'table_name' => 'loc_unions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-02-29 15:45:27',
            ),
            56 =>
            array (
                'id' => 57,
                'key' => 'bulk_delete_loc_unions',
                'table_name' => 'loc_unions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:45:27',
                'updated_at' => '2020-02-29 15:45:27',
            ),
            57 =>
            array (
                'id' => 58,
                'key' => 'browse_loc_villages',
                'table_name' => 'loc_villages',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-02-29 15:54:29',
            ),
            58 =>
            array (
                'id' => 59,
                'key' => 'read_loc_villages',
                'table_name' => 'loc_villages',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-02-29 15:54:29',
            ),
            59 =>
            array (
                'id' => 60,
                'key' => 'edit_loc_villages',
                'table_name' => 'loc_villages',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-02-29 15:54:29',
            ),
            60 =>
            array (
                'id' => 61,
                'key' => 'add_loc_villages',
                'table_name' => 'loc_villages',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-02-29 15:54:29',
            ),
            61 =>
            array (
                'id' => 62,
                'key' => 'delete_loc_villages',
                'table_name' => 'loc_villages',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-02-29 15:54:29',
            ),
            62 =>
            array (
                'id' => 63,
                'key' => 'restore_loc_villages',
                'table_name' => 'loc_villages',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-02-29 15:54:29',
            ),
            63 =>
            array (
                'id' => 64,
                'key' => 'force_delete_loc_villages',
                'table_name' => 'loc_villages',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-02-29 15:54:29',
            ),
            64 =>
            array (
                'id' => 65,
                'key' => 'bulk_delete_loc_villages',
                'table_name' => 'loc_villages',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:54:29',
                'updated_at' => '2020-02-29 15:54:29',
            ),
            65 =>
            array (
                'id' => 66,
                'key' => 'browse_loc_municipalities',
                'table_name' => 'loc_municipalities',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-02-29 15:59:35',
            ),
            66 =>
            array (
                'id' => 67,
                'key' => 'read_loc_municipalities',
                'table_name' => 'loc_municipalities',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-02-29 15:59:35',
            ),
            67 =>
            array (
                'id' => 68,
                'key' => 'edit_loc_municipalities',
                'table_name' => 'loc_municipalities',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-02-29 15:59:35',
            ),
            68 =>
            array (
                'id' => 69,
                'key' => 'add_loc_municipalities',
                'table_name' => 'loc_municipalities',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-02-29 15:59:35',
            ),
            69 =>
            array (
                'id' => 70,
                'key' => 'delete_loc_municipalities',
                'table_name' => 'loc_municipalities',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-02-29 15:59:35',
            ),
            70 =>
            array (
                'id' => 71,
                'key' => 'restore_loc_municipalities',
                'table_name' => 'loc_municipalities',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-02-29 15:59:35',
            ),
            71 =>
            array (
                'id' => 72,
                'key' => 'force_delete_loc_municipalities',
                'table_name' => 'loc_municipalities',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-02-29 15:59:35',
            ),
            72 =>
            array (
                'id' => 73,
                'key' => 'bulk_delete_loc_municipalities',
                'table_name' => 'loc_municipalities',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 15:59:35',
                'updated_at' => '2020-02-29 15:59:35',
            ),
            73 =>
            array (
                'id' => 74,
                'key' => 'browse_permissions',
                'table_name' => 'permissions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-02-29 16:07:09',
            ),
            74 =>
            array (
                'id' => 75,
                'key' => 'read_permissions',
                'table_name' => 'permissions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-02-29 16:07:09',
            ),
            75 =>
            array (
                'id' => 76,
                'key' => 'edit_permissions',
                'table_name' => 'permissions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-02-29 16:07:09',
            ),
            76 =>
            array (
                'id' => 77,
                'key' => 'add_permissions',
                'table_name' => 'permissions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-02-29 16:07:09',
            ),
            77 =>
            array (
                'id' => 78,
                'key' => 'delete_permissions',
                'table_name' => 'permissions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-02-29 16:07:09',
            ),
            78 =>
            array (
                'id' => 79,
                'key' => 'restore_permissions',
                'table_name' => 'permissions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-02-29 16:07:09',
            ),
            79 =>
            array (
                'id' => 80,
                'key' => 'force_delete_permissions',
                'table_name' => 'permissions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-02-29 16:07:09',
            ),
            80 =>
            array (
                'id' => 81,
                'key' => 'bulk_delete_permissions',
                'table_name' => 'permissions',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-02-29 16:07:09',
                'updated_at' => '2020-02-29 16:07:09',
            ),
            81 =>
            array (
                'id' => 82,
                'key' => 'edit_user_access_area',
                'table_name' => 'users',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 1,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            82 =>
            array (
                'id' => 83,
                'key' => 'edit_user_permissions',
                'table_name' => 'users',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 1,
                'created_at' => '2020-03-02 12:27:37',
                'updated_at' => '2020-03-02 12:27:37',
            ),
            83 =>
            array (
                'id' => 84,
                'key' => 'edit_user_role',
                'table_name' => 'users',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 1,
                'created_at' => '2020-03-02 12:28:14',
                'updated_at' => '2020-03-02 12:28:14',
            ),
            84 =>
            array (
                'id' => 85,
                'key' => 'browse_org_designations',
                'table_name' => 'org_designations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-10 13:58:13',
            ),
            85 =>
            array (
                'id' => 86,
                'key' => 'read_org_designations',
                'table_name' => 'org_designations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-10 13:58:13',
            ),
            86 =>
            array (
                'id' => 87,
                'key' => 'edit_org_designations',
                'table_name' => 'org_designations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-10 13:58:13',
            ),
            87 =>
            array (
                'id' => 88,
                'key' => 'add_org_designations',
                'table_name' => 'org_designations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-10 13:58:13',
            ),
            88 =>
            array (
                'id' => 89,
                'key' => 'delete_org_designations',
                'table_name' => 'org_designations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-10 13:58:13',
            ),
            89 =>
            array (
                'id' => 90,
                'key' => 'restore_org_designations',
                'table_name' => 'org_designations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-10 13:58:13',
            ),
            90 =>
            array (
                'id' => 91,
                'key' => 'force_delete_org_designations',
                'table_name' => 'org_designations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-10 13:58:13',
            ),
            91 =>
            array (
                'id' => 92,
                'key' => 'bulk_delete_org_designations',
                'table_name' => 'org_designations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-10 13:58:13',
                'updated_at' => '2020-03-10 13:58:13',
            ),
            92 =>
            array (
                'id' => 93,
                'key' => 'browse_financial_years',
                'table_name' => 'financial_years',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-11 15:41:32',
            ),
            93 =>
            array (
                'id' => 94,
                'key' => 'read_financial_years',
                'table_name' => 'financial_years',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-11 15:41:32',
            ),
            94 =>
            array (
                'id' => 95,
                'key' => 'edit_financial_years',
                'table_name' => 'financial_years',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-11 15:41:32',
            ),
            95 =>
            array (
                'id' => 96,
                'key' => 'add_financial_years',
                'table_name' => 'financial_years',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-11 15:41:32',
            ),
            96 =>
            array (
                'id' => 97,
                'key' => 'delete_financial_years',
                'table_name' => 'financial_years',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-11 15:41:32',
            ),
            97 =>
            array (
                'id' => 98,
                'key' => 'restore_financial_years',
                'table_name' => 'financial_years',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-11 15:41:32',
            ),
            98 =>
            array (
                'id' => 99,
                'key' => 'force_delete_financial_years',
                'table_name' => 'financial_years',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-11 15:41:32',
            ),
            99 =>
            array (
                'id' => 100,
                'key' => 'bulk_delete_financial_years',
                'table_name' => 'financial_years',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 15:41:32',
                'updated_at' => '2020-03-11 15:41:32',
            ),
            100 =>
            array (
                'id' => 101,
                'key' => 'browse_office_informations',
                'table_name' => 'office_informations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 19:39:56',
                'updated_at' => '2020-03-11 19:39:56',
            ),
            101 =>
            array (
                'id' => 102,
                'key' => 'read_office_informations',
                'table_name' => 'office_informations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 19:39:56',
                'updated_at' => '2020-03-11 19:39:56',
            ),
            102 =>
            array (
                'id' => 103,
                'key' => 'edit_office_informations',
                'table_name' => 'office_informations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 19:39:56',
                'updated_at' => '2020-03-11 19:39:56',
            ),
            103 =>
            array (
                'id' => 104,
                'key' => 'add_office_informations',
                'table_name' => 'office_informations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 19:39:56',
                'updated_at' => '2020-03-11 19:39:56',
            ),
            104 =>
            array (
                'id' => 105,
                'key' => 'delete_office_informations',
                'table_name' => 'office_informations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 19:39:56',
                'updated_at' => '2020-03-11 19:39:56',
            ),
            105 =>
            array (
                'id' => 106,
                'key' => 'restore_office_informations',
                'table_name' => 'office_informations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 19:39:56',
                'updated_at' => '2020-03-11 19:39:56',
            ),
            106 =>
            array (
                'id' => 107,
                'key' => 'force_delete_office_informations',
                'table_name' => 'office_informations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 19:39:56',
                'updated_at' => '2020-03-11 19:39:56',
            ),
            107 =>
            array (
                'id' => 108,
                'key' => 'bulk_delete_office_informations',
                'table_name' => 'office_informations',
                'sub_group' => NULL,
                'sub_group_order' => 0,
                'is_user_defined' => 0,
                'created_at' => '2020-03-11 19:39:56',
                'updated_at' => '2020-03-11 19:39:56',
            ),
        ));

        Schema::enableForeignKeyConstraints();
    }
}
