<?php

use Illuminate\Database\Seeder;

class GeoLocationDistrictWiseDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocDistrictsTableDistrictWiseSeeder::class);
        $this->call(LocUpazillasTableDistrictWiseSeeder::class);
        $this->call(LocMunicipalitiesTableDistrictWiseSeeder::class);
        $this->call(LocUnionsTableDistrictWiseSeeder::class);
        $this->call(FinancialYearTableSeeder::class);
        $this->call(CommitteTypesTableSeeder::class);
    }
}
