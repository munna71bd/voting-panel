<?php

use App\Voyager\Traits\Seedable;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;


class VoyagerDatabaseSeeder extends Seeder
{
    use Seedable;

    protected $seedersPath = __DIR__.'/';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->seed('CusDataTypesTableSeeder');
        $this->seed('CusDataRowsTableSeeder');
        $this->seed('CusMenusTableSeeder');
        $this->seed('CusMenuItemsTableSeeder');
        $this->seed('CusRolesTableSeeder');
        $this->seed('CusPermissionsTableSeeder');
        $this->seed('CusPermissionRoleTableSeeder');
        $this->seed('CusSettingsTableSeeder');
        $this->seed('FinancialYearsTableSeeder');
        $this->seed('CusOfficeInformationsTableSeeder');

    }
}
