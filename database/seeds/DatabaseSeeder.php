<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

//        $this->call(LocDivisionsTableSeeder::class);
//        $this->call(LocDistrictsTableSeeder::class);
//        $this->call(LocUpazilasTableSeeder::class);
//        $this->call(LocMunicipalitiesTableSeeder::class);
//        $this->call(LocUnionsTableSeeder::class);
//        $this->call(CusDataTypesTableSeeder::class);
//        $this->call(CusDataRowsTableSeeder::class);
//        $this->call(CusMenuItemsTableSeeder::class);
//        $this->call(CusPermissionsTableSeeder::class);
//        $this->call(CusRolesTableSeeder::class);
//        $this->call(CusPermissionRoleTableSeeder::class);
//        $this->call(CusMenusTableSeeder::class);
//        $this->call(CusSettingsTableSeeder::class);

        $this->call(CusUserRolesTableSeeder::class);
        $this->call(CusFinancialYearsTableSeeder::class);
        $this->call(CusOfficeInformationsTableSeeder::class);
    }
}
