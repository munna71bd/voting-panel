<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataRowsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('data_rows', function(Blueprint $table)
		{
			$table->increments('id');
			$table->unsignedSmallInteger('data_type_id');
			$table->string('field', 191);
			$table->string('type', 191);
			$table->string('display_name', 191);
			$table->boolean('required')->default(0);
			$table->boolean('browse')->default(1);
			$table->boolean('read')->default(1);
			$table->boolean('edit')->default(1);
			$table->boolean('add')->default(1);
			$table->boolean('delete')->default(1);
			$table->boolean('search')->default(0);
			$table->text('details')->nullable();
			$table->text('additional_details')->nullable();
			$table->integer('order')->default(1);

            $table->foreign('data_type_id')
                ->references('id')
                ->on('data_types')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('data_rows');
	}

}
