<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('calendars', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 200);
			$table->text('details')->nullable();
			$table->date('start_date');
			$table->date('end_date')->nullable();
			$table->enum('event_type', array('general_event','holiday','officeday','others'))
                ->nullable()->default('holiday')
                ->index('event_type');

			$table->char('bg_color', 7)->default('#5544FF');
			$table->char('text_color', 7)->default('#FFFFFF');
			$table->timestamps();
			$table->index(['start_date','end_date'], 'start_date_end_date_indx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('calendars');
	}

}
