<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrgDesignationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('org_designations', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('title_en', 100)->nullable();
            $table->string('title_bn', 300)->nullable();
            $table->unsignedMediumInteger('parent_designation_id')->nullable();
            $table->mediumInteger('ordering')->nullable();
            $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                ->default(1)
                ->comment("1 Active, 0 Deactivate, 2 Deleted");

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

            $table->foreign('parent_designation_id')
                ->references('id')
                ->on('org_designations')
                ->onDelete('SET NULL')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('org_designations');
    }
}
