<?php

use App\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocMunicipalitiesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $connections = config('database.mysql_connections');
        $defaultConnName = config('database.default');

        $federatedConnection = $this->getFederatedConnectionString();
        if (!$federatedConnection) {
            return;
        }

        foreach ($connections as $connection => $config) {
            $isMaster = false;
            if ($connection == $defaultConnName) {
                $isMaster = true;
            }
            Schema::connection($connection)->create('loc_municipalities', function (Blueprint $table) use ($isMaster, $federatedConnection) {
                if (!$isMaster) {
                    $table->engine = "FEDERATED CONNECTION='" . $federatedConnection . "/loc_municipalities'";
                }
                $table->increments('id');
                $table->mediumInteger('loc_division_id')->unsigned();
                $table->mediumInteger('loc_district_id')->unsigned();
                $table->integer('loc_upazila_id')->unsigned()->nullable();
                $table->string('geo_code', 7);
                $table->string('title_en', 250);
                $table->string('title', 350);
                $table->string('municipality_type', 300);
                $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                    ->default(1)
                    ->comment("1 Active, 0 Deactivate, 2 Deleted");

                $table->integer('created_by')->unsigned()->nullable();
                $table->integer('updated_by')->unsigned()->nullable();
                $table->timestamps();

                if ($isMaster) {
                    $table->index('row_status');
                    $table->foreign('loc_division_id')
                        ->references('id')
                        ->on('loc_divisions')
                        ->onDelete('CASCADE')->onUpdate('CASCADE');

                    $table->foreign('loc_district_id')
                        ->references('id')
                        ->on('loc_districts')
                        ->onDelete('CASCADE')->onUpdate('CASCADE');

                    $table->foreign('loc_upazila_id')
                        ->references('id')
                        ->on('loc_upazilas')
                        ->onDelete('CASCADE')->onUpdate('CASCADE');
                }

            });
        }
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $connections = array_keys(config('database.mysql_connections'));
        foreach ($connections as $connection) {
            Schema::connection($connection)->dropIfExists('loc_municipalities');
        }
    }

}
