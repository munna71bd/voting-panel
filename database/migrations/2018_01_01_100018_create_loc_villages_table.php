<?php

use App\Migrations\Migration;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;


class CreateLocVillagesTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {

        $connections = config('database.mysql_connections');
        $defaultConnName = config('database.default');

        $federatedConnection = $this->getFederatedConnectionString();
        if(!$federatedConnection){
            return;
        }

        foreach ($connections as $connection => $config) {
            $isMaster = false;
            if ($connection == $defaultConnName) {
                $isMaster = true;
            }
            Schema::connection($connection)
                ->create('loc_villages', function (Blueprint $table) use ($isMaster, $federatedConnection) {
                    if (!$isMaster) {
                        $table->engine = "FEDERATED CONNECTION='" . $federatedConnection . "/loc_villages'";
                    }

                    $table->increments('id');
                    $table->unsignedMediumInteger('loc_division_id');
                    $table->unsignedMediumInteger('loc_district_id');
                    $table->unsignedInteger('loc_upazila_id');
                    $table->unsignedInteger('loc_union_id');

                    $table->char('division_bbs_code', 2)->nullable();
                    $table->char('district_bbs_code', 3)->nullable();
                    $table->char('upazila_bbs_code', 5)->nullable();
                    $table->char('union_bbs_code', 6)->nullable();

                    $table->string('title_en', 250);
                    $table->string('title', 350);
                    $table->char('bbs_code', 7)->nullable();
                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");

                    $table->unsignedInteger('created_by')->nullable();
                    $table->unsignedInteger('updated_by')->nullable();
                    $table->timestamps();

                    if ($isMaster) {

                        $table->index('row_status');

                        $table->foreign('loc_division_id')
                            ->references('id')
                            ->on('loc_divisions')
                            ->onDelete('CASCADE')
                            ->onUpdate('CASCADE');

                        $table->foreign('loc_district_id')
                            ->references('id')
                            ->on('loc_districts')
                            ->onDelete('CASCADE')
                            ->onUpdate('CASCADE');

                        $table->foreign('loc_upazila_id')
                            ->references('id')
                            ->on('loc_upazilas')
                            ->onDelete('CASCADE')
                            ->onUpdate('CASCADE');

                        $table->foreign('loc_union_id')
                            ->references('id')
                            ->on('loc_unions')
                            ->onDelete('CASCADE')
                            ->onUpdate('CASCADE');
                    }

                });
        }
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        $connections = array_keys(config('database.mysql_connections'));
        foreach ($connections as $connection) {
            Schema::connection($connection)->dropIfExists('loc_villages');
        }
    }
}
