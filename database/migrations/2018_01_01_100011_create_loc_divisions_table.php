<?php

use App\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocDivisionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        $connections = config('database.mysql_connections');
        $defaultConnName = config('database.default');

        $federatedConnection = $this->getFederatedConnectionString();
        if (!$federatedConnection) {
            return;
        }
        foreach ($connections as $connection => $config) {
            $isMaster = false;
            if ($connection == $defaultConnName) {
                $isMaster = true;
            }
            Schema::connection($connection)->dropIfExists('loc_divisions');
            Schema::connection($connection)->create('loc_divisions', function (Blueprint $table) use ($isMaster, $federatedConnection) {
                if (!$isMaster) {
                    $table->engine = "FEDERATED CONNECTION='" . $federatedConnection . "/loc_divisions'";
                }

                $table->mediumIncrements('id');
                $table->string('title_en', 191);
                $table->string('title', 300);
                $table->char('bbs_code', 2)->nullable();
                $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                    ->default(1)
                    ->comment("1 Active, 0 Deactivate, 2 Deleted");
                $table->integer('created_by')->unsigned()->nullable();
                $table->integer('updated_by')->unsigned()->nullable();
                $table->timestamps();
                if ($isMaster) {
                    $table->index('row_status');
                }
            });
        }
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        $connections = array_keys(config('database.mysql_connections'));
        foreach ($connections as $connection) {
            Schema::connection($connection)->dropIfExists('loc_divisions');
        }
	}

}
