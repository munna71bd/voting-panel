<?php

use App\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocDistrictsTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        $connections = config('database.mysql_connections');
        $defaultConnName = config('database.default');

        $federatedConnection = $this->getFederatedConnectionString();
        if (!$federatedConnection) {
            return;
        }
        foreach ($connections as $connection => $config) {
            $isMaster = false;
            if ($connection == $defaultConnName) {
                $isMaster = true;
            }
            Schema::connection($connection)
                ->create('loc_districts', function (Blueprint $table) use ($isMaster, $federatedConnection) {
                    if (!$isMaster) {
                        $table->engine = "FEDERATED CONNECTION='" . $federatedConnection . "/loc_districts'";
                    }
                    $table->mediumIncrements('id');
                    $table->unsignedMediumInteger('loc_division_id');
                    $table->char('division_bbs_code', 2)->nullable();
                    $table->string('title_en', 250);
                    $table->string('title', 300);
                    $table->char('bbs_code', 3)->nullable();
                    $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                        ->default(1)
                        ->comment("1 Active, 0 Deactivate, 2 Deleted");
                    $table->integer('created_by')->nullable();
                    $table->integer('updated_by')->nullable();
                    $table->timestamps();

                    if ($isMaster) {
                        $table->index('row_status');
                        $table->foreign('loc_division_id')
                            ->references('id')
                            ->on('loc_divisions')
                            ->onDelete('CASCADE')
                            ->onUpdate('CASCADE');
                    }
                });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $connections = array_keys(config('database.mysql_connections'));
        foreach ($connections as $connection) {
            Schema::connection($connection)->dropIfExists('loc_districts');
        }
    }

}
