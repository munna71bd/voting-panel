<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('api_token', 80)->unique()->nullable()->default(null);
			$table->unsignedSmallInteger('role_id')->nullable();
			$table->string('name', 191);
            $table->string('username', 100)->unique();
            $table->string('email', 191)->nullable();
            $table->string('password', 191);


			$table->string('cell_phone', 15)->unique();
			$table->string('avatar', 191)->nullable()->default('users/default.png');

            $table->string('remember_token', 100)->nullable();


            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->addColumn('tinyinteger', 'row_status', ['unsigned' => true, 'length' => 3])
                ->default(1)
                ->comment("1 Active, 0 Deactivate, 2 Deleted");

			$table->timestamps();


		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}

}
