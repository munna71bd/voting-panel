<?php

use Illuminate\Support\Facades\Route;


Route::get('/logout', function () {
    Auth::logout();
    return redirect('/login');
    return 'log out!';
});

Route::get('clear', function () {
    Artisan::call('config:cache');
    Artisan::call('config:clear');
    Artisan::call('view:clear');
    Artisan::call('storage:link');
    return 'Cleared!';
});


Route::get('', 'ElectionController@index');
Route::get('voter-panel', 'ElectionController@voterPanel')->name('voter-panel');
Route::any('login-voter', 'ElectionController@loginVoter')->name('login-voter');
Route::post('verify-otp', 'ElectionController@verifyOtp')->name('verify-otp');
Route::post('make-vote', 'ElectionController@makeVote')->name('make-vote');
Route::get('vote-result', 'ElectionController@getResult')->name('vote-result');


Route::get('clear-data', function () {
    \Illuminate\Support\Facades\DB::table('users')->update(['is_vote_taken' => 0]);
    \Illuminate\Support\Facades\DB::table('votes')->delete();
    return 'Votes are cleared successfully';
});













